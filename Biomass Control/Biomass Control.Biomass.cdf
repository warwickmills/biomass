
\ ********************** PER File **********************

?EXISTS DL.STAT SWAP DROP .IF 1 DL.STAT .THEN
_END NEW $$$.RUN FORGET $$$.RUN
GetPersistentTop  **Biomass*Control**
-2147483648 FVAR ^Bio_Ash_Removed
-2147483648 FVAR ^Bio_Boiler_Steam_Pressure_Setpoint
-2147483648 FVAR ^Bio_Current_Chip_Volume
-2147483648 FVAR ^Bio_Feeder_Screw_Speed
-2147483648 IVAR ^Bio_Operation_Mode
-2147483648 IVAR ^Bio_running_on_biomass
-2147483648 FVAR ^Bio_Silo_Days_Until_Empty
-2147483648 FVAR ^Bio_Silo_Days_Until_Empty_Extra_Hours
-2147483648 FVAR ^Bio_Silo_Hours_Until_Empty
-2147483648 IVAR ^Bio_System_Enabled_Ash_Removal
-2147483648 IVAR ^Bio_System_Enabled_Day_Bin
-2147483648 IVAR ^Bio_System_Enabled_Moisturizer
-2147483648 IVAR ^Bio_System_Enabled_Scrubber
-2147483648 FVAR ^Bio_Total_Cu_Yd_Per_12_Hours
-2147483648 FVAR ^Bio_Total_Cu_Yd_Per_Hour
10 -2147483648 ITABLE }Bio_Boiler_3_Condensate_Pump_Off_Times
10 -2147483648 ITABLE }Bio_Boiler_3_Condensate_Pump_On_Times
10 -2147483648 ITABLE }Bio_Boiler_4_Condensate_Pump_Off_Times
10 -2147483648 ITABLE }Bio_Boiler_4_Condensate_Pump_On_Times
10 -2147483648 ITABLE }Bio_Day_Bin_Filling_Off_Times
10 -2147483648 ITABLE }Bio_Day_Bin_Filling_On_Times
97 -2147483648 FTABLE }Bio_Feed_15_Minute_Table
366 -2147483648 FTABLE }Bio_Feed_Day_Of_Year_Total
10 -2147483648 ITABLE }Bio_Quench_Outlet_Pump_Off_Times
10 -2147483648 ITABLE }Bio_Quench_Outlet_Pump_On_Times
10 -2147483648 ITABLE }Bio_Scrubber_Outlet_Pump_Off_Times
10 -2147483648 ITABLE }Bio_Scrubber_Outlet_Pump_On_Times
10 -2147483648 ITABLE }Bio_Topside_Blowdown_Off_Times
10 -2147483648 ITABLE }Bio_Topside_Blowdown_On_Times
PersistentMAKECHECK

\ ********************** CRN File **********************

_END NEW $$$.RUN
: FILENAME  ." Biomass Control " ;
1 0 $VAR *_HSV_SEMA 
1024 0 $VAR *_HSV_TEMP 
200 0 $VAR *_HSV_INIT_IO 
0 IVAR ^_HNV_INIT_IO 

\   ++++++++++ SUB name="Calculate_Cycle_Ratio" ++++++++++

0 SUBR &Calculate_Cycle_Ratio
SUB.CODE nullFVAR nullITABLE nullITABLE nullILiteral &Calculate_Cycle_Ratio
0 FVAR ^Cycle_Time_Off
0 FVAR ^Cycle_Time_On
0 IVAR ^Loop_Counter
0 IVAR ^On_Off

: &Calculate_Cycle_Ratio.0
0 JUMP ;
: &Calculate_Cycle_Ratio.1
^On_Off @@ IF 
 GetSystemTime 0 2 PARAM TABLE@ - I>F ^Cycle_Time_On @! 
0 2 PARAM TABLE@ 0 1 PARAM TABLE@ - I>F ^Cycle_Time_Off @! 
ELSE 
0 1 PARAM TABLE@ 0 2 PARAM TABLE@ - I>F ^Cycle_Time_On @! 
 GetSystemTime 0 1 PARAM TABLE@ - I>F ^Cycle_Time_Off @! 
THEN 
1 2 PARAM  TableSize@ 0 -  1 + 1 DO? I ^Loop_Counter @! 
^On_Off @@ IF 
^Loop_Counter @@ 1 -  1 PARAM TABLE@ 0 > ^Loop_Counter @@ 2 PARAM TABLE@ 0 > LAND  IF 
^Cycle_Time_On @@ ^Loop_Counter @@ 1 -  1 PARAM TABLE@ ^Loop_Counter @@ 2 PARAM TABLE@ - I>F F+ ^Cycle_Time_On @! 
THEN 
^Loop_Counter @@ 2 PARAM TABLE@ 0 > ^Loop_Counter @@ 1 PARAM TABLE@ 0 > LAND  IF 
^Cycle_Time_Off @@ ^Loop_Counter @@ 2 PARAM TABLE@ ^Loop_Counter @@ 1 PARAM TABLE@ - I>F F+ ^Cycle_Time_Off @! 
THEN 
ELSE 
^Loop_Counter @@ 1 PARAM TABLE@ 0 > ^Loop_Counter @@ 2 PARAM TABLE@ 0 > LAND  IF 
^Cycle_Time_On @@ ^Loop_Counter @@ 1 PARAM TABLE@ ^Loop_Counter @@ 2 PARAM TABLE@ - I>F F+ ^Cycle_Time_On @! 
THEN 
^Loop_Counter @@ 1 -  2 PARAM TABLE@ 0 > ^Loop_Counter @@ 1 PARAM TABLE@ 0 > LAND  IF 
^Cycle_Time_Off @@ ^Loop_Counter @@ 1 -  2 PARAM TABLE@F F+ ^Loop_Counter @@ 1 PARAM TABLE@F F- ^Cycle_Time_Off @! 
THEN 
THEN 
1 +LOOP 
^Cycle_Time_Off @@ 0 I>F F> IF 
^Cycle_Time_On @@ ^Cycle_Time_On @@ ^Cycle_Time_Off @@ F+ F/ 0 PARAM @! 
ELSE 
0 I>F 0 PARAM @! 
THEN 
0 JUMP ;

CODE
3 PARAM @@ ^On_Off @! 

DUMMY
&Calculate_Cycle_Ratio.0
&Calculate_Cycle_Ratio.1
ENDCODE

\   ++++++++++ SUB name="PACModbusMaster03_Read_Holding_Registers" ++++++++++

0 SUBR &PACModbusMaster03_Read_Holding_Registers
SUB.CODE nullITABLE null$TABLE nullCOMVAR nullITABLE nullITABLE nullPTABLE &PACModbusMaster03_Read_Holding_Registers
0 TVAR ^dtWait_Timer
0 FVAR ^fRTemp1
0 FVAR ^fTemp_1
0 FVAR ^fWaitTime
0 IVAR ^nChR_At_Port
0 IVAR ^nChR_From_Port
0 IVAR ^nIndex
0 IVAR ^nLRC
0 IVAR ^nMasterAdd
0 IVAR ^nMB_Holding_Registers_4X_Int_Length
0 IVAR ^nMB_Holding_Registers_4X_Length
0 IVAR ^nReceive_Index
0 IVAR ^nReceive_Table_Length
0 IVAR ^nRetry_Count
0 IVAR ^nRetry_Delay
0 IVAR ^nRIndex1
0 IVAR ^nRTemp1
0 IVAR ^nRTemp2
0 IVAR ^nSIndex
0 IVAR ^nSIndex2
0 IVAR ^nSLast_CHR
0 IVAR ^nStart_Index
0 IVAR ^nStarting_Address
0 IVAR ^nStartPosition
0 IVAR ^nSTemp1
0 IVAR ^nStep
0 IVAR ^nTemp1
0 IVAR ^nTemp2
0 IVAR ^nTemp3
0 IVAR ^ntest1
0 IVAR ^ntest3
1000 0 $VAR *sASCII_Send_String 
2 0 $VAR *sCRC1HEX 
2 0 $VAR *sCRC2HEX 
4 0 $VAR *sCRC_R_Check 
4 0 $VAR *sCRCHEX 
2 0 $VAR *sFunction_Code_03 
4 0 $VAR *sIdentifierHex 
4 0 $VAR *sLRC_HEX 
4 0 $VAR *sProtocol_IdenitfierHEX 
4 0 $VAR *sQuantity_of_Holding_RegistersHEX 
1000 0 $VAR *sReceice_String 
30 0 $VAR *sReturnStatus 
2 0 $VAR *sRTemp1 
4 0 $VAR *sRTemp2 
4 0 $VAR *sRTemp3 
4 0 $VAR *sRTemp4 
4 0 $VAR *sRTemp5 
8 0 $VAR *sRTemp6 
1000 0 $VAR *sRTU_Rec_String 
1000 0 $VAR *sRTU_Send_String 
4 0 $VAR *sSlave_AddressHEX 
4 0 $VAR *sSLengthHEX 
4 0 $VAR *sStarting_AddressHEX 
2 0 $VAR *sTemp1 
4 0 $VAR *sTest_String 
4 0 $VAR *sTest_String2 
1024 0 COMVAR *chModbusPort 
nullFTABLE 0 POINTER PTR_poftMB_Holding_Registers_4X
nullITABLE 0 POINTER PTR_pontMB_H_Reg4X_Int
261 0 ITABLE }ntCHR_To_Send
261 0 ITABLE }ntReceive_Table

: &PACModbusMaster03_Read_Holding_Registers.0
0 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.1
3 4 PARAM TABLE@F ^fTemp_1 @! 
^fTemp_1 @@ 1000 I>F F/ ^fWaitTime @! 
}ntReceive_Table  TableSize@ ^nReceive_Table_Length @! 
^fWaitTime @@ ^dtWait_Timer  SetTimer 
" " 1 1 PARAM $TABLE@  $MOVE 
0 5 PARAM 
TableMoveToPointer PTR_pontMB_H_Reg4X_Int 
1 5 PARAM 
TableMoveToPointer PTR_poftMB_Holding_Registers_4X 
PTR_poftMB_Holding_Registers_4X  TableSize@ ^nMB_Holding_Registers_4X_Length @! 
PTR_pontMB_H_Reg4X_Int  TableSize@ ^nMB_Holding_Registers_4X_Int_Length @! 
1 4 PARAM TABLE@ 2 4 PARAM TABLE@ +  6 4 PARAM TABLE@ +  ^nMB_Holding_Registers_4X_Length @@ > IF 
" Invalid Table Index" 1 1 PARAM $TABLE@  $MOVE 
THEN 
1 4 PARAM TABLE@ 2 4 PARAM TABLE@ +  6 4 PARAM TABLE@ +  ^nMB_Holding_Registers_4X_Int_Length @@ > IF 
" Invalid Table Index" 1 1 PARAM $TABLE@  $MOVE 
THEN 
1 4 PARAM TABLE@ 2 4 PARAM TABLE@ +  6 4 PARAM TABLE@ +  3 PARAM  TableSize@ > IF 
" Invalid Table Index" 1 1 PARAM $TABLE@  $MOVE 
THEN 
1 4 PARAM TABLE@ 6 4 PARAM TABLE@ +  0 < IF 
" Invalid Table Index" 1 1 PARAM $TABLE@  $MOVE 
THEN 
19 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.3
2 PARAM  ?STREAM.KEY 0 >= IF 
0 1 0 PARAM TABLE! 
ELSE 
2 PARAM  OPEN 1 0 PARAM TABLE! 
THEN 
^nRetry_Count  1+@! 
12 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.7
1 0 PARAM TABLE@ -412 = IF 
1000 ^nRetry_Delay @! 
ELSE 
1 ^nRetry_Delay @! 
THEN 
12 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.14
  " Port Open Retry" 
 
  1  
  1 PARAM $TABLE@   $MOVE
  ^nRetry_Delay @@   DELAY
-3 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.17
2 PARAM 10  S.!EOL 
255 1 0 4 PARAM  ClampTable 
65535 1 1 4 PARAM  ClampTable 
125 1 2 4 PARAM  ClampTable 
1 4 PARAM TABLE@ ^nStarting_Address @! 
2 4 PARAM TABLE@ 4 *sQuantity_of_Holding_RegistersHEX  I>HEX$.R 
^nStarting_Address @@ 1 -  4 *sStarting_AddressHEX  I>HEX$.R 
4 4 PARAM TABLE@ CASE 
0 OF 
0 ^nSIndex2 @! 
0 4 PARAM TABLE@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
3 ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 2 1 + 0 DO? I ^nSIndex @! 
*sStarting_AddressHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
2 2 1 + 0 DO? I ^nSIndex @! 
*sQuantity_of_Holding_RegistersHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
1 ^nSIndex2 @@ 1 -  1 + 0 DO? I ^nIndex @! 
^nIndex @@ }ntCHR_To_Send TABLE@ ^ntest1 @! 
^ntest1 @@ *sRTU_Send_String $APPEND 

1 +LOOP 
-1 *sRTU_Send_String  CalcCRCR ^ntest3 @! 
^ntest3 @@ 4 *sCRCHEX  I>HEX$.R 
-2 0 2 DO? I ^nSIndex @! 
*sCRCHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
-2 +LOOP 
^nSIndex2 @@ 1 -  ^nSLast_CHR @! 
ENDOF 
1 OF 
2 2 1 + 0 DO? I ^nSIndex @! 
*sStarting_AddressHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nTemp1 @@ ^nSTemp1 @@ +  ^nTemp1 @! 
2 +LOOP 
2 2 1 + 0 DO? I ^nSIndex @! 
*sQuantity_of_Holding_RegistersHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nTemp1 @@ ^nSTemp1 @@ +  ^nTemp1 @! 
2 +LOOP 
^nTemp1 @@ 0 4 PARAM TABLE@ +  3 +  ^nTemp1 @! 
BEGIN ^nTemp1 @@ 255 > WHILE 
^nTemp1 @@ 256 -  ^nTemp1 @! 
REPEAT 
^nTemp1 @@ 0 = IF 
0 ^nLRC @! 
ELSE 
255 ^nTemp1 @@ -  1 +  ^nLRC @! 
THEN 
^nLRC @@ 2 *sLRC_HEX  I>HEX$.R 
0 4 PARAM TABLE@ 2 *sSlave_AddressHEX  I>HEX$.R 
" :" *sASCII_Send_String  $MOVE 
*sSlave_AddressHEX *sASCII_Send_String $CAT 

" 03" *sASCII_Send_String $CAT 

*sStarting_AddressHEX *sASCII_Send_String $CAT 

*sQuantity_of_Holding_RegistersHEX *sASCII_Send_String $CAT 

*sLRC_HEX *sASCII_Send_String $CAT 

13 *sASCII_Send_String $APPEND 

10 *sASCII_Send_String $APPEND 

ENDOF 
2 OF 
7 4 PARAM TABLE@ 65535 < IF 
7 4 PARAM TABLE@ 1 +  7 4 PARAM TABLE! 
ELSE 
1 7 4 PARAM TABLE! 
THEN 
7 4 PARAM TABLE@ 4 *sIdentifierHex  I>HEX$.R 
0 4 PARAM TABLE@ 2 *sSlave_AddressHEX  I>HEX$.R 
0 ^nSIndex2 @! 
2 2 1 + 0 DO? I ^nSIndex @! 
*sIdentifierHex ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
" 0000" *sProtocol_IdenitfierHEX  $MOVE 
2 2 1 + 0 DO? I ^nSIndex @! 
*sProtocol_IdenitfierHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
^nSIndex2  1+@! 
^nSIndex2  1+@! 
*sSlave_AddressHEX  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
" 03" *sFunction_Code_03  $MOVE 
*sFunction_Code_03  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 2 1 + 0 DO? I ^nSIndex @! 
*sStarting_AddressHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
2 2 1 + 0 DO? I ^nSIndex @! 
*sQuantity_of_Holding_RegistersHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
^nSIndex2 @@ 1 -  ^nSLast_CHR @! 
^nSLast_CHR @@ 5 -  4 *sSLengthHEX  I>HEX$.R 
4 ^nSIndex2 @! 
2 2 1 + 0 DO? I ^nSIndex @! 
*sSLengthHEX ^nSIndex @@ 2 *sTemp1  $SUB 
*sTemp1  HEX$>I ^nSTemp1 @! 
^nSTemp1 @@ ^nSIndex2 @@ }ntCHR_To_Send TABLE! 
^nSIndex2  1+@! 
2 +LOOP 
ENDOF 
ENDCASE 
0 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.19
2 PARAM  STREAM.CLEAR.BUF 
" " 0 1 PARAM $TABLE@  $MOVE 
4 4 PARAM TABLE@ CASE 
0 OF 
*sRTU_Send_String 0 1 PARAM $TABLE@  $MOVE 
1 ^nSLast_CHR @@ 1 + 0 DO? I ^nSIndex @! 
^nSIndex @@ }ntCHR_To_Send TABLE@ ^nSTemp1 @! 
^nSTemp1 @@ 2 PARAM  EMIT$ 1 0 PARAM TABLE! 
1 +LOOP 
2 PARAM  STREAM.CR 1 0 PARAM TABLE! 
^dtWait_Timer  StartTimer 
ENDOF 
1 OF 
*sASCII_Send_String 0 1 PARAM $TABLE@  $MOVE 
*sASCII_Send_String 2 PARAM  PRT$ 1 0 PARAM TABLE! 
^dtWait_Timer  StartTimer 
ENDOF 
2 OF 
1 ^nSLast_CHR @@ 1 + 0 DO? I ^nSIndex @! 
^nSIndex @@ }ntCHR_To_Send TABLE@ ^nSTemp1 @! 
^nSTemp1 @@ *sRTU_Send_String $APPEND 

^nSTemp1 @@ 2 PARAM  EMIT$ 1 0 PARAM TABLE! 
1 +LOOP 
" " 2 PARAM  PRT$ 1 0 PARAM TABLE! 
*sRTU_Send_String 0 1 PARAM $TABLE@  $MOVE 
^dtWait_Timer  StartTimer 
ENDOF 
ENDCASE 
0 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.21
  2 PARAM   ?STREAM.KEY
  ^nChR_At_Port @! 
9 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.33
" " 2 1 PARAM $TABLE@  $MOVE 
4 4 PARAM TABLE@ CASE 
0 OF 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
^nReceive_Index  1+@! 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
^nReceive_Index  1+@! 
^nChR_From_Port @@ 3 = IF 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
BEGIN 
2 PARAM  ?STREAM.KEY ^nChR_At_Port @! 
^nChR_At_Port @@ ^nChR_From_Port @@ 2 +  = ^dtWait_Timer  T0= LOR  UNTIL 
^nChR_At_Port @@ ^nChR_From_Port @@ 2 +  = IF 
1 ^nChR_From_Port @@ 4 +  1 + 3 DO? I ^nReceive_Index @! 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
1 +LOOP 
^nReceive_Index  1+@! 
ELSE 
" Missing Chars" 1 1 PARAM $TABLE@  $MOVE 
THEN 
ELSE 
BEGIN 
BEGIN 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nReceive_Index @@ ^nReceive_Table_Length @@ < IF 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
ELSE 
" Too Many Characters" 1 1 PARAM $TABLE@  $MOVE 
THEN 
^nReceive_Index  1+@! 
2 PARAM  ?STREAM.KEY ^nChR_At_Port @! 
^nChR_At_Port @@ 0 <= 1 1 PARAM $TABLE@ " " $= LNOT  LOR  UNTIL 
200  DELAY 
2 PARAM  ?STREAM.KEY ^nChR_At_Port @! 
^nChR_At_Port @@ 0 <= 1 1 PARAM $TABLE@ " " $= LNOT  LOR  UNTIL 
THEN 
1 1 PARAM $TABLE@ " " $= IF 
1 ^nReceive_Index @@ 3 -  1 + 0 DO? I ^nIndex @! 
^nIndex @@ }ntReceive_Table TABLE@ ^ntest1 @! 
^ntest1 @@ *sRTU_Rec_String $APPEND 

1 +LOOP 
-1 *sRTU_Rec_String  CalcCRCR ^ntest3 @! 
^ntest3 @@ 4 *sCRCHEX  I>HEX$.R 
^nReceive_Index @@ 1 -  }ntReceive_Table TABLE@ 2 *sCRC1HEX  I>HEX$.R 
^nReceive_Index @@ 2 -  }ntReceive_Table TABLE@ 2 *sCRC2HEX  I>HEX$.R 
^nReceive_Index @@ 2 -  }ntReceive_Table TABLE@ ^ntest1 @! 
^ntest1 @@ *sRTU_Rec_String $APPEND 

^nReceive_Index @@ 1 -  }ntReceive_Table TABLE@ ^ntest1 @! 
^ntest1 @@ *sRTU_Rec_String $APPEND 

*sCRC1HEX *sCRC_R_Check  $MOVE 
*sCRC2HEX *sCRC_R_Check $CAT 

*sRTU_Rec_String 2 1 PARAM $TABLE@  $MOVE 
*sCRCHEX *sCRC_R_Check $= LNOT  IF 
" CRC Mismatch" 1 1 PARAM $TABLE@  $MOVE 
ELSE 
0 }ntCHR_To_Send TABLE@ 0 }ntReceive_Table TABLE@ <> IF 
" Address Mismatch" 1 1 PARAM $TABLE@  $MOVE 
ELSE 
1 }ntCHR_To_Send TABLE@ 1 }ntReceive_Table TABLE@ <> IF 
1 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 2 *sTest_String2  I>HEX$.R 
2 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 2 *sRTemp1  I>HEX$.R 
32 *sTest_String2 $APPEND 

*sRTemp1 *sTest_String2 $CAT 

*sTest_String2 1 1 PARAM $TABLE@  $MOVE 
THEN 
THEN 
THEN 
THEN 
ENDOF 
1 OF 
*sReceice_String 2 PARAM  GET$ 1 0 PARAM TABLE! 
1 0 PARAM TABLE@ 0 <> IF 
" Receive Error" 1 1 PARAM $TABLE@  $MOVE 
ELSE 
*sReceice_String 2 1 PARAM $TABLE@  $MOVE 
58 0 *sReceice_String  FindChar ^nStartPosition @! 
^nStartPosition @@ 0 >= IF 
*sReceice_String ^nStartPosition @@ 1 +  2 *sTest_String  $SUB 
*sTest_String  HEX$>I ^nTemp1 @! 
^nTemp2 @@ ^nTemp1 @@ +  ^nTemp2 @! 
*sSlave_AddressHEX *sTest_String $= LNOT  IF 
" Address Mismatch" 1 1 PARAM $TABLE@  $MOVE 
ELSE 
*sReceice_String ^nStartPosition @@ 3 +  2 *sTest_String  $SUB 
*sTest_String  HEX$>I ^nTemp1 @! 
^nTemp2 @@ ^nTemp1 @@ +  ^nTemp2 @! 
" 03" *sTest_String $= LNOT  IF 
" Function Mismatch" 1 1 PARAM $TABLE@  $MOVE 
*sTest_String " 83" $= IF 
*sReceice_String ^nStartPosition @@ 3 +  4 *sTest_String  $SUB 
*sTest_String 1 1 PARAM $TABLE@  $MOVE 
THEN 
THEN 
THEN 
1 1 PARAM $TABLE@ " " $= IF 
*sReceice_String ^nStartPosition @@ 5 +  2 *sTest_String  $SUB 
*sTest_String  HEX$>I ^nTemp1 @! 
^nTemp2 @@ ^nTemp1 @@ +  ^nTemp2 @! 
2 ^nReceive_Index @! 
^nTemp1 @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
^nReceive_Index  1+@! 
7 ^nTemp3 @! 
1 ^nTemp1 @@ 1 + 1 DO? I ^nStep @! 
*sReceice_String ^nStartPosition @@ ^nTemp3 @@ +  2 *sTest_String  $SUB 
*sTest_String  HEX$>I ^nTemp1 @! 
^nTemp2 @@ ^nTemp1 @@ +  ^nTemp2 @! 
^nTemp1 @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
^nTemp3  1+@! 
^nTemp3  1+@! 
^nReceive_Index  1+@! 
1 +LOOP 
BEGIN ^nTemp2 @@ 255 > WHILE 
^nTemp2 @@ 256 -  ^nTemp2 @! 
REPEAT 
^nTemp2 @@ 0 = IF 
0 ^nLRC @! 
ELSE 
255 ^nTemp2 @@ -  1 +  ^nLRC @! 
THEN 
^nLRC @@ 2 *sLRC_HEX  I>HEX$.R 
*sReceice_String ^nStartPosition @@ ^nTemp3 @@ +  2 *sTest_String  $SUB 
*sLRC_HEX *sTest_String $= LNOT  IF 
" LRC Mismatch" 1 1 PARAM $TABLE@  $MOVE 
THEN 
THEN 
ELSE 
" : Not Found" 1 1 PARAM $TABLE@  $MOVE 
THEN 
THEN 
ENDOF 
2 OF 
BEGIN 
1 ^nChR_At_Port @@ 1 + 1 DO? I ^nRIndex1 @! 
2 PARAM  TOKEY ^nChR_From_Port @! 
^nChR_From_Port @@ ^nReceive_Index @@ }ntReceive_Table TABLE! 
^nReceive_Index  1+@! 
1 +LOOP 
2 PARAM  ?STREAM.KEY ^nChR_At_Port @! 
^nChR_At_Port @@ 0 <= UNTIL 
1 ^nReceive_Index @@ 1 -  1 + 0 DO? I ^nIndex @! 
^nIndex @@ }ntReceive_Table TABLE@ ^ntest1 @! 
^ntest1 @@ *sRTU_Rec_String $APPEND 

1 +LOOP 
*sRTU_Rec_String 2 1 PARAM $TABLE@  $MOVE 
0 }ntCHR_To_Send TABLE@ 0 }ntReceive_Table TABLE@ <> 1 }ntCHR_To_Send TABLE@ 1 }ntReceive_Table TABLE@ <> LOR  IF 
" Identifier Mismatch" 1 1 PARAM $TABLE@  $MOVE 
ELSE 
7 }ntCHR_To_Send TABLE@ 7 }ntReceive_Table TABLE@ <> IF 
7 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 2 *sReturnStatus  I>HEX$.R 
8 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 2 *sRTemp1  I>HEX$.R 
32 *sReturnStatus $APPEND 

*sRTemp1 *sReturnStatus $CAT 

*sReturnStatus 1 1 PARAM $TABLE@  $MOVE 
THEN 
THEN 
ENDOF 
ENDCASE 
11 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.39
4 4 PARAM TABLE@ 2 = IF 
8 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 8 +  ^nRTemp1 @! 
9 ^nSIndex @! 
ELSE 
2 }ntReceive_Table TABLE@ ^nRTemp1 @! 
^nRTemp1 @@ 2 +  ^nRTemp1 @! 
3 ^nSIndex @! 
THEN 
1 4 PARAM TABLE@ 6 4 PARAM TABLE@ +  ^nStart_Index @! 
5 4 PARAM TABLE@ 0 = IF 
2 ^nMasterAdd @! 
ELSE 
1 ^nMasterAdd @! 
THEN 
BEGIN 
^nStarting_Address @@ 3 PARAM TABLE@ CASE 
1 OF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
*sRTemp3 *sRTemp2 $CAT 

*sRTemp2  HEX$>I ^nRTemp2 @! 
^nRTemp2 @@ 32767 > IF 
^nRTemp2 @@ 65536 -  ^nStart_Index @@ PTR_pontMB_H_Reg4X_Int TABLE! 
ELSE 
^nRTemp2 @@ ^nStart_Index @@ PTR_pontMB_H_Reg4X_Int TABLE! 
THEN 
^nStart_Index  1+@! 
^nStarting_Address  1+@! 
^nSIndex @@ 2 +  ^nSIndex @! 
ENDOF 
2 OF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
^nSIndex @@ 2 +  }ntReceive_Table TABLE@ 2 *sRTemp4  I>HEX$.R 
^nSIndex @@ 3 +  }ntReceive_Table TABLE@ 2 *sRTemp5  I>HEX$.R 
*_HSV_SEMA Acquire1String *sRTemp4  *_HSV_TEMP $MOVE *sRTemp5  *_HSV_TEMP $CAT *sRTemp2  *_HSV_TEMP $CAT *sRTemp3  *_HSV_TEMP $CAT *_HSV_TEMP *sRTemp6 $MOVE Release1String 
*sRTemp6  HEX$>I ^fRTemp1 @! 
^fRTemp1 @@ ^nStart_Index @@ PTR_poftMB_Holding_Registers_4X TABLE! 
^nStart_Index @@ ^nMasterAdd @@ +  ^nStart_Index @! 
^nStarting_Address @@ ^nMasterAdd @@ +  ^nStarting_Address @! 
^nSIndex @@ 4 +  ^nSIndex @! 
ENDOF 
3 OF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
^nSIndex @@ 2 +  }ntReceive_Table TABLE@ 2 *sRTemp4  I>HEX$.R 
^nSIndex @@ 3 +  }ntReceive_Table TABLE@ 2 *sRTemp5  I>HEX$.R 
*_HSV_SEMA Acquire1String *sRTemp2  *_HSV_TEMP $MOVE *sRTemp3  *_HSV_TEMP $CAT *sRTemp4  *_HSV_TEMP $CAT *sRTemp5  *_HSV_TEMP $CAT *_HSV_TEMP *sRTemp6 $MOVE Release1String 
*sRTemp6  HEX$>I ^fRTemp1 @! 
^fRTemp1 @@ ^nStart_Index @@ PTR_poftMB_Holding_Registers_4X TABLE! 
^nStart_Index @@ ^nMasterAdd @@ +  ^nStart_Index @! 
^nStarting_Address @@ ^nMasterAdd @@ +  ^nStarting_Address @! 
^nSIndex @@ 4 +  ^nSIndex @! 
ENDOF 
4 OF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
^nSIndex @@ 2 +  }ntReceive_Table TABLE@ 2 *sRTemp4  I>HEX$.R 
^nSIndex @@ 3 +  }ntReceive_Table TABLE@ 2 *sRTemp5  I>HEX$.R 
*_HSV_SEMA Acquire1String *sRTemp4  *_HSV_TEMP $MOVE *sRTemp5  *_HSV_TEMP $CAT *sRTemp2  *_HSV_TEMP $CAT *sRTemp3  *_HSV_TEMP $CAT *_HSV_TEMP *sRTemp6 $MOVE Release1String 
*sRTemp6  HEX$>I ^nRTemp2 @! 
^nRTemp2 @@ ^nStart_Index @@ PTR_pontMB_H_Reg4X_Int TABLE! 
^nStart_Index @@ ^nMasterAdd @@ +  ^nStart_Index @! 
^nStarting_Address @@ ^nMasterAdd @@ +  ^nStarting_Address @! 
^nSIndex @@ 4 +  ^nSIndex @! 
ENDOF 
5 OF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
^nSIndex @@ 2 +  }ntReceive_Table TABLE@ 2 *sRTemp4  I>HEX$.R 
^nSIndex @@ 3 +  }ntReceive_Table TABLE@ 2 *sRTemp5  I>HEX$.R 
*_HSV_SEMA Acquire1String *sRTemp2  *_HSV_TEMP $MOVE *sRTemp3  *_HSV_TEMP $CAT *sRTemp4  *_HSV_TEMP $CAT *sRTemp5  *_HSV_TEMP $CAT *_HSV_TEMP *sRTemp6 $MOVE Release1String 
*sRTemp6  HEX$>I ^nRTemp2 @! 
^nRTemp2 @@ ^nStart_Index @@ PTR_pontMB_H_Reg4X_Int TABLE! 
^nStart_Index @@ ^nMasterAdd @@ +  ^nStart_Index @! 
^nStarting_Address @@ ^nMasterAdd @@ +  ^nStarting_Address @! 
^nSIndex @@ 4 +  ^nSIndex @! 
ENDOF 
^nSIndex @@ }ntReceive_Table TABLE@ 2 *sRTemp2  I>HEX$.R 
^nSIndex @@ 1 +  }ntReceive_Table TABLE@ 2 *sRTemp3  I>HEX$.R 
*sRTemp3 *sRTemp2 $CAT 

*sRTemp2  HEX$>I ^nRTemp2 @! 
^nRTemp2 @@ ^nStart_Index @@ PTR_pontMB_H_Reg4X_Int TABLE! 
^nStart_Index  1+@! 
^nStarting_Address  1+@! 
^nSIndex @@ 2 +  ^nSIndex @! 
ENDCASE 
^nSIndex @@ ^nRTemp1 @@ >= UNTIL 
3 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.47
" No Port" 1 1 PARAM $TABLE@  $MOVE 
2 0 PARAM TABLE@ 1 +  2 0 PARAM TABLE! 
11 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.49
" Timeout" 1 1 PARAM $TABLE@  $MOVE 
2 0 PARAM TABLE@ 1 +  2 0 PARAM TABLE! 
^nChR_At_Port @@ 1 0 PARAM TABLE! 
10 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.50
2 0 PARAM TABLE@ 1 +  2 0 PARAM TABLE! 
9 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.51
" OK" 1 1 PARAM $TABLE@  $MOVE 
0 0 PARAM TABLE@ 1 +  0 0 PARAM TABLE! 
8 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.57
2 0 PARAM TABLE@ 1 +  2 0 PARAM TABLE! 
7 JUMP ;
: &PACModbusMaster03_Read_Holding_Registers.6
FALSE
  0  
  1  
  0 PARAM TABLE@   =
LOR
  -47  
  1  
  0 PARAM TABLE@   =
LOR
IF -11 JUMP ELSE -13 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.10
TRUE
  ^nRetry_Count @@ 
  2    >=
LAND
IF -7 JUMP ELSE -13 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.22
TRUE
  ^nChR_At_Port @@ 
  5    >=
LAND
IF -10 JUMP ELSE 0 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.23
TRUE
  ^dtWait_Timer   T0=
LAND
IF -8 JUMP ELSE 0 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.24
TRUE
  ^nChR_At_Port @@ 
  0    <
LAND
IF -9 JUMP ELSE -13 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.35
TRUE
  " " 
 
  1  
  1 PARAM $TABLE@   $=
LAND
IF -12 JUMP ELSE -9 JUMP THEN ;
: &PACModbusMaster03_Read_Holding_Registers.58
TRUE
  " " 
 
  1  
  1 PARAM $TABLE@   $=
LAND
IF -20 JUMP ELSE -8 JUMP THEN ;

CODE

DUMMY
&PACModbusMaster03_Read_Holding_Registers.0
&PACModbusMaster03_Read_Holding_Registers.1
&PACModbusMaster03_Read_Holding_Registers.3
&PACModbusMaster03_Read_Holding_Registers.7
&PACModbusMaster03_Read_Holding_Registers.14
&PACModbusMaster03_Read_Holding_Registers.17
&PACModbusMaster03_Read_Holding_Registers.19
&PACModbusMaster03_Read_Holding_Registers.21
&PACModbusMaster03_Read_Holding_Registers.33
&PACModbusMaster03_Read_Holding_Registers.39
&PACModbusMaster03_Read_Holding_Registers.47
&PACModbusMaster03_Read_Holding_Registers.49
&PACModbusMaster03_Read_Holding_Registers.50
&PACModbusMaster03_Read_Holding_Registers.51
&PACModbusMaster03_Read_Holding_Registers.57
&PACModbusMaster03_Read_Holding_Registers.6
&PACModbusMaster03_Read_Holding_Registers.10
&PACModbusMaster03_Read_Holding_Registers.22
&PACModbusMaster03_Read_Holding_Registers.23
&PACModbusMaster03_Read_Holding_Registers.24
&PACModbusMaster03_Read_Holding_Registers.35
&PACModbusMaster03_Read_Holding_Registers.58
ENDCODE

\   ++++++++++ SUB name="Record_Cycle_Time" ++++++++++

0 SUBR &Record_Cycle_Time
SUB.CODE nullITABLE &Record_Cycle_Time
0 IVAR ^Loop_Counter

: &Record_Cycle_Time.0
0 JUMP ;
: &Record_Cycle_Time.1
-1 0 0 PARAM  TableSize@ 2 -  DO? I ^Loop_Counter @! 
^Loop_Counter @@ 0 PARAM TABLE@ ^Loop_Counter @@ 1 +  0 PARAM TABLE! 
-1 +LOOP 
 GetSystemTime 0 0 PARAM TABLE! 
0 JUMP ;

CODE

DUMMY
&Record_Cycle_Time.0
&Record_Cycle_Time.1
ENDCODE
0 TASK  &_INIT_IO
0 TASK &Alarms
0 TASK &Ash_Out
0 TASK &Boilers
0 TASK &Changelog
0 TASK &Clear_Ash_Auger
0 TASK &Clear_Belt_Auger_Transistion
0 TASK &Clear_Belt_Jam
0 TASK &Clear_Lift_Auger
0 TASK &Condensate
0 TASK &Day_Bin_Fill
0 TASK &Day_Bin_Timed_Fill
0 TASK &Email
0 TASK &Extinguish_Day_Bin
0 TASK &Feed_Section
0 TASK &Gasifier
0 TASK &Incineration
0 TASK &Mode_Fuel_Saver
0 TASK &Mode_Management
0 TASK &Mode_Run
0 TASK &Mode_Shutdown
0 TASK &Mode_Startup
0 TASK &Moisturizer_Pumpout
0 TASK &Ph_Dose
0 TASK &Powerup
0 TASK &Read_Sensors
0 TASK &Safety_Systems
0 TASK &VFD_Serial
0 IVAR ^Bio_24hour_reset
0 FVAR ^Bio_Above_Door_Temp
0 FVAR ^Bio_Air_Moisturizer_Air_Outlet_Temp
0 FVAR ^Bio_Air_Moisturizer_Level
0 FVAR ^Bio_Air_Moisturizer_Water_Inlet_Flow
0 FVAR ^Bio_Air_Moisturizer_Water_Inlet_Temp
0 FVAR ^Bio_Air_Moisturizer_Water_Temp
0 FVAR ^Bio_Air_Super_Heater_Outlet_Temp
0 IVAR ^Bio_Alarm_Count
0 IVAR ^Bio_Alarm_Triggered
0 FVAR ^Bio_Ash_Temp
0 FVAR ^Bio_Bed_Height
0 FVAR ^Bio_Boiler_3_Condensate_Pump_Cycle_Ratio
0 FVAR ^Bio_Boiler_3_Inlet_Temp
0 FVAR ^Bio_Boiler_3_Outlet_Temp
0 FVAR ^Bio_Boiler_4_Condensate_Pump_Cycle_Ratio
0 FVAR ^Bio_Boiler_4_Inlet_Temp
0 FVAR ^Bio_Boiler_4_Outlet_Temp
0 FVAR ^Bio_Boiler_Steam_Pressure
0 FVAR ^Bio_Ceiling_Temp
0 TVAR ^Bio_Check_Database_Timer
0 FVAR ^Bio_Chip_Volume_Factor
0 IVAR ^Bio_Clearing_Belt_Auger_Jam
0 IVAR ^Bio_Clearing_Belt_Jam
0 FVAR ^Bio_Condensate_pH
0 IVAR ^Bio_Database_Connection
0 FVAR ^Bio_Day_Bin_Fill_Delay
0 FVAR ^Bio_Day_Bin_Fill_Timer
0 FVAR ^Bio_Day_Bin_Filling_Cycle_Ratio
0 IVAR ^Bio_Day_Bin_High_Disabled
0 IVAR ^Bio_Day_Bin_Running_On_Timer
0 IVAR ^Bio_Day_Bin_TPO_Period
0 IVAR ^Bio_Day_Of_Week
0 IVAR ^Bio_DayBin_Spray_ON
1073741824 FVAR ^Bio_DayBin_TPO_Percent
0 FVAR ^Bio_Delay_Ash_Removal_Time
0 IVAR ^Bio_Delay_Day_Bin_Clear_Conveyors_Msec
0 FVAR ^Bio_Delay_Ph_Check
0 FVAR ^Bio_Delay_Ph_Dose_Time
0 IVAR ^Bio_Delay_Silo_Auger_Jam_Clear
0 IVAR ^Bio_Error_No_Database_Connection
0 IVAR ^Bio_Extinguish_Day_Bin_Fire
0 FVAR ^Bio_Feed_Auger_1_Temp
0 FVAR ^Bio_Feed_Auger_2_Temp
0 FVAR ^Bio_Feed_Auger_3_Temp
0 FVAR ^Bio_Feed_Auger_4_Temp
0 FVAR ^Bio_Feed_Auger_5_Temp
0 IVAR ^Bio_Feed_Auto
0 FVAR ^Bio_Feed_Rate_Trim
0 FVAR ^Bio_Feed_Screw_Period
0 FVAR ^Bio_Fuel_Fed_12_Hour_Total
0 FVAR ^Bio_Fuel_Fed_24_Hour_Total
0 FVAR ^Bio_Fuel_Fed_Short
0 FVAR ^Bio_Gasifer_Alarm_Temp
0 FVAR ^Bio_Gasifier_Pressure
0 FVAR ^Bio_Gasifier_Temp_1
0 FVAR ^Bio_Gasifier_Temp_2
0 FVAR ^Bio_Gasifier_Temp_3
0 FVAR ^Bio_Gasifier_Temp_4
0 FVAR ^Bio_Gasifier_Temp_5
0 FVAR ^Bio_Gasifier_Temp_6
0 FVAR ^Bio_Gasifier_Temp_7
0 FVAR ^Bio_Gasifier_Top_Temp_1
0 FVAR ^Bio_Gasifier_Warning_Temp
0 FVAR ^Bio_Incineration_Temp_Minimum
0 IVAR ^Bio_Index_Alarm_Type_Analog_High
0 IVAR ^Bio_Index_Alarm_Type_Analog_Low
0 IVAR ^Bio_Index_Alarm_Type_Custom
0 IVAR ^Bio_Index_Alarm_Type_Digital
0 IVAR ^Bio_Index_Alarm_Type_Torque
0 IVAR ^Bio_Index_Alarms_Ash_Temp_High
0 IVAR ^Bio_Index_Alarms_Ash_Temp_Low
0 IVAR ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High
0 IVAR ^Bio_Index_Alarms_Boiler_3_Over_Pressure
0 IVAR ^Bio_Index_Alarms_Boiler_3_Water_Critical
0 IVAR ^Bio_Index_Alarms_Boiler_3_Water_High
0 IVAR ^Bio_Index_Alarms_Boiler_3_Water_Low
0 IVAR ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High
0 IVAR ^Bio_Index_Alarms_Boiler_4_Over_Pressure
0 IVAR ^Bio_Index_Alarms_Boiler_4_Water_Critical
0 IVAR ^Bio_Index_Alarms_Boiler_4_Water_High
0 IVAR ^Bio_Index_Alarms_Boiler_4_Water_Low
0 IVAR ^Bio_Index_Alarms_Condensate_pH_Low
0 IVAR ^Bio_Index_Alarms_Conveyor_Jam1
0 IVAR ^Bio_Index_Alarms_Conveyor_Jam2
0 IVAR ^Bio_Index_Alarms_Day_Bin_High_PE
0 IVAR ^Bio_Index_Alarms_Day_Bin_Low
0 IVAR ^Bio_Index_Alarms_Feed_Auger_1_Temp_High
0 IVAR ^Bio_Index_Alarms_Feed_Auger_2_Temp_High
0 IVAR ^Bio_Index_Alarms_Feed_Auger_3_Temp_High
0 IVAR ^Bio_Index_Alarms_Feed_Auger_4_Temp_High
0 IVAR ^Bio_Index_Alarms_Feed_Auger_5_Temp_High
0 IVAR ^Bio_Index_Alarms_Feed_Screw_1_Torque_High
0 IVAR ^Bio_Index_Alarms_Feed_Screw_2_Torque_High
0 IVAR ^Bio_Index_Alarms_Feed_Screw_3_Torque_High
0 IVAR ^Bio_Index_Alarms_Feed_Screw_4_Torque_High
0 IVAR ^Bio_Index_Alarms_Feed_Screw_5_Torque_High
0 IVAR ^Bio_Index_Alarms_Feed_System_Estop
0 IVAR ^Bio_Index_Alarms_Gasifier_DP_High
0 IVAR ^Bio_Index_Alarms_Gasifier_Temp_High
0 IVAR ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low
0 IVAR ^Bio_Index_Alarms_Lift_Auger_Torque_High
0 IVAR ^Bio_Index_Alarms_Moisturizer_Water_DP_High
0 IVAR ^Bio_Index_Alarms_Moisturizer_Water_DP_Low
0 IVAR ^Bio_Index_Alarms_Moisturizer_Water_High
0 IVAR ^Bio_Index_Alarms_Moisturizer_Water_Low
0 IVAR ^Bio_Index_Alarms_Primary_Air_Temp_Low
0 IVAR ^Bio_Index_Alarms_Quench_3_Temp_High
0 IVAR ^Bio_Index_Alarms_Quench_3_Water_High
0 IVAR ^Bio_Index_Alarms_Quench_3_Water_Low
0 IVAR ^Bio_Index_Alarms_Quench_4_Temp_High
0 IVAR ^Bio_Index_Alarms_Quench_4_Water_High
0 IVAR ^Bio_Index_Alarms_Quench_4_Water_Low
0 IVAR ^Bio_Index_Alarms_Scrubber_Temp_High
0 IVAR ^Bio_Index_Alarms_Scrubber_Water_High
0 IVAR ^Bio_Index_Alarms_Scrubber_Water_Low
0 IVAR ^Bio_Index_Alarms_Silo_1_Auger_Torque_High
0 IVAR ^Bio_Index_Alarms_Steam_Pressure_High
0 IVAR ^Bio_Index_Alarms_Steam_Pressure_Low
0 IVAR ^Bio_Index_Alarms_System_Estop
0 IVAR ^Bio_Index_Alarms_Topside_Blowdown_Ratio_High
0 IVAR ^Bio_Index_Alarms_Water_Storage_Tank_Low
0 IVAR ^Bio_Index_Feed
0 IVAR ^Bio_Index_Operation_Modes_Fuel_Saver
0 IVAR ^Bio_Index_Operation_Modes_Off
0 IVAR ^Bio_Index_Operation_Modes_Run
0 IVAR ^Bio_Index_Operation_Modes_Shutdown
0 IVAR ^Bio_Index_Operation_Modes_Startup
0 IVAR ^Bio_Index_VFD_Alarm
0 IVAR ^Bio_Index_VFD_Amps
0 IVAR ^Bio_Index_VFD_Capacitor_Life
0 IVAR ^Bio_Index_VFD_Frequency
0 IVAR ^Bio_Index_VFD_Heatsink_Life
0 IVAR ^Bio_Index_VFD_Heatsink_Temp
0 IVAR ^Bio_Index_VFD_PC_Board_Capacitor_Life
0 IVAR ^Bio_Index_VFD_Volts
0 IVAR ^Bio_Jam_Clear_Attemps
0 IVAR ^Bio_Momentary_Ash_Out
0 IVAR ^Bio_Momentary_B3_Low
0 IVAR ^Bio_Momentary_B4_Low
0 IVAR ^Bio_Momentary_Day_Bin_Low
0 IVAR ^Bio_Momentary_Moisturizer_High
0 IVAR ^Bio_Momentary_Quench_High
0 IVAR ^Bio_Momentary_Scrubber_High
0 IVAR ^Bio_Most_Severe_Alarm
0 IVAR ^Bio_Operation_Mode_Last
0 IVAR ^Bio_Operation_Mode_Modulate
0 IVAR ^Bio_Operation_Mode_Next
0 IVAR ^Bio_Operation_Mode_Override
0 FVAR ^Bio_Primary_Air_Flow
0 IVAR ^Bio_primary_max_speed
1073741824 IVAR ^Bio_Primary_Tripped_Counter
0 TVAR ^Bio_Pusher_Timer
0 FVAR ^Bio_Quench_3_Outlet_Temp
0 FVAR ^Bio_Quench_4_Outlet_Temp
0 FVAR ^Bio_Quench_Nozzle_Water_Pressure
0 FVAR ^Bio_Quench_Outlet_Pump_Cycle_Ratio
0 IVAR ^Bio_Quench_Output_1_active
0 FVAR ^Bio_Safety_Water_Pressure
0 IVAR ^Bio_Scratchpad_Status
0 FVAR ^Bio_Scrubber_DP_Level
0 FVAR ^Bio_Scrubber_Outlet_Pump_Cycle_Ratio
0 FVAR ^Bio_Scrubber_Outlet_Temp
0 FVAR ^Bio_Scrubber_Water_Level
0 FVAR ^Bio_Secondary_Air_Spray_1_Open_Ratio
0 FVAR ^Bio_Secondary_Air_Spray_2_Open_Ratio
0 FVAR ^Bio_Secondary_Air_Spray_3_Open_Ratio
0 FVAR ^Bio_Secondary_Air_Temp
0 IVAR ^Bio_Send_Email
0 IVAR ^Bio_Send_Email_Result
0 IVAR ^Bio_Setpoint_Above_Door_Spray
0 FVAR ^Bio_Setpoint_Air_Moisturizer_Inlet_Pump_Speed
0 FVAR ^Bio_Setpoint_Air_Moisturizer_Outlet_Pump_Speed
0 FVAR ^Bio_Setpoint_Day_Bin_Not_High_Time
0 IVAR ^Bio_Setpoint_Moisturizer_Out_Time
0 IVAR ^Bio_Setpoint_Moisturizer_Pumpout_Time
0 FVAR ^Bio_Setpoint_Ph_Level
0 FVAR ^Bio_Setpoint_Steam_Pressure
0 FVAR ^Bio_Setpoint_Water_Spray
0 IVAR ^Bio_Single_Condensate
0 FVAR ^Bio_Stack_Exhaust_O2_Level
0 IVAR ^Bio_State_Spray_Lance_PID
0 FVAR ^Bio_Steam_Flow_Total
0 IVAR ^Bio_Steam_Meter_Status1
0 IVAR ^Bio_System_Ash_Out
0 IVAR ^Bio_System_Both_Coaters_Running
0 IVAR ^Bio_System_Clear_Ash_Augers
0 IVAR ^Bio_System_Clear_Lift_Auger
0 IVAR ^Bio_System_Enabled_Feed_Screws
0 FVAR ^Bio_Tertiary_Air_Spray_Open_Ratio
0 FVAR ^Bio_Tertiary_Air_Temp
0 FVAR ^Bio_Time_Until_Day_Bin_Empty
0 TVAR ^Bio_Timer_15_Minute
0 TVAR ^Bio_Timer_20minute_day_bin
0 TVAR ^Bio_Timer_Ash_Removal
0 TVAR ^Bio_Timer_Day_Bin_Emptying
0 UTVAR ^Bio_Timer_Day_Bin_Filling
0 UTVAR ^Bio_Timer_Day_Bin_High
0 UTVAR ^Bio_Timer_Day_Bin_High_Disabled
0 UTVAR ^Bio_Timer_Day_Bin_Low
0 UTVAR ^Bio_Timer_DayBin_Not_High
0 UTVAR ^Bio_Timer_Feed_Per_15
0 UTVAR ^Bio_Timer_Feed_Screw_Period
0 TVAR ^Bio_Timer_Moisturizer_Adjust_Time
0 UTVAR ^Bio_Timer_Moisturizer_High
0 UTVAR ^Bio_Timer_Moisturizer_Out_OFF
0 UTVAR ^Bio_Timer_Moisturizer_Out_ON
0 UTVAR ^Bio_Timer_Quench_Outlet_OFF
0 UTVAR ^Bio_Timer_Quench_Outlet_ON
0 UTVAR ^Bio_Timer_Scrubber_Outlet_OFF
0 UTVAR ^Bio_Timer_Scrubber_Outlet_ON
0 UTVAR ^Bio_Timer_Since_Volume_Calculation
0 UTVAR ^Bio_Timer_Since_Volume_Storage
0 FVAR ^Bio_Topside_Blowdown_Cycle_Ratio
0 IVAR ^Bio_Total_Chip_Add_Amount
0 IVAR ^Bio_Total_Chip_Add_Btn
0 FVAR ^Bio_Total_Cu_Ft_Per_12_Hours
0 FVAR ^Bio_Total_Cu_Ft_Per_Hour
0 FVAR ^Bio_Total_Past_12_Hours
0 FVAR ^Bio_Total_Past_Hour
0 FVAR ^Bio_Trim_Primary_Air
0 FVAR ^Bio_Trim_Tertiary
0 IVAR ^Bio_VFD_Loop_Counter
0 IVAR ^Bio_Volume_Storage_Counter
0 IVAR ^Current_Day_Of_Year
0 IVAR ^Current_Hour_Of_Day
0 IVAR ^email_alarm_1_sent_flag
0 IVAR ^email_alarm_1_test_trigger
0 IVAR ^index
0 IVAR ^loop_counter
0 IVAR ^nDelay
0 IVAR ^nResult
0 IVAR ^status_trashcan
0 IVAR ^Tertiary_Low_Clamp_Running
0 IVAR ^Tertiary_Low_Clamp_Running_2_Coaters
20 0 $VAR *Bio_current_pump 
32 0 $VAR *Bio_Email_Server_Hostname 
4 0 $VAR *Bio_Email_Server_Port 
3 0 $VAR *Bio_Email_Server_Protocol 
32 0 $VAR *Bio_Operation_Mode_String 
50 0 $VAR *Bio_Steam_Meter_Readback1 
100 0 $VAR *Bio_System_Last_Db_Timestamp 
30 0 $VAR *Bio_Version 
2 0 $VAR *CRLF 
1024 0 $VAR *strErrorMessage 
1024 0 $VAR *strTempString 
1024 0 COMVAR *Bio_Steam_Meter_Serial_1 
1024 0 COMVAR *Modbus_Port 
64 0 ITABLE }Bio_Alarm_Delay
64 0 ITABLE }Bio_Alarm_Throw_Time
64 0 FTABLE }Bio_Alarm_Trigger_Points
64 0 ITABLE }Bio_Alarm_Types
64 0 ITABLE }Bio_Alarm_VFDs
64 0 ITABLE }Bio_Alarms
96 0 FTABLE }Bio_Fuel_Fed
64 0 ITABLE }Bio_Info
64 0 ITABLE }Bio_Info_Delay
10 0 FTABLE }Bio_Setpoints_Feed_Rate
10 0 FTABLE }Bio_Setpoints_Gasifier_Pressure
10 0 FTABLE }Bio_Setpoints_Moisturizer_Inlet
10 0 FTABLE }Bio_Setpoints_Primary_Air
10 0 FTABLE }Bio_Setpoints_Quench_Inlet
10 0 FTABLE }Bio_Setpoints_Scrubber_Inlet
10 0 FTABLE }Bio_Setpoints_Secondary_Air
10 0 FTABLE }Bio_Setpoints_Tertiary_Air
10 0 FTABLE }Bio_VFD_Data_Air_Moisturizer_Inlet_Pump
10 0 FTABLE }Bio_VFD_Data_Air_Moisturizer_Outlet_Pump
10 0 FTABLE }Bio_VFD_Data_Ash_Removal_Screws
10 0 FTABLE }Bio_VFD_Data_Belt_Conveyor
10 0 FTABLE }Bio_VFD_Data_Fuel_Feeder_Screw_1
10 0 FTABLE }Bio_VFD_Data_Fuel_Feeder_Screw_2
10 0 FTABLE }Bio_VFD_Data_Fuel_Feeder_Screw_3
10 0 FTABLE }Bio_VFD_Data_Fuel_Feeder_Screw_4
10 0 FTABLE }Bio_VFD_Data_Fuel_Feeder_Screw_5
10 0 FTABLE }Bio_VFD_Data_Lift_Auger
10 0 FTABLE }Bio_VFD_Data_Primary_Air_Fan
10 0 FTABLE }Bio_VFD_Data_Quench_Inlet_Pump
10 0 FTABLE }Bio_VFD_Data_Quench_Outlet_Pump_1
10 0 FTABLE }Bio_VFD_Data_Quench_Outlet_Pump_2
10 0 FTABLE }Bio_VFD_Data_Scrubber_Inlet_Pump
10 0 FTABLE }Bio_VFD_Data_Scrubber_Outlet_Pump
10 0 FTABLE }Bio_VFD_Data_Secondary_Air_Fan
10 0 FTABLE }Bio_VFD_Data_Silo_1_Augers
10 0 FTABLE }Bio_VFD_Data_Stack_Fan
10 0 FTABLE }Bio_VFD_Data_Tertiary_Air_Fan
64 0 ITABLE }Bio_Warning_Delay
64 0 ITABLE }Bio_Warnings
3000 0 FTABLE }VFD_Data_Float
3000 0 ITABLE }VFD_Data_Int
3 0 ITABLE }VFD_Port_Status
3000 0 ITABLE }VFD_Register_Data_Types
8 0 ITABLE }VFD_Setup
10 256 0 $TABLE {arrstrAttach 
100 1024 0 $TABLE {arrstrBody 
10 256 0 $TABLE {arrstrRecipients 
10 256 0 $TABLE {arrstrServer 
64 128 0 $TABLE {Bio_Alarm_Descriptions 
100 100 0 $TABLE {Bio_Email_Body 
10 100 0 $TABLE {Bio_Email_Recipients 
5 100 0 $TABLE {Bio_Email_Server_Info 
10 32 0 $TABLE {Bio_Operation_Mode_Strings 
1 30 0 $TABLE {database 
3 32 0 $TABLE {VFD_Serial_Status 
nullFVAR 0 POINTER PTR_Bio_Current_Alarm_Point_Analog
nullSPointInput 0 POINTER PTR_Bio_Current_Alarm_Point_Digital
nullFTABLE 0 POINTER PTR_Bio_Current_Alarm_Point_VFD
nullSPointOutput 0 POINTER PTR_Bio_Current_Quench_Outlet_Pump
nullFTABLE 0 POINTER PTR_Bio_Current_VFD_Data
64 0 PTABLE PTBL_Bio_Alarm_Points
30 0 PTABLE PTBL_Bio_VFD_Data
5 0 PTABLE PTBL_VFD_Data

nullM64BOARD $FFFFFFFE $FFFFFFFE 16 33024 10.000000 0.010000 0.000000 2001 $C0A801CD 0 11 BOARD.MMP %Bioler_Room
%Bioler_Room 25 BRD.TRIES
 4  1 SPOINT 0.0    OUTPUT  0  0 %Bioler_Room 0 POINT.TNG ~BA_Coater_1_ESTOP
 4  1 SPOINT 0.0    OUTPUT  1  0 %Bioler_Room 0 POINT.TNG ~BA_Coater_2_ESTOP
 4  1 SPOINT 0.0    OUTPUT  2  0 %Bioler_Room 0 POINT.TNG ~B1_diverter
 4  1 SPOINT 0.0    OUTPUT  3  0 %Bioler_Room 0 POINT.TNG ~B2_diverter
32  1 SPOINT 0.0     INPUT  0  3 %Bioler_Room 0 POINT.TNG ~BA_Coater_1_request_for_incineration
32  1 SPOINT 0.0     INPUT  1  3 %Bioler_Room 0 POINT.TNG ~BA_Coater_2_request_for_incineration
 4  1 SPOINT 0.0    OUTPUT  0  4 %Bioler_Room 0 POINT.TNG ~BA_Coater_1_clearance
 4  1 SPOINT 0.0    OUTPUT  1  4 %Bioler_Room 0 POINT.TNG ~BA_Coater_2_clearance
 4  1 SPOINT 0.0    OUTPUT  2  4 %Bioler_Room 0 POINT.TNG ~B1_enabled
 4  1 SPOINT 0.0    OUTPUT  3  4 %Bioler_Room 0 POINT.TNG ~B2_enabled
 8  1 APOINT -270.0000 8  7 12 %Bioler_Room 0 POINT.TNG ~Condensate_Tank_temp
nullM64BOARD $FFFFFFFE $FFFFFFFE 16 33024 1.000000 0.010000 0.000000 2001 $C0A80168 0 11 BOARD.MMP %Biomass_1
 4  1 APOINT  -37.5000 0  0  0 %Biomass_1 0 POINT.TNG ~Bio_Input_Stack_Exhaust_O2_Level_1
  25.0000  -37.5000 ~Bio_Input_Stack_Exhaust_O2_Level_1 SET.SCALE
 4  1 APOINT  -37.5000 0  1  0 %Biomass_1 0 POINT.TNG ~Bio_Input_Stack_Exhaust_O2_Level_2
  25.0000  -37.5000 ~Bio_Input_Stack_Exhaust_O2_Level_2 SET.SCALE
 4  1 APOINT   -7.5750 0  2  0 %Biomass_1 0 POINT.TNG ~Bio_Input_Bed_Height
   5.2000   -7.5750 ~Bio_Input_Bed_Height SET.SCALE
 4  1 APOINT  -20.0000 64  3  0 %Biomass_1 0 POINT.TNG ~Bio_Input_Air_Moisturizer_Inlet_Water_Flow
 8  1 APOINT -270.0000 8  0  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Above_Door_Temp
 8  1 APOINT -270.0000 8  1  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Gasifier_Temp_3
 8  1 APOINT -270.0000 8  2  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Steam_Temp
 8  1 APOINT -270.0000 8  4  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Gasifier_Top_Temp_1
 8  1 APOINT -270.0000 8  6  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Ash_Temp_Left
 8  1 APOINT -270.0000 8  7  1 %Biomass_1 0 POINT.TNG ~Bio_Input_Ash_Temp_Right
 8  1 APOINT -270.0000 8  2  2 %Biomass_1 0 POINT.TNG ~Bio_Input_Ceiling_Temp
 8  1 APOINT -270.0000 8  5  2 %Biomass_1 0 POINT.TNG ~Bio_Input_Air_Moisturizer_Air_Outlet_Temp
 8  1 APOINT -270.0000 8  6  2 %Biomass_1 0 POINT.TNG ~Bio_Input_Air_Moisturizer_Water_Temp
 8  1 APOINT -270.0000 8  7  2 %Biomass_1 0 POINT.TNG ~Bio_Input_Air_Super_Heater_Outlet_Temp_1
 8  1 APOINT -270.0000 8  0  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_3_Outlet_Temp
 8  1 APOINT -270.0000 8  2  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_3_Outlet_Temp_1
 8  1 APOINT -270.0000 8  3  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_3_Outlet_Temp_2
 8  1 APOINT -270.0000 8  4  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_4_Outlet_Temp_1
 8  1 APOINT -270.0000 8  5  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_4_Outlet_Temp_2
 8  1 APOINT -270.0000 8  6  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Scrubber_Outlet_Temp_1
 8  1 APOINT -270.0000 8  7  3 %Biomass_1 0 POINT.TNG ~Bio_Input_Scrubber_Outlet_Temp_2
32  1 APOINT   15.0168 0  0  4 %Biomass_1 0 POINT.TNG ~Bio_Input_PH_Level
 -20.4128   15.0168 ~Bio_Input_PH_Level SET.SCALE
32  1 APOINT    0.5000 0  1  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Gasifier_Pressure
  -2.0000    0.5000 ~Bio_Input_Gasifier_Pressure SET.SCALE
32  1 APOINT  100.0000 0  2  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_Steam_Pressure_1
-150.0000  100.0000 ~Bio_Input_Boiler_Steam_Pressure_1 SET.SCALE
32  1 APOINT  100.0000 0  3  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_Steam_Pressure_2
-150.0000  100.0000 ~Bio_Input_Boiler_Steam_Pressure_2 SET.SCALE
32  1 APOINT  100.0000 0  4  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_Inlet_Water_Pressure
-150.0000  100.0000 ~Bio_Input_Quench_Inlet_Water_Pressure SET.SCALE
32  1 APOINT  100.0000 0  5  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Scrubber_Inlet_Water_Pressure
-150.0000  100.0000 ~Bio_Input_Scrubber_Inlet_Water_Pressure SET.SCALE
32  1 APOINT  100.0000 0  6  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Safety_Water_Pressure
-150.0000  100.0000 ~Bio_Input_Safety_Water_Pressure SET.SCALE
32  1 APOINT    0.2500 0  8  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Primary_Air_Flow
  -0.3750    0.2500 ~Bio_Input_Primary_Air_Flow SET.SCALE
32  1 APOINT   25.0000 0  9  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Scrubber_Water_Level
 -37.5000   25.0000 ~Bio_Input_Scrubber_Water_Level SET.SCALE
32  1 APOINT   25.0000 0 10  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Quench_Water_Level
 -37.5000   25.0000 ~Bio_Input_Quench_Water_Level SET.SCALE
32  1 APOINT   25.0000 0 11  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Air_Moisturizer_Level
 -37.5000   25.0000 ~Bio_Input_Air_Moisturizer_Level SET.SCALE
32  1 APOINT  100.0000 0 12  4 %Biomass_1 0 POINT.TNG ~Bio_Input_Hot_Water_Storage_Tank_Level
-150.0000  100.0000 ~Bio_Input_Hot_Water_Storage_Tank_Level SET.SCALE
32  1 APOINT 10000.0000 0 13  4 %Biomass_1 0 POINT.TNG ~Bio_Steam_Flow_1
-15000.0000 10000.0000 ~Bio_Steam_Flow_1 SET.SCALE
32  1 APOINT 10000.0000 0 14  4 %Biomass_1 0 POINT.TNG ~Bio_Steam_Flow_2
-15000.0000 10000.0000 ~Bio_Steam_Flow_2 SET.SCALE
32  1 APOINT 10000.0000 0 15  4 %Biomass_1 0 POINT.TNG ~Bio_Steam_Flow_3
-15000.0000 10000.0000 ~Bio_Steam_Flow_3 SET.SCALE
32  1 SPOINT 0.0     INPUT  0  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Incineration_Diverter_Closed_Switch
32  1 SPOINT 0.0     INPUT  1  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Incineration_Diverter_Open_Switch
32  1 SPOINT 0.0     INPUT  2  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_Screw_3_VFD_Error
32  1 SPOINT 0.0     INPUT  3  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_Screw_4_VFD_Error
32  1 SPOINT 0.0     INPUT  4  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_Screw_5_VFD_Error
32  1 SPOINT 0.0     INPUT  5  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Day_Bin_High
32  1 SPOINT 0.0     INPUT  6  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Day_Bin_Low
32  1 SPOINT 0.0     INPUT  7  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Secondary_Water_Flow_1
32  1 SPOINT 0.0     INPUT  8  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Secondary_Water_Flow_2
32  1 SPOINT 0.0     INPUT  9  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Estop_Feedback
32  1 SPOINT 0.0     INPUT 10  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_3_Water_High
32  1 SPOINT 0.0     INPUT 11  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_3_Water_Low
32  1 SPOINT 0.0     INPUT 12  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_4_Water_High
32  1 SPOINT 0.0     INPUT 13  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_4_Water_Low
32  1 SPOINT 0.0     INPUT 14  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_3_Over_Pressure
32  1 SPOINT 0.0     INPUT 15  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_4_Over_Pressure
32  1 SPOINT 0.0     INPUT 16  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_3_Water_Critical
32  1 SPOINT 0.0     INPUT 17  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Boiler_4_Water_Critical
32  1 SPOINT 0.0   COUNTER 18  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Steam_Flow_1_Pulse
32  1 SPOINT 0.0   COUNTER 19  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Steam_Flow_2_Pulse
32  1 SPOINT 0.0   COUNTER 20  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Steam_Flow_3_Pulse
32  1 SPOINT 0.0     INPUT 23  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_System_Estop_Feedback
32  1 SPOINT 0.0     INPUT 24  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Secondary_Water_Flow_3
32  1 SPOINT 0.0     INPUT 25  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Tertiary_Water_Flow
32  1 SPOINT 0.0     INPUT 26  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Primary_VFD_Error
32  1 SPOINT 0.0     INPUT 27  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Condensate1_VFD_Error
32  1 SPOINT 0.0     INPUT 28  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Condensate2_VFD_Error
32  1 SPOINT 0.0     INPUT 29  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Stack_VFD_Error
32  1 SPOINT 0.0     INPUT 30  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_Screw_1_VFD_Error
32  1 SPOINT 0.0     INPUT 31  5 %Biomass_1 0 POINT.TNG ~Bio_Input_Feed_Screw_2_VFD_Error
32  1 SPOINT 0.0    OUTPUT  0  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_1_Reverse
32  1 SPOINT 0.0    OUTPUT  1  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_2_Reverse
32  1 SPOINT 0.0    OUTPUT  2  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_3_Reverse
32  1 SPOINT 0.0    OUTPUT  3  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_4_Reverse
32  1 SPOINT 0.0    OUTPUT  4  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_5_Reverse
32  1 SPOINT 0.0    OUTPUT  5  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_1_Forward
32  1 SPOINT 0.0    OUTPUT  6  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_2_Forward
32  1 SPOINT 0.0    OUTPUT  7  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_3_Forward
32  1 SPOINT 0.0    OUTPUT  8  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_4_Forward
32  1 SPOINT 0.0    OUTPUT  9  6 %Biomass_1 0 POINT.TNG ~Bio_Fuel_Feeder_Screw_5_Forward
32  1 SPOINT 0.0    OUTPUT 10  6 %Biomass_1 0 POINT.TNG ~Bio_Silo_1_Augers_Forward
32  1 SPOINT 0.0    OUTPUT 11  6 %Biomass_1 0 POINT.TNG ~Bio_Silo_1_Augers_Reverse
32  1 SPOINT 0.0    OUTPUT 12  6 %Biomass_1 0 POINT.TNG ~Bio_Lift_Auger_Reverse
32  1 SPOINT 0.0    OUTPUT 13  6 %Biomass_1 0 POINT.TNG ~Bio_Lift_Auger_Forward
32  1 SPOINT 0.0    OUTPUT 14  6 %Biomass_1 0 POINT.TNG ~Bio_Belt_Conveyor_On_Off
32  1 SPOINT 0.0    OUTPUT 16  6 %Biomass_1 0 POINT.TNG ~Bio_Air_Moisturizer_Inlet_Pump_Motor_On_Off
32  1 SPOINT 0.0    OUTPUT 17  6 %Biomass_1 0 POINT.TNG ~Bio_Air_Moisturizer_Outlet_Pump_Motor_On_Off
32  1 SPOINT 0.0    OUTPUT 18  6 %Biomass_1 0 POINT.TNG ~Bio_Tertiary_Air_Fan_On_Off
32  1 SPOINT 0.0    OUTPUT 19  6 %Biomass_1 0 POINT.TNG ~Bio_Ventilation_Fan_On_Off
32  1 SPOINT 0.0    OUTPUT 20  6 %Biomass_1 0 POINT.TNG ~Bio_Secondary_Air_Fan_Motor_On_Off
32  1 SPOINT 0.0    OUTPUT 21  6 %Biomass_1 0 POINT.TNG ~Bio_Primary_Air_Fan_Motor_On_Off
32  1 SPOINT 0.0    OUTPUT 22  6 %Biomass_1 0 POINT.TNG ~Bio_Boiler_4_Condensate_Pump_On_Off
32  1 SPOINT 0.0    OUTPUT 23  6 %Biomass_1 0 POINT.TNG ~Bio_Quench_Outlet_Pump_2_On_Off
32  1 SPOINT 0.0    OUTPUT 24  6 %Biomass_1 0 POINT.TNG ~Bio_Scrubber_Outlet_Pump_On_Off
32  1 SPOINT 0.0    OUTPUT 25  6 %Biomass_1 0 POINT.TNG ~Bio_Boiler_3_Condensate_Pump_On_Off
32  1 SPOINT 0.0    OUTPUT 27  6 %Biomass_1 0 POINT.TNG ~Bio_Quench_Inlet_Pump_On_Off
32  1 SPOINT 0.0    OUTPUT 28  6 %Biomass_1 0 POINT.TNG ~Bio_Scrubber_Inlet_Pump_On_Off
32  1 SPOINT 0.0    OUTPUT 29  6 %Biomass_1 0 POINT.TNG ~Bio_Stack_Fan_Motor_On_Off
32  1 SPOINT 0.0    OUTPUT 30  6 %Biomass_1 0 POINT.TNG ~Bio_Ash_Removal_Screws_Forward
32  1 SPOINT 0.0    OUTPUT 31  6 %Biomass_1 0 POINT.TNG ~Bio_Ash_Removal_Screws_Reverse
 2  1 APOINT    0.0000 128  0  7 %Biomass_1 0 POINT.TNG ~Bio_Stack_Fan_Motor_Speed
 100.0000    0.0000 ~Bio_Stack_Fan_Motor_Speed SET.SCALE
 2  1 APOINT    0.0000 128  1  7 %Biomass_1 0 POINT.TNG ~Bio_Tertiary_Air_Fan_Speed
 100.0000    0.0000 ~Bio_Tertiary_Air_Fan_Speed SET.SCALE
 2  1 APOINT    0.0000 128  0  8 %Biomass_1 0 POINT.TNG ~Bio_Air_Moisturizer_Inlet_Pump_Speed
 100.0000    0.0000 ~Bio_Air_Moisturizer_Inlet_Pump_Speed SET.SCALE
 2  1 APOINT    0.0000 128  1  8 %Biomass_1 0 POINT.TNG ~Bio_Air_Moisturizer_Outlet_Pump_Speed
 100.0000    0.0000 ~Bio_Air_Moisturizer_Outlet_Pump_Speed SET.SCALE
 2  1 APOINT    0.0000 128  0 10 %Biomass_1 0 POINT.TNG ~Bio_Secondary_Air_Fan_Motor_Speed
 100.0000    0.0000 ~Bio_Secondary_Air_Fan_Motor_Speed SET.SCALE
 2  1 APOINT    0.0000 128  1 10 %Biomass_1 0 POINT.TNG ~Bio_Primary_Air_Fan_Motor_Speed
 100.0000    0.0000 ~Bio_Primary_Air_Fan_Motor_Speed SET.SCALE
 2  1 APOINT    0.0000 128  1 13 %Biomass_1 0 POINT.TNG ~Bio_Quench_Inlet_Pump_Speed
 100.0000    0.0000 ~Bio_Quench_Inlet_Pump_Speed SET.SCALE
 2  1 APOINT    0.0000 128  0 14 %Biomass_1 0 POINT.TNG ~Bio_Scrubber_Inlet_Pump_Speed
 100.0000    0.0000 ~Bio_Scrubber_Inlet_Pump_Speed SET.SCALE
32  1 SPOINT 0.0    OUTPUT  0 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_3_Safety_Water_Valve
32  1 SPOINT 0.0    OUTPUT  1 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_4_Safety_Water_Valve
32  1 SPOINT 0.0    OUTPUT  2 15 %Biomass_1 0 POINT.TNG ~Bio_Scrubber_Safety_Water_Valve
32  1 SPOINT 0.0    OUTPUT  3 15 %Biomass_1 0 POINT.TNG ~Bio_Incineration_Diverter
32  1 SPOINT 0.0    OUTPUT  4 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_3_Nozzle_1
32  1 SPOINT 0.0    OUTPUT  5 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_3_Nozzle_2
32  1 SPOINT 0.0    OUTPUT  6 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_3_Nozzle_3
32  1 SPOINT 0.0    OUTPUT  7 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_3_Nozzle_4
32  1 SPOINT 0.0    OUTPUT  8 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_4_Nozzle_1
32  1 SPOINT 0.0    OUTPUT  9 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_4_Nozzle_2
32  1 SPOINT 0.0    OUTPUT 10 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_4_Nozzle_3
32  1 SPOINT 0.0    OUTPUT 11 15 %Biomass_1 0 POINT.TNG ~Bio_Quench_4_Nozzle_4
32  1 SPOINT 0.0    OUTPUT 12 15 %Biomass_1 0 POINT.TNG ~Bio_Incineration_Enabled
32  1 SPOINT 0.0    OUTPUT 13 15 %Biomass_1 0 POINT.TNG ~Bio_Water_Storage_Tank_Fill_Valve
32  1 SPOINT 0.0    OUTPUT 14 15 %Biomass_1 0 POINT.TNG ~Bio_Topside_Blowdown_1
32  1 SPOINT 0.0    OUTPUT 15 15 %Biomass_1 0 POINT.TNG ~Bio_Topside_Blowdown_2
32  1 SPOINT 0.0       TPO 16 15 %Biomass_1 0 POINT.TNG ~Bio_Day_Bin_Spray
32  1 SPOINT 0.0    OUTPUT 21 15 %Biomass_1 0 POINT.TNG ~Bio_Air_Moisturizer_Outlet_Pump
32  1 SPOINT 0.0    OUTPUT 22 15 %Biomass_1 0 POINT.TNG ~Bio_Air_Super_Heater_Boost_Steam_Valve
32  1 SPOINT 0.0    OUTPUT 23 15 %Biomass_1 0 POINT.TNG ~Bio_Ph_Pump
32  1 SPOINT 0.0    OUTPUT 24 15 %Biomass_1 0 POINT.TNG ~Bio_Yellow_Light
32  1 SPOINT 0.0    OUTPUT 25 15 %Biomass_1 0 POINT.TNG ~Bio_Red_Light
32  1 SPOINT 0.0    OUTPUT 26 15 %Biomass_1 0 POINT.TNG ~Bio_Green_Light
32  1 SPOINT 0.0    OUTPUT 27 15 %Biomass_1 0 POINT.TNG ~Bio_Audible_Alarm
32  1 SPOINT 0.0       TPO 28 15 %Biomass_1 0 POINT.TNG ~Bio_Tertiary_Air_Spray
32  1 SPOINT 0.0       TPO 29 15 %Biomass_1 0 POINT.TNG ~Bio_Secondary_Air_Spray_1
32  1 SPOINT 0.0       TPO 30 15 %Biomass_1 0 POINT.TNG ~Bio_Secondary_Air_Spray_2
32  1 SPOINT 0.0       TPO 31 15 %Biomass_1 0 POINT.TNG ~Bio_Secondary_Air_Spray_3
nullM64BOARD $FFFFFFFE $FFFFFFFE 16 33024 1.000000 0.010000 0.000000 2001 $C0A80167 0 11 BOARD.MMP %Biomass_2
 8  1 APOINT -270.0000 8  0  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Boiler_3_Inlet_Temp_1
 8  1 APOINT -270.0000 8  1  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Boiler_3_Inlet_Temp_2
 8  1 APOINT -270.0000 8  2  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Boiler_4_Inlet_Temp_1
 8  1 APOINT -270.0000 8  3  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Boiler_4_Inlet_Temp_2
 8  1 APOINT -270.0000 8  4  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_4
 8  1 APOINT -270.0000 8  5  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_5
 8  1 APOINT -270.0000 8  6  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_6
 8  1 APOINT -270.0000 8  7  1 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_7
 8  1 APOINT -270.0000 8  0  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Air_Moisturizer_Water_Inlet_Temp
 8  1 APOINT -270.0000 8  1  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Air_Super_Heater_Outlet_Temp_2
 8  1 APOINT -270.0000 8  2  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Secondary_Air_Temp_1
 8  1 APOINT -270.0000 8  3  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Secondary_Air_Temp_2
 8  1 APOINT -270.0000 8  5  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Top_Temp_2
 8  1 APOINT -270.0000 8  6  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_2
 8  1 APOINT -270.0000 8  7  2 %Biomass_2 0 POINT.TNG ~Bio_Input_Gasifier_Temp_1
 8  1 APOINT -270.0000 8  0  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Tertiary_Air_Temp_1
 8  1 APOINT -270.0000 8  1  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Tertiary_Air_Temp_2
 8  1 APOINT -270.0000 8  2  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Boiler_4_Exit_Temp
 8  1 APOINT -270.0000 8  3  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Feed_Auger_1_Temp
 8  1 APOINT -270.0000 8  4  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Feed_Auger_2_Temp
 8  1 APOINT -270.0000 8  5  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Feed_Auger_3_Temp
 8  1 APOINT -270.0000 8  6  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Feed_Auger_4_Temp
 8  1 APOINT -270.0000 8  7  3 %Biomass_2 0 POINT.TNG ~Bio_Input_Feed_Auger_5_Temp
 4  1 APOINT -150.0000 0  0  4 %Biomass_2 0 POINT.TNG ~Bio_Temp_PPM
 100.0000 -150.0000 ~Bio_Temp_PPM SET.SCALE
 4  1 APOINT  -20.0000 64  1  4 %Biomass_2 0 POINT.TNG ~ai_Biomass_2_0401
 4  1 APOINT  -20.0000 64  2  4 %Biomass_2 0 POINT.TNG ~ai_Biomass_2_0402
 4  1 APOINT  -20.0000 64  3  4 %Biomass_2 0 POINT.TNG ~ai_Biomass_2_0403
32  1 SPOINT 0.0    OUTPUT  0  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1
32  1 SPOINT 0.0    OUTPUT  1  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0501
32  1 SPOINT 0.0    OUTPUT  2  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0502
32  1 SPOINT 0.0    OUTPUT  3  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0503
32  1 SPOINT 0.0    OUTPUT  4  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0504
32  1 SPOINT 0.0    OUTPUT  5  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0505
32  1 SPOINT 0.0    OUTPUT  6  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0506
32  1 SPOINT 0.0    OUTPUT  7  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0507
32  1 SPOINT 0.0    OUTPUT  8  5 %Biomass_2 0 POINT.TNG ~Bio_Pusher_Extend
32  1 SPOINT 0.0    OUTPUT  9  5 %Biomass_2 0 POINT.TNG ~Bio_Air_Blast_Sensor_Clean
32  1 SPOINT 0.0    OUTPUT 10  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0510
32  1 SPOINT 0.0    OUTPUT 11  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0511
32  1 SPOINT 0.0    OUTPUT 12  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0512
32  1 SPOINT 0.0    OUTPUT 13  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0513
32  1 SPOINT 0.0    OUTPUT 14  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0514
32  1 SPOINT 0.0    OUTPUT 15  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0515
32  1 SPOINT 0.0    OUTPUT 16  5 %Biomass_2 0 POINT.TNG ~Bio_Reset_Primary_VFD
32  1 SPOINT 0.0    OUTPUT 17  5 %Biomass_2 0 POINT.TNG ~Bio_Reset_Condensate1_VFD
32  1 SPOINT 0.0    OUTPUT 18  5 %Biomass_2 0 POINT.TNG ~Bio_Reset_Condensate2_VFD
32  1 SPOINT 0.0    OUTPUT 19  5 %Biomass_2 0 POINT.TNG ~Bio_Reset_Stack_VFD
32  1 SPOINT 0.0    OUTPUT 20  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0520
32  1 SPOINT 0.0    OUTPUT 21  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0521
32  1 SPOINT 0.0    OUTPUT 22  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0522
32  1 SPOINT 0.0    OUTPUT 23  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0523
32  1 SPOINT 0.0    OUTPUT 24  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0524
32  1 SPOINT 0.0    OUTPUT 25  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0525
32  1 SPOINT 0.0    OUTPUT 26  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0526
32  1 SPOINT 0.0    OUTPUT 27  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0527
32  1 SPOINT 0.0    OUTPUT 28  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0528
32  1 SPOINT 0.0    OUTPUT 29  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0529
32  1 SPOINT 0.0    OUTPUT 30  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0530
32  1 SPOINT 0.0    OUTPUT 31  5 %Biomass_2 0 POINT.TNG ~Bio_Relay1_0531
 4  1 SPOINT 0.0     INPUT  0  6 %Biomass_2 0 POINT.TNG ~Bio_Input_Conveyor_Blockage1
 4  1 SPOINT 0.0     INPUT  1  6 %Biomass_2 0 POINT.TNG ~Bio_Input_Conveyor_Blockage2
 4  1 SPOINT 0.0     INPUT  2  6 %Biomass_2 0 POINT.TNG ~Bio_Input_Daybin_High_PE
1   60.0000    5.0000    0.0000    0.0000    5.0000   60.0000    1.2000    5.0000
  10.0000    0.0000    0.0000   -1.0000    0.0100   20.0000
   2.8000 ~Bio_Input_Bed_Height 0.0
$d0000002 0 %Biomass_1 0 PID |Feed_Rate
1   10.0000    0.0000    0.0000    0.0000    0.0000  100.0000   -0.5000    0.5000
   1.0000    0.0000    0.0000    0.7500    0.0800    1.0000
  -0.0500    0.0000 0.0
$f0000000 1 %Biomass_1 0 PID |Gasifier_Pressure
1    0.0000   50.0000    0.0000    0.0000    0.0000   50.0000 1550.0000 2200.0000
  10.0000    0.0000    0.0000    0.5000    0.0100    0.5000
   0.0000 1800.0000 0.0
$f0000002 12 %Biomass_1 0 PID |PID_Secondary_Center_Lance
1    0.0000   40.0000    0.0000    0.0000    0.0000   40.0000 1550.0000 2200.0000
  10.0000    0.0000    0.0000    0.2000    0.0100    0.1000
   0.0000 1800.0000 0.0
$f0000002 13 %Biomass_1 0 PID |PID_Secondary_Outer_Lances
1    0.0000   50.0000    0.0000    0.0000    0.0000   50.0000 1550.0000 2100.0000
  10.0000    0.0000    0.0000    3.0000    0.0100    1.5000
   0.0000 1800.0000 0.0
$f0000002 11 %Biomass_1 0 PID |PID_Tertiary_Lance
2   10.0000   10.0000    0.0000    0.0000   10.0000  100.0000    0.0000   40.0000
  15.0000    0.0000    0.0000   -4.0000    0.0015  100.0000
  30.0000    0.0000 0.0
$f0000002 2 %Biomass_1 0 PID |Primary_Air
1    0.0000    0.0000    0.0000    0.0000   10.0000   80.0000    0.0000  900.0000
   1.0000    0.0000    0.0000    1.0000    0.0100    0.0000
 300.0000 ~Bio_Input_Air_Super_Heater_Outlet_Temp_1 0.0
$d0000000 9 %Biomass_1 0 PID |Primary_Air_Low_Clamp
1    0.0000    0.0000    0.0000    2.0000    0.0000  100.0000    0.0000  300.0000
   2.0000    0.0000    0.0000   10.0000    0.0100    0.0100
 212.0000    0.0000 ~Bio_Quench_Inlet_Pump_Speed
$b0000000 3 %Biomass_1 0 PID |Quench_Water_Inlet_Flow
1  100.0000    0.0000    0.0000    0.0000    0.0000  100.0000   -5.0000    5.0000
   5.0000    0.0000    0.0000   -1.0000    0.0500    5.0000
   0.2500 ~Bio_Input_Scrubber_Water_Level ~Bio_Scrubber_Inlet_Pump_Speed
$90000002 5 %Biomass_1 0 PID |Scrubber_Inlet_PID
1    0.0000    0.0000    0.0000    1.0000   10.0000  100.0000    0.0000 2500.0000
   3.0000    0.0000    0.0000   -0.2500    0.5000    0.0000
1500.0000    0.0000 0.0
$f0000000 4 %Biomass_1 0 PID |Secondary_Air
1    0.0000    0.0000    0.0000    0.0000   10.0000  100.0000    0.0000  500.0000
   1.0000    0.0000    0.0000    1.0000    0.0100    0.0000
 300.0000    0.0000 0.0
$f0000000 8 %Biomass_1 0 PID |Secondary_Air_Low_Clamp
1   10.0000   40.0000    0.0000    0.0000   10.0000   40.0000    0.0000 2100.0000
   2.0000    0.0000    0.0000   15.0000    0.0150    1.0000
1900.0000    0.0000 0.0
$f0000002 6 %Biomass_1 0 PID |Tertiary_Air
1    0.0000    0.0000    0.0000    0.0000    0.0000   25.0000    0.0000  500.0000
   1.0000    0.0000    0.0000    5.0000    0.1500    0.0000
 250.0000    0.0000 0.0
$f0000000 7 %Biomass_1 0 PID |Tertiary_Air_Low_Clamp
: 23_0
1 JUMP ;
: 23_294
  1.0    FDELAY
-2 JUMP ;
: 23_327
  -1  
  ^loop_counter @!   
  0  
  ^Bio_Most_Severe_Alarm @!   
  0  
  ^Bio_Alarm_Count @!   
0 JUMP ;
: 23_328
  ^loop_counter   1+@!
2 JUMP ;
: 23_590
^Bio_Send_Email @@ IF 
" mmazzantini@warwickmills.com" 0 {Bio_Email_Server_Info $TABLE@  $MOVE 
" mike240i" 1 {Bio_Email_Server_Info $TABLE@  $MOVE 
" smtp.gmail.com" *Bio_Email_Server_Hostname  $MOVE 
" 587" *Bio_Email_Server_Port  $MOVE 
" TLS" *Bio_Email_Server_Protocol  $MOVE 
*Bio_Email_Server_Hostname 2 {Bio_Email_Server_Info $TABLE@  $MOVE 
*Bio_Email_Server_Port 3 {Bio_Email_Server_Info $TABLE@  $MOVE 
*Bio_Email_Server_Protocol 4 {Bio_Email_Server_Info $TABLE@  $MOVE 
" physicsmazz@gmail.com" 0 {Bio_Email_Recipients $TABLE@  $MOVE 
" " 1 {Bio_Email_Recipients $TABLE@  $MOVE 
" Test Email" 0 {Bio_Email_Body $TABLE@  $MOVE 
" Testing..." 1 {Bio_Email_Body $TABLE@  $MOVE 
" " 2 {Bio_Email_Body $TABLE@  $MOVE 
 0 {Bio_Email_Server_Info  {Bio_Email_Recipients {Bio_Email_Body  SMTP.SEND ^Bio_Send_Email_Result @! 
0 ^Bio_Send_Email @! 
THEN 
-4 JUMP ;
: 23_645
^Bio_Operation_Mode @@ 0 = ^Bio_Most_Severe_Alarm @@ 0 = LOR  IF 
~Bio_Green_Light  ON 
~Bio_Yellow_Light  OFF 
~Bio_Red_Light  OFF 
ELSE 
^Bio_Most_Severe_Alarm @@ 1 = IF 
~Bio_Green_Light  OFF 
~Bio_Yellow_Light  ON 
~Bio_Red_Light  OFF 
ELSE 
^Bio_Most_Severe_Alarm @@ 2 = IF 
~Bio_Green_Light  OFF 
~Bio_Yellow_Light  OFF 
~Bio_Red_Light  ON 
THEN THEN THEN 
0 }Bio_Alarms TABLE@ 1 }Bio_Alarms TABLE@ LOR  2 }Bio_Alarms TABLE@ LOR  3 }Bio_Alarms TABLE@ LOR  41 }Bio_Alarms TABLE@ LOR  42 }Bio_Alarms TABLE@ LOR  IF 
~Bio_Audible_Alarm  ON 
ELSE 
~Bio_Audible_Alarm  OFF 
THEN 
-2 JUMP ;
: 23_653
0 ^Bio_Alarm_Triggered @! 
^loop_counter @@ }Bio_Alarm_Types TABLE@ ^Bio_Index_Alarm_Type_Digital @@ = IF 
^loop_counter @@ PTBL_Bio_Alarm_Points 
TableMoveToPointer PTR_Bio_Current_Alarm_Point_Digital 
PTR_Bio_Current_Alarm_Point_Digital @@I I>F ^loop_counter @@ }Bio_Alarm_Trigger_Points TABLE@ F= IF 
1 ^Bio_Alarm_Triggered @! 
THEN 
ELSE 
^loop_counter @@ }Bio_Alarm_Types TABLE@ ^Bio_Index_Alarm_Type_Analog_High @@ = IF 
^loop_counter @@ PTBL_Bio_Alarm_Points 
TableMoveToPointer PTR_Bio_Current_Alarm_Point_Analog 
PTR_Bio_Current_Alarm_Point_Analog @@ ^loop_counter @@ }Bio_Alarm_Trigger_Points TABLE@ F> IF 
1 ^Bio_Alarm_Triggered @! 
THEN 
ELSE 
^loop_counter @@ }Bio_Alarm_Types TABLE@ ^Bio_Index_Alarm_Type_Analog_Low @@ = IF 
^loop_counter @@ PTBL_Bio_Alarm_Points 
TableMoveToPointer PTR_Bio_Current_Alarm_Point_Analog 
PTR_Bio_Current_Alarm_Point_Analog @@ ^loop_counter @@ }Bio_Alarm_Trigger_Points TABLE@ F< IF 
1 ^Bio_Alarm_Triggered @! 
THEN 
ELSE 
^loop_counter @@ }Bio_Alarm_Types TABLE@ ^Bio_Index_Alarm_Type_Torque @@ = IF 
^loop_counter @@ }Bio_Alarm_VFDs TABLE@ PTBL_Bio_VFD_Data 
TableMoveToPointer PTR_Bio_Current_Alarm_Point_VFD 
2 PTR_Bio_Current_Alarm_Point_VFD TABLE@ ^loop_counter @@ }Bio_Alarm_Trigger_Points TABLE@ F> IF 
1 ^Bio_Alarm_Triggered @! 
THEN 
ELSE 
^loop_counter @@ }Bio_Alarm_Types TABLE@ ^Bio_Index_Alarm_Type_Custom @@ = IF 
THEN THEN THEN THEN THEN 
^Bio_Alarm_Triggered @@ IF 
^loop_counter @@ }Bio_Alarm_Throw_Time TABLE@ 0 = IF 
 GetSystemTime ^loop_counter @@ }Bio_Alarm_Throw_Time TABLE! 
THEN 
^loop_counter @@ }Bio_Alarm_Delay TABLE@ -1 <>  GetSystemTime ^loop_counter @@ }Bio_Alarm_Throw_Time TABLE@ ^loop_counter @@ }Bio_Alarm_Delay TABLE@ +  > LAND  IF 
1 ^loop_counter @@ }Bio_Alarms TABLE! 
0 ^loop_counter @@ }Bio_Warnings TABLE! 
0 ^loop_counter @@ }Bio_Info TABLE! 
^Bio_Most_Severe_Alarm @@ 2 < IF 
2 ^Bio_Most_Severe_Alarm @! 
THEN 
^Bio_Alarm_Count @@ 1 +  ^Bio_Alarm_Count @! 
ELSE 
^loop_counter @@ }Bio_Warning_Delay TABLE@ -1 <>  GetSystemTime ^loop_counter @@ }Bio_Alarm_Throw_Time TABLE@ ^loop_counter @@ }Bio_Warning_Delay TABLE@ +  > LAND  IF 
0 ^loop_counter @@ }Bio_Alarms TABLE! 
1 ^loop_counter @@ }Bio_Warnings TABLE! 
0 ^loop_counter @@ }Bio_Info TABLE! 
^Bio_Most_Severe_Alarm @@ 1 < IF 
1 ^Bio_Most_Severe_Alarm @! 
THEN 
ELSE 
^loop_counter @@ }Bio_Info_Delay TABLE@ -1 <>  GetSystemTime ^loop_counter @@ }Bio_Alarm_Throw_Time TABLE@ ^loop_counter @@ }Bio_Info_Delay TABLE@ +  > LAND  IF 
0 ^loop_counter @@ }Bio_Alarms TABLE! 
0 ^loop_counter @@ }Bio_Warnings TABLE! 
1 ^loop_counter @@ }Bio_Info TABLE! 
ELSE 
0 ^loop_counter @@ }Bio_Alarms TABLE! 
0 ^loop_counter @@ }Bio_Warnings TABLE! 
0 ^loop_counter @@ }Bio_Info TABLE! 
THEN THEN THEN 
ELSE 
0 ^loop_counter @@ }Bio_Alarms TABLE! 
0 ^loop_counter @@ }Bio_Warnings TABLE! 
0 ^loop_counter @@ }Bio_Info TABLE! 
0 ^loop_counter @@ }Bio_Alarm_Throw_Time TABLE! 
THEN 
0 JUMP ;
: 23_624
TRUE
  ^loop_counter @@ 
  64    <=
LAND
IF -5 ELSE -3 THEN JUMP ;
T: T23
DUMMY
23_0
23_294
23_327
23_328
23_590
23_645
23_653
23_624
T;
&Alarms ' T23 SETTASK
: 77_0
0 JUMP ;
: 77_1
  ~Bio_Ash_Removal_Screws_Forward   ON
  ^Bio_Delay_Ash_Removal_Time @@   FDELAY
  ~Bio_Ash_Removal_Screws_Forward   OFF
  ^Bio_Timer_Ash_Removal   StartTimer
0 JUMP ;
T: T77
DUMMY
77_0
77_1
T;
&Ash_Out ' T77 SETTASK
: 67_0
3 JUMP ;
: 67_5
  ~Bio_Topside_Blowdown_1 @@ 
  }Bio_Topside_Blowdown_On_Times 
  }Bio_Topside_Blowdown_Off_Times 
  ^Bio_Topside_Blowdown_Cycle_Ratio  &Calculate_Cycle_Ratio CALL.SUB
  ^status_trashcan @! 
3 JUMP ;
: 67_20
~Bio_Topside_Blowdown_1  OFF 
~Bio_Topside_Blowdown_2  OFF 
}Bio_Topside_Blowdown_Off_Times &Record_Cycle_Time CALL.SUB DROP  
-2 JUMP ;
: 67_23
~Bio_Topside_Blowdown_1  ON 
~Bio_Topside_Blowdown_2  ON 
}Bio_Topside_Blowdown_On_Times &Record_Cycle_Time CALL.SUB DROP  
12 JUMP ;
: 67_37
  |Primary_Air 
  ^Bio_Setpoint_Steam_Pressure @@   $0002.. 3 ROLL !PID
19 JUMP ;
: 67_41
  100    DELAY
-6 JUMP ;
: 67_58
~Bio_Boiler_3_Condensate_Pump_On_Off @@I LNOT  IF 
~Bio_Boiler_3_Condensate_Pump_On_Off  ON 
}Bio_Boiler_3_Condensate_Pump_On_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
13 JUMP ;
: 67_59
~Bio_Boiler_3_Condensate_Pump_On_Off @@I IF 
~Bio_Boiler_3_Condensate_Pump_On_Off  OFF 
}Bio_Boiler_3_Condensate_Pump_Off_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
12 JUMP ;
: 67_65
~Bio_Boiler_4_Condensate_Pump_On_Off @@I IF 
~Bio_Boiler_4_Condensate_Pump_On_Off  OFF 
}Bio_Boiler_4_Condensate_Pump_Off_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
4 JUMP ;
: 67_66
~Bio_Boiler_4_Condensate_Pump_On_Off @@I LNOT  IF 
~Bio_Boiler_4_Condensate_Pump_On_Off  ON 
}Bio_Boiler_4_Condensate_Pump_On_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
3 JUMP ;
: 67_76
~Bio_Boiler_3_Condensate_Pump_On_Off @@I LNOT  IF 
~Bio_Boiler_3_Condensate_Pump_On_Off  ON 
}Bio_Boiler_3_Condensate_Pump_On_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
1 JUMP ;
: 67_77
~Bio_Boiler_3_Condensate_Pump_On_Off @@I IF 
~Bio_Boiler_3_Condensate_Pump_On_Off  OFF 
}Bio_Boiler_3_Condensate_Pump_Off_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
0 JUMP ;
: 67_100
0 JUMP ;
: 67_117
3 JUMP ;
: 67_138
~Bio_Boiler_4_Condensate_Pump_On_Off @@I IF 
~Bio_Boiler_4_Condensate_Pump_On_Off  OFF 
}Bio_Boiler_4_Condensate_Pump_Off_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
-3 JUMP ;
: 67_139
~Bio_Boiler_4_Condensate_Pump_On_Off @@I LNOT  IF 
~Bio_Boiler_4_Condensate_Pump_On_Off  ON 
}Bio_Boiler_4_Condensate_Pump_On_Times &Record_Cycle_Time CALL.SUB DROP  
THEN 
-4 JUMP ;
: 67_31
TRUE
  ^Bio_Boiler_Steam_Pressure @@ 
  ^Bio_Boiler_Steam_Pressure_Setpoint @@   F<=
LAND
IF -15 ELSE -16 THEN JUMP ;
: 67_32
TRUE
  0  
  ^Bio_Index_Alarms_Steam_Pressure_High @@ 
  }Bio_Info TABLE@   <>
LAND
IF -15 ELSE -2 THEN JUMP ;
: 67_49
FALSE
  0  
  ^Bio_Index_Alarms_Boiler_3_Water_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Boiler_3_Water_Low @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Boiler_3_Water_Low @@ 
  }Bio_Alarms TABLE@   <>
LOR
  ^Bio_Momentary_B3_Low @@   0<>
LOR
IF -13 ELSE 1 THEN JUMP ;
: 67_50
TRUE
  ~Bio_Input_Boiler_3_Water_High   ?OFF
LAND
IF -13 ELSE -2 THEN JUMP ;
: 67_53
TRUE
  ~Bio_Input_Boiler_4_Water_High   ?OFF
LAND
IF -13 ELSE 0 THEN JUMP ;
: 67_54
FALSE
  0  
  ^Bio_Index_Alarms_Boiler_4_Water_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Boiler_4_Water_Low @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Boiler_4_Water_Low @@ 
  }Bio_Alarms TABLE@   <>
LOR
  ^Bio_Momentary_B4_Low @@   0<>
LOR
IF -13 ELSE -9 THEN JUMP ;
: 67_74
TRUE
  ~Bio_Input_Boiler_3_Water_High   ?OFF
LAND
  ~Bio_Input_Boiler_4_Water_High   ?OFF
LAND
IF -12 ELSE -11 THEN JUMP ;
: 67_75
FALSE
  ~Bio_Input_Boiler_3_Water_Low   ?OFF
LOR
  ~Bio_Input_Boiler_4_Water_Low   ?ON
LOR
IF -14 ELSE -2 THEN JUMP ;
: 67_126
TRUE
  ^Bio_Single_Condensate @@ 
  3    =
LAND
IF -2 ELSE 0 THEN JUMP ;
: 67_131
TRUE
  ^Bio_Single_Condensate @@ 
  4    =
LAND
IF 0 ELSE -7 THEN JUMP ;
: 67_140
FALSE
  ~Bio_Input_Boiler_3_Water_Low   ?OFF
LOR
  ~Bio_Input_Boiler_4_Water_Low   ?ON
LOR
IF -12 ELSE 0 THEN JUMP ;
: 67_141
TRUE
  ~Bio_Input_Boiler_3_Water_High   ?OFF
LAND
  ~Bio_Input_Boiler_4_Water_High   ?OFF
LAND
IF -14 ELSE -16 THEN JUMP ;
T: T67
DUMMY
67_0
67_5
67_20
67_23
67_37
67_41
67_58
67_59
67_65
67_66
67_76
67_77
67_100
67_117
67_138
67_139
67_31
67_32
67_49
67_50
67_53
67_54
67_74
67_75
67_126
67_131
67_140
67_141
T;
&Boilers ' T67 SETTASK
: 64_0
0 JUMP ;
T: T64
DUMMY
64_0
T;
&Changelog ' T64 SETTASK
: 74_0
  &Ash_Out   STOP.T
  0  
  ^index @!   
8 JUMP ;
: 74_10
  ~Bio_Ash_Removal_Screws_Forward   OFF
8 JUMP ;
: 74_11
  20.0    FDELAY
-2 JUMP ;
: 74_12
  ~Bio_Ash_Removal_Screws_Forward   ON
-2 JUMP ;
: 74_13
  6.0    FDELAY
-2 JUMP ;
: 74_14
  ~Bio_Ash_Removal_Screws_Reverse   OFF
-2 JUMP ;
: 74_15
  20.0    FDELAY
-2 JUMP ;
: 74_16
  ~Bio_Ash_Removal_Screws_Reverse   ON
-2 JUMP ;
: 74_21
  6.0    FDELAY
-2 JUMP ;
: 74_22
  ^index   1+@!
  ~Bio_Ash_Removal_Screws_Forward   OFF
-2 JUMP ;
: 74_26
  ^Bio_System_Clear_Ash_Augers   FALSE@!
0 JUMP ;
T: T74
DUMMY
74_0
74_10
74_11
74_12
74_13
74_14
74_15
74_16
74_21
74_22
74_26
T;
&Clear_Ash_Auger ' T74 SETTASK
: 81_0
  ^Bio_Clearing_Belt_Auger_Jam   TRUE@!
  20.0  
  ^Bio_Pusher_Timer   SetTimer
  1  
  ^Bio_Jam_Clear_Attemps @!   
0 JUMP ;
: 81_3
  ~Bio_Silo_1_Augers_Forward   OFF
3 JUMP ;
: 81_6
  500    DELAY
26 JUMP ;
: 81_21
  12.0    FDELAY
2 JUMP ;
: 81_22
  ~Bio_Lift_Auger_Forward   ON
-2 JUMP ;
: 81_23
  ^Bio_System_Enabled_Day_Bin   FALSE@!
23 JUMP ;
: 81_28
  ^Bio_Pusher_Timer   StartTimer
23 JUMP ;
: 81_30
  ~Bio_Pusher_Extend   ON
0 JUMP ;
: 81_31
  750    DELAY
0 JUMP ;
: 81_32
  ~Bio_Pusher_Extend   OFF
0 JUMP ;
: 81_38
  1000    DELAY
19 JUMP ;
: 81_43
  ^Bio_Clearing_Belt_Auger_Jam   FALSE@!
  ^Bio_System_Enabled_Day_Bin   TRUE@!
23 JUMP ;
: 81_44
  ^Bio_Clearing_Belt_Auger_Jam   FALSE@!
  ^Bio_System_Enabled_Day_Bin   FALSE@!
22 JUMP ;
: 81_48
  ~Bio_Lift_Auger_Forward   OFF
-2 JUMP ;
: 81_54
  ^Bio_Jam_Clear_Attemps   1+@!
17 JUMP ;
: 81_60
  ~Bio_Belt_Conveyor_On_Off   ON
0 JUMP ;
: 81_62
  ~Bio_Silo_1_Augers_Forward   ON
0 JUMP ;
: 81_63
  20.0    FDELAY
0 JUMP ;
: 81_65
  ~Bio_Silo_1_Augers_Forward   OFF
1 JUMP ;
: 81_67
  ~Bio_Belt_Conveyor_On_Off   OFF
2 JUMP ;
: 81_68
  20.0    FDELAY
-2 JUMP ;
: 81_90
-17 JUMP ;
: 81_101
  20.0    FDELAY
0 JUMP ;
: 81_105
-19 JUMP ;
: 81_106
-20 JUMP ;
: 81_109
  ~Bio_Lift_Auger_Forward   ON
-11 JUMP ;
: 81_112
  ~Bio_Lift_Auger_Forward   OFF
-6 JUMP ;
: 81_113
  ~Bio_Lift_Auger_Forward   OFF
-4 JUMP ;
: 81_118
  ~Bio_Lift_Auger_Forward   OFF
-18 JUMP ;
: 81_20
TRUE
  &Day_Bin_Fill   ?STOPPED
LAND
IF -26 ELSE -28 THEN JUMP ;
: 81_29
TRUE
  ^Bio_Pusher_Timer   T0=
LAND
IF 0 ELSE -24 THEN JUMP ;
: 81_41
TRUE
  0  
  ^Bio_Index_Alarms_Conveyor_Jam2 @@ 
  }Bio_Warnings TABLE@   =
LAND
  0  
  ^Bio_Index_Alarms_Conveyor_Jam2 @@ 
  }Bio_Alarms TABLE@   =
LAND
IF -4 ELSE -18 THEN JUMP ;
: 81_51
TRUE
  ^Bio_Jam_Clear_Attemps @@ 
  5    <=
LAND
IF -7 ELSE 0 THEN JUMP ;
: 81_57
FALSE
  ^Bio_Jam_Clear_Attemps @@ 
  6    =
LOR
  ^Bio_Jam_Clear_Attemps @@ 
  11    =
LOR
IF -9 ELSE 0 THEN JUMP ;
: 81_83
TRUE
  ^Bio_Jam_Clear_Attemps @@ 
  15    <=
LAND
IF -8 ELSE -22 THEN JUMP ;
T: T81
DUMMY
81_0
81_3
81_6
81_21
81_22
81_23
81_28
81_30
81_31
81_32
81_38
81_43
81_44
81_48
81_54
81_60
81_62
81_63
81_65
81_67
81_68
81_90
81_101
81_105
81_106
81_109
81_112
81_113
81_118
81_20
81_29
81_41
81_51
81_57
81_83
T;
&Clear_Belt_Auger_Transistion ' T81 SETTASK
: 80_0
  ^Bio_Clearing_Belt_Jam   TRUE@!
9 JUMP ;
: 80_1
  ^Bio_System_Enabled_Day_Bin   FALSE@!
21 JUMP ;
: 80_2
  ~Bio_Lift_Auger_Forward   ON
0 JUMP ;
: 80_3
  12.0    FDELAY
0 JUMP ;
: 80_5
  ~Bio_Belt_Conveyor_On_Off   ON
0 JUMP ;
: 80_6
  120.0    FDELAY
0 JUMP ;
: 80_7
  ~Bio_Belt_Conveyor_On_Off   OFF
0 JUMP ;
: 80_8
  12.0    FDELAY
0 JUMP ;
: 80_9
  ~Bio_Lift_Auger_Forward   OFF
  ^Bio_Clearing_Belt_Jam   FALSE@!
15 JUMP ;
: 80_18
  500    DELAY
13 JUMP ;
: 80_22
  ~Bio_Silo_1_Augers_Forward   OFF
-10 JUMP ;
: 80_29
  ^Bio_Clearing_Belt_Jam   FALSE@!
  ^Bio_System_Enabled_Day_Bin   TRUE@!
14 JUMP ;
: 80_30
  ^Bio_Clearing_Belt_Jam   FALSE@!
  ^Bio_System_Enabled_Day_Bin   FALSE@!
13 JUMP ;
: 80_34
  ~Bio_Lift_Auger_Forward   ON
6 JUMP ;
: 80_39
  20.0    FDELAY
7 JUMP ;
: 80_44
  120.0    FDELAY
0 JUMP ;
: 80_45
  ~Bio_Belt_Conveyor_On_Off   OFF
-3 JUMP ;
: 80_46
  ~Bio_Silo_1_Augers_Forward   OFF
-3 JUMP ;
: 80_47
  20.0    FDELAY
-2 JUMP ;
: 80_48
  ~Bio_Silo_1_Augers_Forward   ON
-2 JUMP ;
: 80_49
  ~Bio_Belt_Conveyor_On_Off   ON
-2 JUMP ;
: 80_54
  ^Bio_Clearing_Belt_Jam   FALSE@!
  ^Bio_System_Enabled_Day_Bin   TRUE@!
4 JUMP ;
: 80_56
  ~Bio_Lift_Auger_Forward   OFF
  ^Bio_Clearing_Belt_Jam   FALSE@!
2 JUMP ;
: 80_4
TRUE
  &Day_Bin_Fill   ?STOPPED
LAND
IF -22 ELSE -15 THEN JUMP ;
: 80_26
TRUE
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Warnings TABLE@   =
LAND
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Alarms TABLE@   =
LAND
IF -14 ELSE -12 THEN JUMP ;
: 80_50
TRUE
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Warnings TABLE@   =
LAND
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Alarms TABLE@   =
LAND
IF -5 ELSE -14 THEN JUMP ;
T: T80
DUMMY
80_0
80_1
80_2
80_3
80_5
80_6
80_7
80_8
80_9
80_18
80_22
80_29
80_30
80_34
80_39
80_44
80_45
80_46
80_47
80_48
80_49
80_54
80_56
80_4
80_26
80_50
T;
&Clear_Belt_Jam ' T80 SETTASK
: 72_0
0 JUMP ;
: 72_1
  ^Bio_System_Enabled_Day_Bin   FALSE@!
  ~Bio_Silo_1_Augers_Forward   OFF
  ~Bio_Lift_Auger_Forward   OFF
  ~Bio_Belt_Conveyor_On_Off   OFF
0 JUMP ;
: 72_2
  10.0    FDELAY
0 JUMP ;
: 72_3
  ~Bio_Lift_Auger_Reverse   ON
0 JUMP ;
: 72_4
  15.0    FDELAY
0 JUMP ;
: 72_5
  ~Bio_Lift_Auger_Reverse   OFF
0 JUMP ;
: 72_6
  6.0    FDELAY
0 JUMP ;
: 72_7
  ~Bio_Lift_Auger_Forward   ON
0 JUMP ;
: 72_8
  25.0    FDELAY
0 JUMP ;
: 72_9
  ~Bio_Lift_Auger_Forward   OFF
0 JUMP ;
: 72_19
  ^Bio_System_Clear_Lift_Auger   FALSE@!
0 JUMP ;
T: T72
DUMMY
72_0
72_1
72_2
72_3
72_4
72_5
72_6
72_7
72_8
72_9
72_19
T;
&Clear_Lift_Auger ' T72 SETTASK
: 52_0
2 JUMP ;
: 52_487
~Bio_Scrubber_Inlet_Pump_Speed @@ 1.000000e+001 F>= IF 
~Bio_Scrubber_Inlet_Pump_On_Off  ON 
ELSE 
~Bio_Scrubber_Inlet_Pump_On_Off  OFF 
THEN 
12 JUMP ;
: 52_489
~Bio_Quench_Inlet_Pump_Speed @@ 1.000000e+001 F>= IF 
~Bio_Quench_Inlet_Pump_On_Off  ON 
ELSE 
~Bio_Quench_Inlet_Pump_On_Off  OFF 
THEN 
-2 JUMP ;
: 52_490
~Bio_Air_Moisturizer_Inlet_Pump_Speed @@ 1.000000e+001 F>= IF 
~Bio_Air_Moisturizer_Inlet_Pump_Motor_On_Off  ON 
ELSE 
~Bio_Air_Moisturizer_Inlet_Pump_Motor_On_Off  OFF 
THEN 
0 JUMP ;
: 52_510
~Bio_Air_Moisturizer_Outlet_Pump_Speed @@ 2.000000e+001 F>= IF 
~Bio_Air_Moisturizer_Outlet_Pump_Motor_On_Off  ON 
ELSE 
~Bio_Air_Moisturizer_Outlet_Pump_Motor_On_Off  OFF 
THEN 
-3 JUMP ;
: 52_1204
  ^Bio_System_Enabled_Moisturizer   FALSE@!
6 JUMP ;
: 52_1206
  0.0  
  ~Bio_Scrubber_Inlet_Pump_Speed @!   
  ~Bio_Scrubber_Outlet_Pump_On_Off   OFF
-2 JUMP ;
: 52_1218
|Scrubber_Inlet_PID ~Bio_Input_Scrubber_Water_Level @@ 1 I>F F+  $0002.. 3 ROLL !PID 
2 JUMP ;
: 52_1219
|Scrubber_Inlet_PID ~Bio_Input_Scrubber_Water_Level @@ 1 I>F F-  $0002.. 3 ROLL !PID 
3 JUMP ;
: 52_1245
~Bio_Scrubber_Outlet_Pump_On_Off  ON 
^Bio_Timer_Scrubber_Outlet_OFF  StopTimer 
^Bio_Timer_Scrubber_Outlet_ON @@F 0 I>F F= IF 
^Bio_Timer_Scrubber_Outlet_ON  StartTimer 
THEN 
-2 JUMP ;
: 52_1246
~Bio_Scrubber_Outlet_Pump_On_Off  OFF 
^Bio_Timer_Scrubber_Outlet_ON  StopTimer 
^Bio_Timer_Scrubber_Outlet_OFF @@F 0 I>F F= IF 
^Bio_Timer_Scrubber_Outlet_OFF  StartTimer 
THEN 
1 JUMP ;
: 52_1379
-12 JUMP ;
: 52_1422
0 JUMP ;
: 52_1445
~Bio_Steam_Flow_1 @@ 0 I>F F< IF 
0 I>F ~Bio_Steam_Flow_1 @! 
THEN 
~Bio_Steam_Flow_2 @@ 0 I>F F< IF 
0 I>F ~Bio_Steam_Flow_2 @! 
THEN 
~Bio_Steam_Flow_3 @@ 0 I>F F< IF 
0 I>F ~Bio_Steam_Flow_3 @! 
THEN 
~Bio_Steam_Flow_1 @@ ~Bio_Steam_Flow_2 @@ F+ ~Bio_Steam_Flow_3 @@ F+ ^Bio_Steam_Flow_Total @! 
-3 JUMP ;
: 52_1207
TRUE
  ^Bio_System_Enabled_Scrubber @@   0<>
LAND
IF 1 ELSE -9 THEN JUMP ;
: 52_1228
TRUE
  ^Bio_Timer_Scrubber_Outlet_ON @@F 
  60.0    F>=
LAND
IF -6 ELSE 1 THEN JUMP ;
: 52_1230
TRUE
  ^Bio_Timer_Scrubber_Outlet_OFF @@F 
  1800.0    F>=
LAND
IF -8 ELSE 1 THEN JUMP ;
: 52_1252
FALSE
  0  
  ^Bio_Index_Alarms_Scrubber_Water_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Water_Low @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Water_Low @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -11 ELSE -5 THEN JUMP ;
: 52_1253
FALSE
  0  
  ^Bio_Index_Alarms_Scrubber_Water_High @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Water_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Water_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
  ^Bio_Momentary_Scrubber_High @@   0<>
LOR
IF -10 ELSE -4 THEN JUMP ;
T: T52
DUMMY
52_0
52_487
52_489
52_490
52_510
52_1204
52_1206
52_1218
52_1219
52_1245
52_1246
52_1379
52_1422
52_1445
52_1207
52_1228
52_1230
52_1252
52_1253
T;
&Condensate ' T52 SETTASK
: 71_0
 DOW@I  ^Bio_Day_Of_Week @! 
3 JUMP ;
: 71_31
~Bio_Silo_1_Augers_Forward @@I ~Bio_Belt_Conveyor_On_Off @@I LOR  ~Bio_Lift_Auger_Forward @@I LOR  IF 
~Bio_Silo_1_Augers_Forward  OFF 
^Bio_Delay_Day_Bin_Clear_Conveyors_Msec @@  DELAY 
~Bio_Belt_Conveyor_On_Off  OFF 
~Bio_Lift_Auger_Forward  OFF 
^Bio_Timer_Day_Bin_Filling  StopTimer 
THEN 
5 JUMP ;
: 71_58
  1.0    FDELAY
2 JUMP ;
: 71_62
~Bio_Silo_1_Augers_Forward @@I LNOT  ~Bio_Belt_Conveyor_On_Off @@I LNOT  LOR  ~Bio_Lift_Auger_Forward @@I LNOT  LOR  IF 
~Bio_Lift_Auger_Forward  ON 
~Bio_Belt_Conveyor_On_Off  ON 
~Bio_Silo_1_Augers_Forward  ON 
^Bio_Timer_Day_Bin_Filling  StartTimer 
THEN 
-2 JUMP ;
: 71_75
  ~Bio_Air_Blast_Sensor_Clean   ON
  2.0    FDELAY
  ~Bio_Air_Blast_Sensor_Clean   OFF
0 JUMP ;
: 71_30
TRUE
  ^Bio_System_Enabled_Day_Bin @@   0<>
LAND
IF 0 ELSE -5 THEN JUMP ;
: 71_33
FALSE
  0  
  ^Bio_Index_Alarms_Day_Bin_High_PE @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Day_Bin_High_PE @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Day_Bin_High_PE @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -6 ELSE -4 THEN JUMP ;
T: T71
DUMMY
71_0
71_31
71_58
71_62
71_75
71_30
71_33
T;
&Day_Bin_Fill ' T71 SETTASK
: 83_0
5 JUMP ;
: 83_1
  ~Bio_Input_Day_Bin_Low   DISABLE
  ~Bio_Input_Day_Bin_Low   IN.ON
0 JUMP ;
: 83_2
  ^Bio_Day_Bin_Fill_Timer @@   FDELAY
0 JUMP ;
: 83_3
  ~Bio_Input_Day_Bin_Low   ENABLE
0 JUMP ;
: 83_4
  ^Bio_Day_Bin_Fill_Delay @@   FDELAY
1 JUMP ;
: 83_15
  ^Bio_Timer_Day_Bin_High   StartTimer
4 JUMP ;
: 83_23
  5.0    FDELAY
4 JUMP ;
: 83_28
  ^Bio_Timer_Day_Bin_High   StopTimer
-2 JUMP ;
: 83_13
TRUE
  ~Bio_Input_Daybin_High_PE   ?ON
LAND
IF 0 ELSE -2 THEN JUMP ;
: 83_14
TRUE
  ^Bio_Timer_Day_Bin_High @@F 
  0.0    F>
LAND
IF 0 ELSE -5 THEN JUMP ;
: 83_16
TRUE
  ^Bio_Timer_Day_Bin_High @@F 
  600.0    F>=
LAND
IF -10 ELSE -5 THEN JUMP ;
: 83_42
TRUE
  ^Bio_System_Enabled_Day_Bin @@   0<>
LAND
IF -4 ELSE -6 THEN JUMP ;
T: T83
DUMMY
83_0
83_1
83_2
83_3
83_4
83_15
83_23
83_28
83_13
83_14
83_16
83_42
T;
&Day_Bin_Timed_Fill ' T83 SETTASK
: 82_0
0 JUMP ;
: 82_1
*_HSV_SEMA Acquire1String " " *_HSV_TEMP $MOVE 13  *_HSV_TEMP $APPEND 10  *_HSV_TEMP $APPEND *_HSV_TEMP *CRLF $MOVE Release1String 
" mmazzantini@warwickmills.com" 0 {arrstrServer $TABLE@  $MOVE 
" mike240i" 1 {arrstrServer $TABLE@  $MOVE 
" smtp.gmail.com" 2 {arrstrServer $TABLE@  $MOVE 
" 587" 3 {arrstrServer $TABLE@  $MOVE 
" tls" 4 {arrstrServer $TABLE@  $MOVE 
2 JUMP ;
: 82_2
 0 {arrstrServer  {arrstrRecipients {arrstrBody  SMTP.SEND ^nResult @! 
^nResult @@ 0 = IF 
^email_alarm_1_sent_flag  TRUE@! 
ELSE 
^nResult @@ *strTempString  I>$ 
*_HSV_SEMA Acquire1String " Could not send email, error: "  *_HSV_TEMP $MOVE *strTempString  *_HSV_TEMP $CAT *_HSV_TEMP *strErrorMessage $MOVE Release1String 
 16  -22000 0 *strErrorMessage  AddToMessageQueue 
THEN 
1 JUMP ;
: 82_5
" Controller Email w/Attachments" 0 {arrstrBody $TABLE@  $MOVE 
*_HSV_SEMA Acquire1String " Hello:"  *_HSV_TEMP $MOVE *CRLF  *_HSV_TEMP $CAT *_HSV_TEMP 1 {arrstrBody $TABLE@ $MOVE Release1String 
*CRLF 2 {arrstrBody $TABLE@  $MOVE 
" This is an example of an email that can be sent from a PAC " 3 {arrstrBody $TABLE@  $MOVE 
" controller. You can also do attachments with" 4 {arrstrBody $TABLE@  $MOVE 
*_HSV_SEMA Acquire1String " the SendEmailWithAttachments command."  *_HSV_TEMP $MOVE *CRLF  *_HSV_TEMP $CAT *CRLF  *_HSV_TEMP $CAT *_HSV_TEMP 5 {arrstrBody $TABLE@ $MOVE Release1String 
" Any kind of file may be attached, so long as there is room " 6 {arrstrBody $TABLE@  $MOVE 
" in the filesystem. Be cautious about file size, however, as" 7 {arrstrBody $TABLE@  $MOVE 
*_HSV_SEMA Acquire1String "  many ISPs still have a 10MB attachment limit."  *_HSV_TEMP $MOVE *CRLF  *_HSV_TEMP $CAT *CRLF  *_HSV_TEMP $CAT *_HSV_TEMP 8 {arrstrBody $TABLE@ $MOVE Release1String 
*_HSV_SEMA Acquire1String " Sincerely,"  *_HSV_TEMP $MOVE 
*CRLF  *_HSV_TEMP $CAT *_HSV_TEMP 9 {arrstrBody $TABLE@ $MOVE Release1String 
" Opto Controller" 10 {arrstrBody $TABLE@  $MOVE 
" " 11 {arrstrBody $TABLE@  $MOVE 
" This is not included in the body." 12 {arrstrBody $TABLE@  $MOVE 
" physicsmazz@gmail.com" 0 {arrstrRecipients $TABLE@  $MOVE 
" mmazzantini@warwickmills.com" 1 {arrstrRecipients $TABLE@  $MOVE 
" " 2 {arrstrRecipients $TABLE@  $MOVE 
-2 JUMP ;
: 82_6
  ^nDelay @@   DELAY
2 JUMP ;
: 82_19
  ^email_alarm_1_sent_flag   FALSE@!
-2 JUMP ;
: 82_31
2 JUMP ;
: 82_20
TRUE
 \  " Point this to a digital point. You can check for on or off. (Change the command to 'Off?' in that case)." 
 
 \  " You can check that the latch is set, AND the point is on by changing the Operator to AND just to the right -------_____>" 
 
 \  " these are some of the commands you might use to check analog points. i.e. temperatures and valves" 
 
  ^email_alarm_1_test_trigger @@   0<>
LAND
IF 0 ELSE -3 THEN JUMP ;
: 82_21
TRUE
  ^email_alarm_1_sent_flag @@   0<>
LAND
IF -5 ELSE -6 THEN JUMP ;
T: T82
DUMMY
82_0
82_1
82_2
82_5
82_6
82_19
82_31
82_20
82_21
T;
&Email ' T82 SETTASK
: 85_0
  ^Bio_Extinguish_Day_Bin_Fire   TRUE@!
0 JUMP ;
: 85_1
  ^Bio_System_Enabled_Day_Bin   FALSE@!
0 JUMP ;
: 85_2
  100.0  
  ^Bio_DayBin_TPO_Percent @!   
  100.0  
  ~Bio_Day_Bin_Spray   F!TPO%
  ~Bio_Day_Bin_Spray   ON
0 JUMP ;
: 85_3
  40.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  1    $20000.. 3 ROLL !PID
  |Feed_Rate 
  60.0    $0008.. 3 ROLL !PID
6 JUMP ;
: 85_5
  0.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  0    $20000.. 3 ROLL !PID
  |Feed_Rate 
  20.0    $0008.. 3 ROLL !PID
3 JUMP ;
: 85_6
  ~Bio_Day_Bin_Spray   OFF
  20.0  
  ^Bio_Day_Bin_TPO_Period F@!   
  20.0  
  ~Bio_Day_Bin_Spray   F!TPO%
1 JUMP ;
: 85_7
  ^Bio_System_Enabled_Day_Bin   TRUE@!
-2 JUMP ;
: 85_8
  120.0    FDELAY
-4 JUMP ;
: 85_9
  1200.0  
  ^Bio_Timer_20minute_day_bin   SetTimer
  ^Bio_Timer_20minute_day_bin   StartTimer
6 JUMP ;
: 85_10
  0.0  
  ^Bio_DayBin_TPO_Percent @!   
  0.0  
  ~Bio_Day_Bin_Spray   F!TPO%
  ~Bio_Day_Bin_Spray   OFF
1 JUMP ;
: 85_21
  2400.0  
  ^Bio_Timer_Day_Bin_Emptying   SetTimer
  ^Bio_Timer_Day_Bin_Emptying   StartTimer
3 JUMP ;
: 85_23
  ^Bio_Extinguish_Day_Bin_Fire   FALSE@!
4 JUMP ;
: 85_28
  1.0    FDELAY
1 JUMP ;
: 85_33
  1.0    FDELAY
1 JUMP ;
: 85_22
FALSE
  ^Bio_Timer_Day_Bin_Emptying   T0=
LOR
  ^Bio_Extinguish_Day_Bin_Fire @@   0=
LOR
IF -9 ELSE -3 THEN JUMP ;
: 85_34
FALSE
  ^Bio_Timer_20minute_day_bin   T0=
LOR
  ^Bio_Extinguish_Day_Bin_Fire @@   0=
LOR
IF -7 ELSE -3 THEN JUMP ;
T: T85
DUMMY
85_0
85_1
85_2
85_3
85_5
85_6
85_7
85_8
85_9
85_10
85_21
85_23
85_28
85_33
85_22
85_34
T;
&Extinguish_Day_Bin ' T85 SETTASK
: 65_0
40 JUMP ;
: 65_23
  &Clear_Lift_Auger   START.T
  ^status_trashcan @! 
29 JUMP ;
: 65_28
  ~Bio_Silo_1_Augers_Forward   OFF
  ~Bio_Silo_1_Augers_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Silo_1_Augers_Reverse   OFF
21 JUMP ;
: 65_40
  ~Bio_Fuel_Feeder_Screw_5_Forward   OFF
  ~Bio_Fuel_Feeder_Screw_5_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Fuel_Feeder_Screw_5_Reverse   OFF
4 JUMP ;
: 65_43
  ~Bio_Fuel_Feeder_Screw_3_Forward   OFF
  ~Bio_Fuel_Feeder_Screw_3_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Fuel_Feeder_Screw_3_Reverse   OFF
23 JUMP ;
: 65_46
  ~Bio_Fuel_Feeder_Screw_4_Forward   OFF
  ~Bio_Fuel_Feeder_Screw_4_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Fuel_Feeder_Screw_4_Reverse   OFF
20 JUMP ;
: 65_49
  ~Bio_Fuel_Feeder_Screw_2_Forward   OFF
  ~Bio_Fuel_Feeder_Screw_2_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Fuel_Feeder_Screw_2_Reverse   OFF
20 JUMP ;
: 65_52
  ~Bio_Fuel_Feeder_Screw_1_Forward   OFF
  ~Bio_Fuel_Feeder_Screw_1_Reverse   ON
  ^Bio_Delay_Silo_Auger_Jam_Clear @@   DELAY
  ~Bio_Fuel_Feeder_Screw_1_Reverse   OFF
21 JUMP ;
: 65_54
 GetJulianDay ^Current_Day_Of_Year @! 
 TIME@ -ROT DROP DROP ^Current_Hour_Of_Day @! 
150 I>F ^Bio_Feeder_Screw_Speed @@ 100 I>F F/ F/ ^Bio_Time_Until_Day_Bin_Empty @! 
^Bio_Timer_Feed_Screw_Period  T0= IF 
^Bio_Timer_Feed_Screw_Period  StartTimer 
THEN 
^Bio_Timer_15_Minute  T0= IF 
1 }Bio_Feed_15_Minute_Table  ShiftTable 
^Bio_Timer_Feed_Per_15  GET.RESTART.TMR 0 }Bio_Feed_15_Minute_Table TABLE! 
^Bio_Current_Chip_Volume @@ 0 }Bio_Feed_15_Minute_Table TABLE@ ^Bio_Chip_Volume_Factor @@ F* 27 I>F F/ F- ^Bio_Current_Chip_Volume @! 
0 }Bio_Feed_15_Minute_Table TABLE@ 1 }Bio_Feed_15_Minute_Table TABLE@ F+ 2 }Bio_Feed_15_Minute_Table TABLE@ F+ 3 }Bio_Feed_15_Minute_Table TABLE@ F+ ^Bio_Total_Past_Hour @! 
0 I>F ^Bio_Fuel_Fed_12_Hour_Total @! 
0 I>F ^Bio_Fuel_Fed_24_Hour_Total @! 
1 95 1 + 0 DO? I ^Bio_Index_Feed @! 
^Bio_Index_Feed @@ 47 <= IF 
^Bio_Fuel_Fed_12_Hour_Total @@ ^Bio_Index_Feed @@ }Bio_Feed_15_Minute_Table TABLE@ F+ ^Bio_Fuel_Fed_12_Hour_Total @! 
THEN 
^Bio_Fuel_Fed_24_Hour_Total @@ ^Bio_Index_Feed @@ }Bio_Feed_15_Minute_Table TABLE@ F+ ^Bio_Fuel_Fed_24_Hour_Total @! 
1 +LOOP 
^Bio_Total_Past_Hour @@ ^Bio_Chip_Volume_Factor @@ F* ^Bio_Total_Cu_Ft_Per_Hour @! 
^Bio_Fuel_Fed_12_Hour_Total @@ ^Bio_Chip_Volume_Factor @@ F* ^Bio_Total_Cu_Ft_Per_12_Hours @! 
^Bio_Total_Cu_Ft_Per_Hour @@ 27 I>F F/ ^Bio_Total_Cu_Yd_Per_Hour @! 
^Bio_Total_Cu_Ft_Per_12_Hours @@ 27 I>F F/ ^Bio_Total_Cu_Yd_Per_12_Hours @! 
^Bio_Current_Chip_Volume @@ 1 }Bio_Feed_15_Minute_Table TABLE@ 2 }Bio_Feed_15_Minute_Table TABLE@ F+ 3 }Bio_Feed_15_Minute_Table TABLE@ F+ 4 }Bio_Feed_15_Minute_Table TABLE@ F+ ^Bio_Chip_Volume_Factor @@ F* 27 I>F F/ F/ ^Bio_Silo_Hours_Until_Empty @! 
^Bio_Silo_Hours_Until_Empty @@ 24 I>F F/  Truncate ^Bio_Silo_Days_Until_Empty @! 
^Bio_Silo_Hours_Until_Empty @@I 24 MOD I>F ^Bio_Silo_Days_Until_Empty_Extra_Hours @! 
^Bio_Timer_15_Minute  StartTimer 
^Bio_Timer_Feed_Per_15  StartTimer 
THEN 
^Bio_Timer_Feed_Screw_Period @@F ^Bio_Feed_Screw_Period @@ F/ ^Bio_Feeder_Screw_Speed @@ 100 I>F F/ F<= ^Bio_System_Enabled_Feed_Screws @@  0<> LAND  IF 
~Bio_Fuel_Feeder_Screw_1_Forward  ON 
~Bio_Fuel_Feeder_Screw_2_Forward  ON 
~Bio_Fuel_Feeder_Screw_3_Forward  ON 
~Bio_Fuel_Feeder_Screw_4_Forward  ON 
~Bio_Fuel_Feeder_Screw_5_Forward  ON 
^Bio_Timer_Day_Bin_Low @@F ^Bio_Time_Until_Day_Bin_Empty @@ F> IF 
^Bio_Timer_Feed_Per_15  PauseTimer 
ELSE 
^Bio_Timer_Feed_Per_15  ContinueTimer 
THEN 
ELSE 
~Bio_Fuel_Feeder_Screw_1_Forward  OFF 
~Bio_Fuel_Feeder_Screw_2_Forward  OFF 
~Bio_Fuel_Feeder_Screw_3_Forward  OFF 
~Bio_Fuel_Feeder_Screw_4_Forward  OFF 
~Bio_Fuel_Feeder_Screw_5_Forward  OFF 
^Bio_Timer_Feed_Per_15  PauseTimer 
THEN 
^Current_Hour_Of_Day @@ 0 = ^Bio_24hour_reset @@ 0 = LAND  IF 
^Bio_Fuel_Fed_24_Hour_Total @@ ^Bio_Chip_Volume_Factor @@ F* 27 I>F F/ ^Current_Day_Of_Year @@ 1 -  }Bio_Feed_Day_Of_Year_Total TABLE! 
1 ^Bio_24hour_reset @! 
ELSE 
^Current_Hour_Of_Day @@ 0 > IF 
0 ^Bio_24hour_reset @! 
THEN THEN 
8 JUMP ;
: 65_55
  1000    DELAY
1 JUMP ;
: 65_71
  &Day_Bin_Fill   START.T
  ^status_trashcan @! 
19 JUMP ;
: 65_81
-12 JUMP ;
: 65_83
  &Clear_Ash_Auger   START.T
  ^status_trashcan @! 
29 JUMP ;
: 65_91
  ^Bio_Timer_DayBin_Not_High   StartTimer
9 JUMP ;
: 65_93
  ^Bio_Timer_DayBin_Not_High   StopTimer
8 JUMP ;
: 65_99
  ^Bio_Timer_Day_Bin_Low   StopTimer
14 JUMP ;
: 65_103
  ^Bio_Timer_Day_Bin_Low   StartTimer
-7 JUMP ;
: 65_108
^Bio_Total_Chip_Add_Btn @@ 1 = IF 
^Bio_Current_Chip_Volume @@ ^Bio_Total_Chip_Add_Amount @@F F+ ^Bio_Current_Chip_Volume @! 
0 ^Bio_Total_Chip_Add_Btn @! 
THEN 
7 JUMP ;
: 65_114
  &Clear_Belt_Jam   START.T
  ^status_trashcan @! 
17 JUMP ;
: 65_119
  &Clear_Belt_Auger_Transistion   START.T
  ^status_trashcan @! 
-11 JUMP ;
: 65_131
  ^Bio_Timer_Day_Bin_High_Disabled   StartTimer
11 JUMP ;
: 65_133
  ^Bio_Day_Bin_High_Disabled   FALSE@!
  ~Bio_Input_Daybin_High_PE   ENABLE
  ^Bio_Timer_Day_Bin_High_Disabled   StopTimer
20 JUMP ;
: 65_135
  ~Bio_Input_Daybin_High_PE   DISABLE
  ~Bio_Input_Daybin_High_PE   IN.ON
19 JUMP ;
: 65_16
FALSE
  ~Bio_Input_Day_Bin_Low   ?ON
LOR
  0  
  ^Bio_Index_Alarms_Day_Bin_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  ^Bio_Momentary_Day_Bin_Low @@   0<>
LOR
  ^Bio_Timer_DayBin_Not_High @@F 
  ^Bio_Setpoint_Day_Bin_Not_High_Time @@   F>=
LOR
IF 10 ELSE -9 THEN JUMP ;
: 65_26
FALSE
  0  
  ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
  ^Bio_System_Clear_Lift_Auger @@   0<>
LOR
IF -24 ELSE 6 THEN JUMP ;
: 65_29
FALSE
  0  
  ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
IF -24 ELSE -2 THEN JUMP ;
: 65_39
FALSE
  0  
  ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
IF -24 ELSE -19 THEN JUMP ;
: 65_42
FALSE
  0  
  ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
IF -24 ELSE 0 THEN JUMP ;
: 65_47
FALSE
  0  
  ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
IF -24 ELSE -3 THEN JUMP ;
: 65_48
FALSE
  0  
  ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
IF -24 ELSE -3 THEN JUMP ;
: 65_53
FALSE
  0  
  ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -24 ELSE -2 THEN JUMP ;
: 65_78
FALSE
  ^Bio_System_Clear_Ash_Augers @@   0<>
LOR
IF -20 ELSE 5 THEN JUMP ;
: 65_87
TRUE
  ~Bio_Input_Daybin_High_PE   ?OFF
LAND
IF -19 ELSE 0 THEN JUMP ;
: 65_88
TRUE
  ^Bio_Timer_DayBin_Not_High @@F 
  0.0    F>
LAND
IF -11 ELSE -21 THEN JUMP ;
: 65_102
TRUE
  ^Bio_Timer_Day_Bin_Low @@F 
  0.0    F>
LAND
IF -25 ELSE -19 THEN JUMP ;
: 65_111
FALSE
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Conveyor_Jam1 @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -18 ELSE 0 THEN JUMP ;
: 65_120
FALSE
  0  
  ^Bio_Index_Alarms_Conveyor_Jam2 @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -18 ELSE -28 THEN JUMP ;
: 65_125
TRUE
  ^Bio_System_Enabled_Day_Bin @@   0<>
LAND
IF -3 ELSE -29 THEN JUMP ;
: 65_129
FALSE
  ^Bio_Day_Bin_High_Disabled @@   0<>
LOR
IF 0 ELSE -7 THEN JUMP ;
: 65_130
TRUE
  ^Bio_Timer_Day_Bin_High_Disabled @@F 
  0.0    F>
LAND
IF -8 ELSE -20 THEN JUMP ;
: 65_132
TRUE
  ^Bio_Timer_Day_Bin_High_Disabled @@F 
  1200 I>F   F>=
LAND
IF -20 ELSE -19 THEN JUMP ;
: 65_151
TRUE
  ^Bio_Day_Bin_Running_On_Timer @@   0<>
LAND
IF -12 ELSE -4 THEN JUMP ;
T: T65
DUMMY
65_0
65_23
65_28
65_40
65_43
65_46
65_49
65_52
65_54
65_55
65_71
65_81
65_83
65_91
65_93
65_99
65_103
65_108
65_114
65_119
65_131
65_133
65_135
65_16
65_26
65_29
65_39
65_42
65_47
65_48
65_53
65_78
65_87
65_88
65_102
65_111
65_120
65_125
65_129
65_130
65_132
65_151
T;
&Feed_Section ' T65 SETTASK
: 66_0
5 JUMP ;
: 66_30
^Bio_Secondary_Air_Spray_1_Open_Ratio @@ ~Bio_Secondary_Air_Spray_1  F!TPO% 
^Bio_Secondary_Air_Spray_2_Open_Ratio @@ ~Bio_Secondary_Air_Spray_2  F!TPO% 
^Bio_Secondary_Air_Spray_3_Open_Ratio @@ ~Bio_Secondary_Air_Spray_3  F!TPO% 
^Bio_Tertiary_Air_Spray_Open_Ratio @@ ~Bio_Tertiary_Air_Spray  F!TPO% 
0 JUMP ;
: 66_35
  100    DELAY
-3 JUMP ;
: 66_38
~Bio_Stack_Fan_Motor_Speed @@ 1.000000e+001 F>= IF 
~Bio_Stack_Fan_Motor_On_Off  ON 
ELSE 
~Bio_Stack_Fan_Motor_On_Off  OFF 
THEN 
19 JUMP ;
: 66_41
~Bio_Tertiary_Air_Fan_Speed @@ 1.000000e+001 F>= IF 
~Bio_Tertiary_Air_Fan_On_Off  ON 
ELSE 
~Bio_Tertiary_Air_Fan_On_Off  OFF 
THEN 
-2 JUMP ;
: 66_42
~Bio_Secondary_Air_Fan_Motor_Speed @@ 1.000000e+001 F>= IF 
~Bio_Secondary_Air_Fan_Motor_On_Off  ON 
ELSE 
~Bio_Secondary_Air_Fan_Motor_On_Off  OFF 
THEN 
-2 JUMP ;
: 66_43
~Bio_Primary_Air_Fan_Motor_Speed @@ 1.000000e+001 F>= IF 
~Bio_Primary_Air_Fan_Motor_On_Off  ON 
ELSE 
~Bio_Primary_Air_Fan_Motor_On_Off  OFF 
THEN 
-2 JUMP ;
: 66_54
  |Primary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
  |Secondary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
  |Tertiary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
12 JUMP ;
: 66_55
  |Primary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
  |Secondary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
  |Tertiary_Air_Low_Clamp 
  0    $20000.. 3 ROLL !PID
  |Primary_Air_Low_Clamp 
  0    $0008.. 3 ROLL !PID
  |Secondary_Air_Low_Clamp 
  0    $0008.. 3 ROLL !PID
  |Tertiary_Air_Low_Clamp 
  0    $0008.. 3 ROLL !PID
11 JUMP ;
: 66_62
  |Primary_Air 
  ^Bio_primary_max_speed @@F   $2000.. 3 ROLL !PID
  |Secondary_Air 
  100.0    $2000.. 3 ROLL !PID
  |Tertiary_Air 
  40.0    $2000.. 3 ROLL !PID
-3 JUMP ;
: 66_63
  |Primary_Air 
  0.0    $2000.. 3 ROLL !PID
  |Secondary_Air 
  0.0    $2000.. 3 ROLL !PID
  |Tertiary_Air 
  0.0    $2000.. 3 ROLL !PID
-3 JUMP ;
: 66_64
  |Primary_Air 
  40.0    $2000.. 3 ROLL !PID
  |Secondary_Air 
  30.0    $2000.. 3 ROLL !PID
  |Tertiary_Air 
  20.0    $2000.. 3 ROLL !PID
8 JUMP ;
: 66_65
  |Primary_Air 
  64.0    $2000.. 3 ROLL !PID
  |Secondary_Air 
  48.0    $2000.. 3 ROLL !PID
  |Tertiary_Air 
  32.0    $2000.. 3 ROLL !PID
7 JUMP ;
: 66_84
  |PID_Secondary_Center_Lance 
  1    $20000.. 3 ROLL !PID
  |PID_Secondary_Outer_Lances 
  1    $20000.. 3 ROLL !PID
  |PID_Tertiary_Lance 
  1    $20000.. 3 ROLL !PID
-13 JUMP ;
: 66_85
  |PID_Secondary_Center_Lance 
  0    $20000.. 3 ROLL !PID
  |PID_Secondary_Outer_Lances 
  0    $20000.. 3 ROLL !PID
  |PID_Tertiary_Lance 
  0    $20000.. 3 ROLL !PID
0 JUMP ;
: 66_90
  |PID_Secondary_Center_Lance   $0008.. ROT PID@
  ^Bio_Secondary_Air_Spray_2_Open_Ratio @! 
  |PID_Secondary_Outer_Lances   $0008.. ROT PID@
  ^Bio_Secondary_Air_Spray_1_Open_Ratio @! 
  |PID_Secondary_Outer_Lances   $0008.. ROT PID@
  ^Bio_Secondary_Air_Spray_3_Open_Ratio @! 
  |PID_Tertiary_Lance   $0008.. ROT PID@
  ^Bio_Tertiary_Air_Spray_Open_Ratio @! 
1 JUMP ;
: 66_94
  |PID_Tertiary_Lance 
  ^Bio_Setpoint_Water_Spray @@   $0002.. 3 ROLL !PID
  |PID_Secondary_Outer_Lances 
  ^Bio_Setpoint_Water_Spray @@   $0002.. 3 ROLL !PID
  |PID_Secondary_Center_Lance 
  ^Bio_Setpoint_Water_Spray @@   $0002.. 3 ROLL !PID
  |PID_Secondary_Center_Lance 
  ~Bio_Input_Gasifier_Top_Temp_1 @@   $0001.. 3 ROLL !PID
  |PID_Secondary_Outer_Lances 
  ~Bio_Input_Gasifier_Top_Temp_1 @@   $0001.. 3 ROLL !PID
  |PID_Tertiary_Lance 
  ~Bio_Input_Gasifier_Top_Temp_1 @@   $0001.. 3 ROLL !PID
9 JUMP ;
: 66_100
|PID_Secondary_Center_Lance ^Bio_Setpoint_Above_Door_Spray @@F  $0002.. 3 ROLL !PID 
|PID_Secondary_Outer_Lances ^Bio_Setpoint_Above_Door_Spray @@F  $0002.. 3 ROLL !PID 
|PID_Tertiary_Lance ^Bio_Setpoint_Water_Spray @@  $0002.. 3 ROLL !PID 
|PID_Tertiary_Lance ~Bio_Input_Gasifier_Top_Temp_1 @@  $0001.. 3 ROLL !PID 
|PID_Secondary_Center_Lance ~Bio_Input_Above_Door_Temp @@  $0001.. 3 ROLL !PID 
|PID_Secondary_Outer_Lances ~Bio_Input_Above_Door_Temp @@  $0001.. 3 ROLL !PID 
-17 JUMP ;
: 66_104
  &Ash_Out   START.T
  ^status_trashcan @! 
5 JUMP ;
: 66_18
FALSE
  ^Bio_Timer_Ash_Removal   T0=
LOR
  ^Bio_Momentary_Ash_Out @@   0<>
LOR
IF -2 ELSE 4 THEN JUMP ;
: 66_23
TRUE
  ^Bio_System_Enabled_Ash_Removal @@   0<>
LAND
IF -2 ELSE 3 THEN JUMP ;
: 66_66
TRUE
  1  
  ^Bio_Index_Alarms_Gasifier_DP_High @@ 
  }Bio_Alarms TABLE@   =
LAND
IF -12 ELSE 0 THEN JUMP ;
: 66_67
TRUE
  1  
  ^Bio_Index_Alarms_Gasifier_DP_High @@ 
  }Bio_Warnings TABLE@   =
LAND
IF -12 ELSE -11 THEN JUMP ;
: 66_68
TRUE
  1  
  ^Bio_Index_Alarms_Gasifier_DP_High @@ 
  }Bio_Info TABLE@   =
LAND
IF -3 ELSE -15 THEN JUMP ;
: 66_74
FALSE
  0  
  ^Bio_Index_Alarms_Gasifier_Temp_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Gasifier_Temp_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -23 ELSE 0 THEN JUMP ;
: 66_81
TRUE
  ^Bio_State_Spray_Lance_PID @@   0<>
LAND
IF -12 ELSE -13 THEN JUMP ;
T: T66
DUMMY
66_0
66_30
66_35
66_38
66_41
66_42
66_43
66_54
66_55
66_62
66_63
66_64
66_65
66_84
66_85
66_90
66_94
66_100
66_104
66_18
66_23
66_66
66_67
66_68
66_74
66_81
T;
&Gasifier ' T66 SETTASK
: 62_0
  100    DELAY
25 JUMP ;
: 62_2
  ~BA_Coater_1_clearance   OFF
  ~BA_Coater_2_clearance   OFF
  ~Bio_Incineration_Enabled   OFF
  ~Bio_Incineration_Diverter   OFF
  ~BA_Coater_1_ESTOP   ON
  ~BA_Coater_2_ESTOP   ON
-2 JUMP ;
: 62_10
  ~Bio_Incineration_Diverter   ON
15 JUMP ;
: 62_14
  ~Bio_Incineration_Enabled   ON
13 JUMP ;
: 62_28
-5 JUMP ;
: 62_35
  ^Bio_Error_No_Database_Connection   TRUE@!
0 JUMP ;
: 62_36
-6 JUMP ;
: 62_43
-7 JUMP ;
: 62_52
  ~BA_Coater_1_clearance   OFF
  ~BA_Coater_1_ESTOP   ON
15 JUMP ;
: 62_53
  ~BA_Coater_2_clearance   OFF
  ~BA_Coater_2_ESTOP   ON
-6 JUMP ;
: 62_64
  ^Bio_Check_Database_Timer   StartTimer
9 JUMP ;
: 62_67
  0  
  ^Bio_Database_Connection @!   
  ^Bio_Above_Door_Temp   FALSE@!
  ^Bio_Error_No_Database_Connection   FALSE@!
4 JUMP ;
: 62_72
  ~BA_Coater_1_clearance   ON
11 JUMP ;
: 62_73
  ~BA_Coater_2_clearance   ON
-10 JUMP ;
: 62_106
  %Bioler_Room 
  1  
  ^Bio_running_on_biomass @@   IO.SP.I.WRITE
  ^Bio_Scratchpad_Status @! 
  200    DELAY
-15 JUMP ;
: 62_112
  %Bioler_Room 
  1  
  ^Bio_running_on_biomass @@   IO.SP.I.WRITE
  ^Bio_Scratchpad_Status @! 
3 JUMP ;
: 62_1
FALSE
  ^Bio_Gasifier_Top_Temp_1 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ^Bio_Gasifier_Temp_1 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ^Bio_Gasifier_Temp_2 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ^Bio_Above_Door_Temp @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ~Bio_Input_Gasifier_Top_Temp_2 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ~Bio_Input_Gasifier_Temp_2 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ~Bio_Input_Gasifier_Temp_3 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
  ~Bio_Input_Gasifier_Temp_4 @@ 
  ^Bio_Incineration_Temp_Minimum @@   F>=
LOR
IF 8 ELSE -10 THEN JUMP ;
: 62_6
FALSE
  ~BA_Coater_1_request_for_incineration   ?ON
LOR
IF 4 ELSE -10 THEN JUMP ;
: 62_9
TRUE
  ~Bio_Input_Incineration_Diverter_Open_Switch   ?ON
LAND
  ~Bio_Input_Incineration_Diverter_Closed_Switch   ?OFF
LAND
IF -16 ELSE -18 THEN JUMP ;
: 62_17
TRUE
  ^Bio_Operation_Mode @@ 
  1  
  3    2 PICK >= -ROT >= AND
LAND
IF 1 ELSE -19 THEN JUMP ;
: 62_25
TRUE
  ^Bio_Database_Connection @@ 
  1    =
LAND
IF -10 ELSE -16 THEN JUMP ;
: 62_27
TRUE
  ^Bio_Check_Database_Timer   T0=
LAND
IF -12 ELSE -6 THEN JUMP ;
: 62_51
TRUE
  ~BA_Coater_1_ESTOP   ?ON
LAND
IF -15 ELSE -11 THEN JUMP ;
: 62_54
TRUE
  ~BA_Coater_2_ESTOP   ?ON
LAND
IF -15 ELSE -11 THEN JUMP ;
: 62_93
FALSE
  ~BA_Coater_2_request_for_incineration   ?ON
LOR
IF -2 ELSE -16 THEN JUMP ;
: 62_101
TRUE
  1  
  41  
  }Bio_Alarms TABLE@   =
LAND
IF -19 ELSE -24 THEN JUMP ;
: 62_105
TRUE
  ^Bio_running_on_biomass @@   0<>
LAND
IF -12 ELSE -13 THEN JUMP ;
T: T62
DUMMY
62_0
62_2
62_10
62_14
62_28
62_35
62_36
62_43
62_52
62_53
62_64
62_67
62_72
62_73
62_106
62_112
62_1
62_6
62_9
62_17
62_25
62_27
62_51
62_54
62_93
62_101
62_105
T;
&Incineration ' T62 SETTASK
: 75_0
  |Secondary_Air 
  1    $20000.. 3 ROLL !PID
  |Secondary_Air 
  0 I>F   $0008.. 3 ROLL !PID
  0.0  
  ^Bio_Trim_Primary_Air @!   
  -3.0  
  ^Bio_Trim_Tertiary @!   
  0.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  1    $20000.. 3 ROLL !PID
  10.0  
  ^Bio_Setpoint_Steam_Pressure @!   
  |Secondary_Air 
  500.0    $0002.. 3 ROLL !PID
  |Gasifier_Pressure 
  25.0    $1000.. 3 ROLL !PID
  |Primary_Air 
  10.0    $1000.. 3 ROLL !PID
  |Feed_Rate 
  10.0    $1000.. 3 ROLL !PID
  |Feed_Rate 
  1.5    $0002.. 3 ROLL !PID
  |Gasifier_Pressure 
  -0.03    $0002.. 3 ROLL !PID
  |Feed_Rate 
  18.0    $0008.. 3 ROLL !PID
2 JUMP ;
: 75_29
  |Gasifier_Pressure 
  -0.03    $0002.. 3 ROLL !PID
2 JUMP ;
: 75_30
  |Tertiary_Air 
  0    $20000.. 3 ROLL !PID
-2 JUMP ;
: 75_32
  |Primary_Air 
  0    $20000.. 3 ROLL !PID
-2 JUMP ;
: 75_39
  1.0    FDELAY
-2 JUMP ;
T: T75
DUMMY
75_0
75_29
75_30
75_32
75_39
T;
&Mode_Fuel_Saver ' T75 SETTASK
: 55_0
  100    DELAY
6 JUMP ;
: 55_192
  ^Bio_Operation_Mode @@ 
  {Bio_Operation_Mode_Strings $TABLE@ 
  *Bio_Operation_Mode_String   $MOVE
0 JUMP ;
: 55_366
  ^Bio_Operation_Mode @@ 
  ^Bio_Operation_Mode_Last @!   
-3 JUMP ;
: 55_380
  &Mode_Fuel_Saver   STOP.T
  &Mode_Shutdown   STOP.T
  &Mode_Startup   STOP.T
  &Mode_Run   START.T
  ^status_trashcan @! 
-3 JUMP ;
: 55_391
  &Mode_Run   STOP.T
  &Mode_Shutdown   STOP.T
  &Mode_Startup   STOP.T
  &Mode_Fuel_Saver   START.T
  ^status_trashcan @! 
-4 JUMP ;
: 55_396
  &Mode_Run   STOP.T
  &Mode_Fuel_Saver   STOP.T
  &Mode_Shutdown   STOP.T
  &Mode_Startup   START.T
  ^status_trashcan @! 
-5 JUMP ;
: 55_407
  &Mode_Run   STOP.T
  &Mode_Fuel_Saver   STOP.T
  &Mode_Startup   STOP.T
  &Mode_Shutdown   START.T
  ^status_trashcan @! 
-6 JUMP ;
: 55_285
TRUE
  ^Bio_Operation_Mode @@ 
  ^Bio_Index_Operation_Modes_Run @@   =
LAND
IF 1 ELSE 0 THEN JUMP ;
: 55_286
TRUE
  ^Bio_Operation_Mode @@ 
  ^Bio_Index_Operation_Modes_Fuel_Saver @@   =
LAND
IF 1 ELSE 3 THEN JUMP ;
: 55_382
TRUE
  &Mode_Run   ?RUNNING
LAND
IF -9 ELSE -7 THEN JUMP ;
: 55_387
TRUE
  &Mode_Fuel_Saver   ?RUNNING
LAND
IF -10 ELSE -7 THEN JUMP ;
: 55_398
TRUE
  &Mode_Startup   ?RUNNING
LAND
IF -11 ELSE -7 THEN JUMP ;
: 55_399
TRUE
  ^Bio_Operation_Mode @@ 
  ^Bio_Index_Operation_Modes_Startup @@   =
LAND
IF -2 ELSE 0 THEN JUMP ;
: 55_404
TRUE
  ^Bio_Operation_Mode @@ 
  ^Bio_Index_Operation_Modes_Shutdown @@   =
LAND
IF 0 ELSE -13 THEN JUMP ;
: 55_405
TRUE
  &Mode_Shutdown   ?RUNNING
LAND
IF -14 ELSE -9 THEN JUMP ;
T: T55
DUMMY
55_0
55_192
55_366
55_380
55_391
55_396
55_407
55_285
55_286
55_382
55_387
55_398
55_399
55_404
55_405
T;
&Mode_Management ' T55 SETTASK
: 76_0
  |Primary_Air 
  0    $20000.. 3 ROLL !PID
  |Tertiary_Air 
  0    $20000.. 3 ROLL !PID
  3.0  
  ^Bio_Trim_Tertiary @!   
  0.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  0    $20000.. 3 ROLL !PID
  |Gasifier_Pressure 
  35.0    $1000.. 3 ROLL !PID
  |Primary_Air 
  75.0    $2000.. 3 ROLL !PID
  |Primary_Air 
  10.0    $1000.. 3 ROLL !PID
  |Gasifier_Pressure 
  -0.03    $0002.. 3 ROLL !PID
  |Secondary_Air 
  500.0    $0002.. 3 ROLL !PID
  30.0  
  ^Bio_Setpoint_Steam_Pressure @!   
  |Feed_Rate 
  2.15    $0002.. 3 ROLL !PID
  |Feed_Rate 
  18.0    $1000.. 3 ROLL !PID
  |Feed_Rate 
  60.0    $2000.. 3 ROLL !PID
2 JUMP ;
: 76_1
  |Tertiary_Air_Low_Clamp 
  ^Tertiary_Low_Clamp_Running @@F   $1000.. 3 ROLL !PID
  ^Bio_System_Both_Coaters_Running   FALSE@!
12 JUMP ;
: 76_3
  |Tertiary_Air_Low_Clamp 
  ^Tertiary_Low_Clamp_Running_2_Coaters @@F   $1000.. 3 ROLL !PID
  ^Bio_System_Both_Coaters_Running   TRUE@!
8 JUMP ;
: 76_16
  100    DELAY
11 JUMP ;
: 76_22
  |Feed_Rate 
  0    $20000.. 3 ROLL !PID
7 JUMP ;
: 76_23
  |Feed_Rate 
  1    $20000.. 3 ROLL !PID
6 JUMP ;
: 76_31
  |Tertiary_Air_Low_Clamp 
  ^Tertiary_Low_Clamp_Running @@F   $1000.. 3 ROLL !PID
7 JUMP ;
: 76_45
  16  
  ^Tertiary_Low_Clamp_Running_2_Coaters @!   
-6 JUMP ;
: 76_47
  21  
  ^Tertiary_Low_Clamp_Running_2_Coaters @!   
-7 JUMP ;
: 76_48
  25  
  ^Tertiary_Low_Clamp_Running_2_Coaters @!   
-8 JUMP ;
: 76_53
  30  
  ^Tertiary_Low_Clamp_Running_2_Coaters @!   
-9 JUMP ;
: 76_58
2 JUMP ;
: 76_64
-10 JUMP ;
: 76_4
TRUE
  ~BA_Coater_1_clearance   ?ON
LAND
  ~BA_Coater_2_clearance   ?ON
LAND
  ~BA_Coater_1_request_for_incineration   ?ON
LAND
  ~BA_Coater_2_request_for_incineration   ?ON
LAND
IF 2 ELSE -13 THEN JUMP ;
: 76_21
TRUE
  ^Bio_Feed_Auto @@   0<>
LAND
IF -11 ELSE -10 THEN JUMP ;
: 76_30
FALSE
  ~BA_Coater_1_request_for_incineration   ?ON
LOR
  ~BA_Coater_2_request_for_incineration   ?ON
LOR
IF -3 ELSE -10 THEN JUMP ;
: 76_43
TRUE
  ~Bio_Primary_Air_Fan_Motor_Speed @@ 
  75.0    F>=
LAND
IF -10 ELSE 0 THEN JUMP ;
: 76_46
TRUE
  ~Bio_Primary_Air_Fan_Motor_Speed @@ 
  60.0    F>=
LAND
IF -10 ELSE 0 THEN JUMP ;
: 76_49
TRUE
  ~Bio_Primary_Air_Fan_Motor_Speed @@ 
  40.0    F>=
LAND
IF -10 ELSE -9 THEN JUMP ;
T: T76
DUMMY
76_0
76_1
76_3
76_16
76_22
76_23
76_31
76_45
76_47
76_48
76_53
76_58
76_64
76_4
76_21
76_30
76_43
76_46
76_49
T;
&Mode_Run ' T76 SETTASK
: 79_0
0 JUMP ;
: 79_1
  0.0  
  ^Bio_Trim_Primary_Air @!   
  0.0  
  ^Bio_Trim_Tertiary @!   
  0.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  0    $20000.. 3 ROLL !PID
  2000.0  
  ^Bio_Setpoint_Water_Spray @!   
  |Feed_Rate 
  1    $20000.. 3 ROLL !PID
  |Primary_Air 
  1    $20000.. 3 ROLL !PID
  |Secondary_Air 
  1    $20000.. 3 ROLL !PID
  |Tertiary_Air 
  1    $20000.. 3 ROLL !PID
  |Gasifier_Pressure 
  0    $20000.. 3 ROLL !PID
  |Scrubber_Inlet_PID 
  1    $20000.. 3 ROLL !PID
  |Feed_Rate 
  0.0    $0008.. 3 ROLL !PID
  |Primary_Air 
  0.0    $0008.. 3 ROLL !PID
  |Secondary_Air 
  0.0    $0008.. 3 ROLL !PID
  |Tertiary_Air 
  0.0    $0008.. 3 ROLL !PID
  |Gasifier_Pressure 
  0.0    $0002.. 3 ROLL !PID
  |Scrubber_Inlet_PID 
  100.0    $0008.. 3 ROLL !PID
0 JUMP ;
: 79_2
  100    DELAY
0 JUMP ;
: 79_14
-2 JUMP ;
T: T79
DUMMY
79_0
79_1
79_2
79_14
T;
&Mode_Shutdown ' T79 SETTASK
: 78_0
2 JUMP ;
: 78_37
  |Gasifier_Pressure 
  -0.03    $0002.. 3 ROLL !PID
0 JUMP ;
: 78_39
  100    DELAY
-2 JUMP ;
: 78_40
  0.0  
  ^Bio_Trim_Primary_Air @!   
  0.0  
  ^Bio_Trim_Tertiary @!   
  0.0  
  ^Bio_Feed_Rate_Trim @!   
  |Feed_Rate 
  0    $20000.. 3 ROLL !PID
  2000.0  
  ^Bio_Setpoint_Water_Spray @!   
  |Feed_Rate 
  1    $20000.. 3 ROLL !PID
  |Primary_Air 
  1    $20000.. 3 ROLL !PID
  |Secondary_Air 
  1    $20000.. 3 ROLL !PID
  |Tertiary_Air 
  1    $20000.. 3 ROLL !PID
  |Gasifier_Pressure 
  1    $20000.. 3 ROLL !PID
  |Scrubber_Inlet_PID 
  1    $20000.. 3 ROLL !PID
  |Feed_Rate 
  5.0    $0008.. 3 ROLL !PID
  |Primary_Air 
  25.0    $0008.. 3 ROLL !PID
  |Secondary_Air 
  0.0    $0008.. 3 ROLL !PID
  |Tertiary_Air 
  0.0    $0008.. 3 ROLL !PID
  |Gasifier_Pressure 
  40.0    $0008.. 3 ROLL !PID
  |Scrubber_Inlet_PID 
  100.0    $0008.. 3 ROLL !PID
-2 JUMP ;
T: T78
DUMMY
78_0
78_37
78_39
78_40
T;
&Mode_Startup ' T78 SETTASK
: 70_0
5 JUMP ;
: 70_3
  ~Bio_Air_Moisturizer_Outlet_Pump   OFF
  0.0  
  ^Bio_Timer_Moisturizer_Out_ON @!   
  ^Bio_Timer_Moisturizer_Out_ON   StopTimer
  ^Bio_Timer_Moisturizer_Out_OFF   StartTimer
6 JUMP ;
: 70_6
  ~Bio_Air_Moisturizer_Outlet_Pump   ON
2 JUMP ;
: 70_9
  1.0    FDELAY
-2 JUMP ;
: 70_13
  ^Bio_Timer_Moisturizer_Out_ON   StartTimer
-3 JUMP ;
: 70_2
FALSE
  0  
  ^Bio_Index_Alarms_Moisturizer_Water_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Moisturizer_Water_Low @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Moisturizer_Water_DP_Low @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Moisturizer_Water_DP_Low @@ 
  }Bio_Warnings TABLE@   <>
LOR
IF 1 ELSE -3 THEN JUMP ;
: 70_12
TRUE
  ^Bio_Timer_Moisturizer_Out_ON @@F 
  1.0    F>
LAND
IF -5 ELSE -3 THEN JUMP ;
: 70_27
TRUE
  ^Bio_Timer_Moisturizer_Out_ON @@F 
  ^Bio_Setpoint_Moisturizer_Pumpout_Time @@F   F>=
LAND
IF -7 ELSE -5 THEN JUMP ;
T: T70
DUMMY
70_0
70_3
70_6
70_9
70_13
70_2
70_12
70_27
T;
&Moisturizer_Pumpout ' T70 SETTASK
: 68_0
4 JUMP ;
: 68_2
  ^Bio_Delay_Ph_Check @@   FDELAY
-2 JUMP ;
: 68_3
  ~Bio_Ph_Pump   ON
0 JUMP ;
: 68_6
  1.0    FDELAY
-4 JUMP ;
: 68_8
  ~Bio_Ph_Pump   OFF
-4 JUMP ;
: 68_1
TRUE
  ~Bio_Input_PH_Level @@ 
  ^Bio_Setpoint_Ph_Level @@   F<=
LAND
IF -4 ELSE -2 THEN JUMP ;
T: T68
DUMMY
68_0
68_2
68_3
68_6
68_8
68_1
T;
&Ph_Dose ' T68 SETTASK
: 0_0
0 JUMP ;
: 0_99
~Bio_Input_Incineration_Diverter_Open_Switch  DISABLE 
~Bio_Input_Incineration_Diverter_Open_Switch  IN.ON 
|Secondary_Air 1  $20000.. 3 ROLL !PID 
|Secondary_Air 0 I>F  $0008.. 3 ROLL !PID 
" 2.1.3" *Bio_Version  $MOVE 
30 I>F ^Bio_Setpoint_Steam_Pressure @! 
|Feed_Rate 2.750000e+000  $0002.. 3 ROLL !PID 
3.000000e+000 ^Bio_Trim_Tertiary @! 
1572 I>F ^Bio_Incineration_Temp_Minimum @! 
150 I>F ^Bio_Delay_Ash_Removal_Time @! 
30000 ^Bio_Delay_Day_Bin_Clear_Conveyors_Msec @! 
15000 ^Bio_Delay_Silo_Auger_Jam_Clear @! 
120 I>F ^Bio_Feed_Screw_Period @! 
^Bio_Feed_Screw_Period @@ ^Bio_Timer_Feed_Screw_Period  SetTimer 
^Bio_Timer_Feed_Screw_Period  StartTimer 
60 I>F ^Bio_Check_Database_Timer  SetTimer 
4.400000e-002 ^Bio_Chip_Volume_Factor @! 
10 I>F ~Bio_Secondary_Air_Spray_1  F!TPO. 
10 I>F ~Bio_Secondary_Air_Spray_2  F!TPO. 
10 I>F ~Bio_Secondary_Air_Spray_3  F!TPO. 
10 I>F ~Bio_Tertiary_Air_Spray  F!TPO. 
2125 I>F ^Bio_Setpoint_Water_Spray @! 
2100 ^Bio_Setpoint_Above_Door_Spray @! 
1 ^Bio_State_Spray_Lance_PID @! 
60 2 * I>F ^Bio_Delay_Ph_Check @! 
20 I>F ^Bio_Delay_Ph_Dose_Time @! 
8.000000e+000 ^Bio_Setpoint_Ph_Level @! 
" 74.125.29.108" *Bio_Email_Server_Hostname  $MOVE 
" 465" *Bio_Email_Server_Port  $MOVE 
" tls" *Bio_Email_Server_Protocol  $MOVE 
|Gasifier_Pressure 0  $20000.. 3 ROLL !PID 
|Feed_Rate 1  $20000.. 3 ROLL !PID 
|Scrubber_Inlet_PID 1  $20000.. 3 ROLL !PID 
|Quench_Water_Inlet_Flow 1  $20000.. 3 ROLL !PID 
|Scrubber_Inlet_PID 30 I>F  $0008.. 3 ROLL !PID 
10 I>F ^Bio_Timer_Moisturizer_Adjust_Time @! 
1 ^Bio_System_Enabled_Day_Bin @! 
0 ^Bio_System_Enabled_Ash_Removal @! 
0 ^Bio_System_Enabled_Moisturizer @! 
0 ^Bio_System_Enabled_Scrubber @! 
1 ^Bio_Feed_Auto @! 
|Secondary_Air 1500 I>F  $0002.. 3 ROLL !PID 
|Tertiary_Air 2100 I>F  $0002.. 3 ROLL !PID 
1 I>F ^Bio_Timer_Since_Volume_Calculation  SetTimer 
^Bio_Timer_Since_Volume_Calculation  StartTimer 
60 15 * I>F ^Bio_Timer_Since_Volume_Storage  SetTimer 
^Bio_Timer_Since_Volume_Storage  StartTimer 
60 120 * I>F ^Bio_Timer_Day_Bin_Filling  SetTimer 
0 ^Bio_Index_Operation_Modes_Off @! 
" Off" ^Bio_Index_Operation_Modes_Off @@ {Bio_Operation_Mode_Strings $TABLE@  $MOVE 
1 ^Bio_Index_Operation_Modes_Run @! 
" Run" ^Bio_Index_Operation_Modes_Run @@ {Bio_Operation_Mode_Strings $TABLE@  $MOVE 
2 ^Bio_Index_Operation_Modes_Fuel_Saver @! 
" Fuel Saver" ^Bio_Index_Operation_Modes_Fuel_Saver @@ {Bio_Operation_Mode_Strings $TABLE@  $MOVE 
3 ^Bio_Index_Operation_Modes_Startup @! 
" Startup" ^Bio_Index_Operation_Modes_Startup @@ {Bio_Operation_Mode_Strings $TABLE@  $MOVE 
4 ^Bio_Index_Operation_Modes_Shutdown @! 
" Shutdown" ^Bio_Index_Operation_Modes_Shutdown @@ {Bio_Operation_Mode_Strings $TABLE@  $MOVE 
~Bio_Quench_3_Nozzle_3  ON 
~Bio_Quench_4_Nozzle_3  ON 
~Bio_Quench_3_Nozzle_1  ON 
~Bio_Quench_3_Nozzle_2  ON 
~Bio_Quench_3_Nozzle_4  ON 
~Bio_Quench_4_Nozzle_1  ON 
~Bio_Quench_4_Nozzle_2  ON 
~Bio_Quench_4_Nozzle_4  ON 
15 60 * I>F ^Bio_Timer_15_Minute  SetTimer 
^Bio_Timer_15_Minute  StartTimer 
^Bio_Timer_Feed_Per_15  StartTimer 
60 5 *  ^Bio_Day_Bin_TPO_Period @! 
^Bio_Day_Bin_TPO_Period @@F ~Bio_Day_Bin_Spray  F!TPO. 
2 JUMP ;
: 0_109
  &VFD_Serial   START.T
  ^status_trashcan @! 
  &Read_Sensors   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_116
  &Alarms   START.T
  ^status_trashcan @! 
2 JUMP ;
: 0_128
1 ^Bio_Index_Alarm_Type_Digital @! 
2 ^Bio_Index_Alarm_Type_Analog_High @! 
3 ^Bio_Index_Alarm_Type_Analog_Low @! 
4 ^Bio_Index_Alarm_Type_Torque @! 
5 ^Bio_Index_Alarm_Type_Custom @! 
0 ^Bio_Index_Alarms_Boiler_3_Over_Pressure @! 
" Boiler 3 Over Pressure" ^Bio_Index_Alarms_Boiler_3_Over_Pressure @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_3_Over_Pressure @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_3_Over_Pressure 
^Bio_Index_Alarms_Boiler_3_Over_Pressure @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_3_Over_Pressure @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_3_Over_Pressure @@ }Bio_Alarm_Delay TABLE! 
1 ^Bio_Index_Alarms_Boiler_3_Water_Critical @! 
" Boiler 3 Water Critical" ^Bio_Index_Alarms_Boiler_3_Water_Critical @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_3_Water_Critical @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_3_Water_Critical 
^Bio_Index_Alarms_Boiler_3_Water_Critical @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_3_Water_Critical @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_3_Water_Critical @@ }Bio_Alarm_Delay TABLE! 
2 ^Bio_Index_Alarms_Boiler_4_Over_Pressure @! 
" Boiler 4 Over Pressure" ^Bio_Index_Alarms_Boiler_4_Over_Pressure @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_4_Over_Pressure @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_4_Over_Pressure 
^Bio_Index_Alarms_Boiler_4_Over_Pressure @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_4_Over_Pressure @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_4_Over_Pressure @@ }Bio_Alarm_Delay TABLE! 
3 ^Bio_Index_Alarms_Boiler_4_Water_Critical @! 
" Boiler 4 Water Critical" ^Bio_Index_Alarms_Boiler_4_Water_Critical @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_4_Water_Critical @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_4_Water_Critical 
^Bio_Index_Alarms_Boiler_4_Water_Critical @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_4_Water_Critical @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_4_Water_Critical @@ }Bio_Alarm_Delay TABLE! 
4 ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @! 
" Boiler 3 Inlet Temperature High" ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Boiler_3_Inlet_Temp 
^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
1800 I>F ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
60 5 *  ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ }Bio_Warning_Delay TABLE! 
60 10 *  ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @@ }Bio_Alarm_Delay TABLE! 
5 ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @! 
" Feed Auger 1 Temperature High" ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Feed_Auger_1_Temp 
^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
4.000000e+002 ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @@ }Bio_Alarm_Delay TABLE! 
6 ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @! 
" Feed Auger 2 Temperature High" ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Feed_Auger_2_Temp 
^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
4.000000e+002 ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @@ }Bio_Alarm_Delay TABLE! 
7 ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @! 
" Feed Auger 3 Temperature High" ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Feed_Auger_3_Temp 
^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
4.000000e+002 ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @@ }Bio_Alarm_Delay TABLE! 
8 ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @! 
" Feed Auger 4 Temperature High" ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Feed_Auger_4_Temp 
^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
4.000000e+002 ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @@ }Bio_Alarm_Delay TABLE! 
9 ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @! 
" Feed Auger 5 Temperature High" ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Feed_Auger_5_Temp 
^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
4.000000e+002 ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @@ }Bio_Alarm_Delay TABLE! 
10 ^Bio_Index_Alarms_Gasifier_DP_High @! 
" Gasifier Differential Pressure High" ^Bio_Index_Alarms_Gasifier_DP_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Gasifier_DP_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Gasifier_Pressure 
^Bio_Index_Alarms_Gasifier_DP_High @@ PTBL_Bio_Alarm_Points TABLE!
1.000000e-001 ^Bio_Index_Alarms_Gasifier_DP_High @@ }Bio_Alarm_Trigger_Points TABLE! 
20 ^Bio_Index_Alarms_Gasifier_DP_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Gasifier_DP_High @@ }Bio_Warning_Delay TABLE! 
180 ^Bio_Index_Alarms_Gasifier_DP_High @@ }Bio_Alarm_Delay TABLE! 
11 ^Bio_Index_Alarms_Gasifier_Temp_High @! 
" Gasifier Temperature High" ^Bio_Index_Alarms_Gasifier_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Gasifier_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Gasifier_Top_Temp_1 
^Bio_Index_Alarms_Gasifier_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
2100 I>F ^Bio_Index_Alarms_Gasifier_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
5 ^Bio_Index_Alarms_Gasifier_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Gasifier_Temp_High @@ }Bio_Alarm_Delay TABLE! 
12 ^Bio_Index_Alarms_Primary_Air_Temp_Low @! 
" Primary Air Temperature Low" ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_Low @@ ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ }Bio_Alarm_Types TABLE! 
^Bio_Air_Super_Heater_Outlet_Temp 
^Bio_Index_Alarms_Primary_Air_Temp_Low @@ PTBL_Bio_Alarm_Points TABLE!
120 I>F ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
60 ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ }Bio_Info_Delay TABLE! 
60 60 *  3 *  ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ }Bio_Warning_Delay TABLE! 
60 60 *  8 *  ^Bio_Index_Alarms_Primary_Air_Temp_Low @@ }Bio_Alarm_Delay TABLE! 
13 ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @! 
" Boiler 4 Inlet Temperature High" ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Boiler_4_Inlet_Temp 
^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
2300 I>F ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
60 5 *  ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ }Bio_Warning_Delay TABLE! 
60 10 *  ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @@ }Bio_Alarm_Delay TABLE! 
14 ^Bio_Index_Alarms_Quench_3_Temp_High @! 
" Quench 3 Outlet Temperature High" ^Bio_Index_Alarms_Quench_3_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Quench_3_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Quench_3_Outlet_Temp 
^Bio_Index_Alarms_Quench_3_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
300 I>F ^Bio_Index_Alarms_Quench_3_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
20 ^Bio_Index_Alarms_Quench_3_Temp_High @@ }Bio_Info_Delay TABLE! 
60 2 *  ^Bio_Index_Alarms_Quench_3_Temp_High @@ }Bio_Warning_Delay TABLE! 
60 15 *  ^Bio_Index_Alarms_Quench_3_Temp_High @@ }Bio_Alarm_Delay TABLE! 
15 ^Bio_Index_Alarms_Quench_4_Temp_High @! 
" Quench 4 Outlet Temperature High" ^Bio_Index_Alarms_Quench_4_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Quench_4_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Quench_4_Outlet_Temp 
^Bio_Index_Alarms_Quench_4_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
300 I>F ^Bio_Index_Alarms_Quench_4_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
20 ^Bio_Index_Alarms_Quench_4_Temp_High @@ }Bio_Info_Delay TABLE! 
60 2 *  ^Bio_Index_Alarms_Quench_4_Temp_High @@ }Bio_Warning_Delay TABLE! 
60 15 *  ^Bio_Index_Alarms_Quench_4_Temp_High @@ }Bio_Alarm_Delay TABLE! 
16 ^Bio_Index_Alarms_Scrubber_Temp_High @! 
" Scrubber Outlet Temperature High" ^Bio_Index_Alarms_Scrubber_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Scrubber_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Scrubber_Outlet_Temp 
^Bio_Index_Alarms_Scrubber_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
1.900000e+002 ^Bio_Index_Alarms_Scrubber_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
30 ^Bio_Index_Alarms_Scrubber_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Scrubber_Temp_High @@ }Bio_Alarm_Delay TABLE! 
17 ^Bio_Index_Alarms_Ash_Temp_High @! 
" Ash Temperature High" ^Bio_Index_Alarms_Ash_Temp_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Ash_Temp_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Ash_Temp 
^Bio_Index_Alarms_Ash_Temp_High @@ PTBL_Bio_Alarm_Points TABLE!
700 I>F ^Bio_Index_Alarms_Ash_Temp_High @@ }Bio_Alarm_Trigger_Points TABLE! 
30 ^Bio_Index_Alarms_Ash_Temp_High @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Ash_Temp_High @@ }Bio_Alarm_Delay TABLE! 
18 ^Bio_Index_Alarms_Boiler_3_Water_High @! 
" Boiler 3 Water High" ^Bio_Index_Alarms_Boiler_3_Water_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_3_Water_High @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_3_Water_High 
^Bio_Index_Alarms_Boiler_3_Water_High @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_3_Water_High @@ }Bio_Alarm_Trigger_Points TABLE! 
5 ^Bio_Index_Alarms_Boiler_3_Water_High @@ }Bio_Info_Delay TABLE! 
7200 ^Bio_Index_Alarms_Boiler_3_Water_High @@ }Bio_Warning_Delay TABLE! 
7200 ^Bio_Index_Alarms_Boiler_3_Water_High @@ }Bio_Alarm_Delay TABLE! 
19 ^Bio_Index_Alarms_Boiler_3_Water_Low @! 
" Boiler 3 Water Low" ^Bio_Index_Alarms_Boiler_3_Water_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_3_Water_Low @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_3_Water_Low 
^Bio_Index_Alarms_Boiler_3_Water_Low @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_3_Water_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_3_Water_Low @@ }Bio_Info_Delay TABLE! 
300 ^Bio_Index_Alarms_Boiler_3_Water_Low @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Boiler_3_Water_Low @@ }Bio_Alarm_Delay TABLE! 
20 ^Bio_Index_Alarms_Boiler_4_Water_High @! 
" Boiler 4 Water High" ^Bio_Index_Alarms_Boiler_4_Water_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_4_Water_High @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_4_Water_High 
^Bio_Index_Alarms_Boiler_4_Water_High @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Boiler_4_Water_High @@ }Bio_Alarm_Trigger_Points TABLE! 
5 ^Bio_Index_Alarms_Boiler_4_Water_High @@ }Bio_Info_Delay TABLE! 
7200 ^Bio_Index_Alarms_Boiler_4_Water_High @@ }Bio_Warning_Delay TABLE! 
7200 ^Bio_Index_Alarms_Boiler_4_Water_High @@ }Bio_Alarm_Delay TABLE! 
21 ^Bio_Index_Alarms_Boiler_4_Water_Low @! 
" Boiler 4 Water Low" ^Bio_Index_Alarms_Boiler_4_Water_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Boiler_4_Water_Low @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Boiler_4_Water_Low 
^Bio_Index_Alarms_Boiler_4_Water_Low @@ PTBL_Bio_Alarm_Points TABLE!
1 I>F ^Bio_Index_Alarms_Boiler_4_Water_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Boiler_4_Water_Low @@ }Bio_Info_Delay TABLE! 
300 ^Bio_Index_Alarms_Boiler_4_Water_Low @@ }Bio_Warning_Delay TABLE! 
300 ^Bio_Index_Alarms_Boiler_4_Water_Low @@ }Bio_Alarm_Delay TABLE! 
22 ^Bio_Index_Alarms_Day_Bin_High_PE @! 
" Day Bin High" ^Bio_Index_Alarms_Day_Bin_High_PE @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Day_Bin_High_PE @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Daybin_High_PE 
^Bio_Index_Alarms_Day_Bin_High_PE @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Day_Bin_High_PE @@ }Bio_Alarm_Trigger_Points TABLE! 
15 ^Bio_Index_Alarms_Day_Bin_High_PE @@ }Bio_Info_Delay TABLE! 
60 10 *  ^Bio_Index_Alarms_Day_Bin_High_PE @@ }Bio_Warning_Delay TABLE! 
36000 ^Bio_Index_Alarms_Day_Bin_High_PE @@ }Bio_Alarm_Delay TABLE! 
23 ^Bio_Index_Alarms_Day_Bin_Low @! 
" Day Bin Low" ^Bio_Index_Alarms_Day_Bin_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Day_Bin_Low @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Day_Bin_Low 
^Bio_Index_Alarms_Day_Bin_Low @@ PTBL_Bio_Alarm_Points TABLE!
1 I>F ^Bio_Index_Alarms_Day_Bin_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
15 ^Bio_Index_Alarms_Day_Bin_Low @@ }Bio_Info_Delay TABLE! 
300 ^Bio_Index_Alarms_Day_Bin_Low @@ }Bio_Warning_Delay TABLE! 
600 ^Bio_Index_Alarms_Day_Bin_Low @@ }Bio_Alarm_Delay TABLE! 
28 ^Bio_Index_Alarms_Steam_Pressure_High @! 
" Steam Pressure High" ^Bio_Index_Alarms_Steam_Pressure_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Steam_Pressure_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Boiler_Steam_Pressure 
^Bio_Index_Alarms_Steam_Pressure_High @@ PTBL_Bio_Alarm_Points TABLE!
6.000000e+001 ^Bio_Index_Alarms_Steam_Pressure_High @@ }Bio_Alarm_Trigger_Points TABLE! 
10 ^Bio_Index_Alarms_Steam_Pressure_High @@ }Bio_Info_Delay TABLE! 
600 ^Bio_Index_Alarms_Steam_Pressure_High @@ }Bio_Warning_Delay TABLE! 
600 ^Bio_Index_Alarms_Steam_Pressure_High @@ }Bio_Alarm_Delay TABLE! 
29 ^Bio_Index_Alarms_Steam_Pressure_Low @! 
" Steam Pressure Low" ^Bio_Index_Alarms_Steam_Pressure_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_Low @@ ^Bio_Index_Alarms_Steam_Pressure_Low @@ }Bio_Alarm_Types TABLE! 
^Bio_Boiler_Steam_Pressure 
^Bio_Index_Alarms_Steam_Pressure_Low @@ PTBL_Bio_Alarm_Points TABLE!
5.000000e+000 ^Bio_Index_Alarms_Steam_Pressure_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
10 ^Bio_Index_Alarms_Steam_Pressure_Low @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Steam_Pressure_Low @@ }Bio_Warning_Delay TABLE! 
120 ^Bio_Index_Alarms_Steam_Pressure_Low @@ }Bio_Alarm_Delay TABLE! 
30 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @! 
" Feed Screw 1 Torque High" ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Alarm_Types TABLE! 
4 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @@ }Bio_Alarm_Delay TABLE! 
31 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @! 
" Feed Screw 2 Torque High" ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Alarm_Types TABLE! 
5 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @@ }Bio_Alarm_Delay TABLE! 
32 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @! 
" Feed Screw 3 Torque High" ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Alarm_Types TABLE! 
6 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @@ }Bio_Alarm_Delay TABLE! 
33 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @! 
" Feed Screw 4 Torque High" ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Alarm_Types TABLE! 
7 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @@ }Bio_Alarm_Delay TABLE! 
34 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @! 
" Feed Screw 5 Torque High" ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Alarm_Types TABLE! 
8 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @@ }Bio_Alarm_Delay TABLE! 
35 ^Bio_Index_Alarms_Lift_Auger_Torque_High @! 
" Lift Auger Torque High" ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Alarm_Types TABLE! 
3 ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Lift_Auger_Torque_High @@ }Bio_Alarm_Delay TABLE! 
36 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @! 
" Silo 1 Auger Torque High" ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Torque @@ ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Alarm_Types TABLE! 
1 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Alarm_VFDs TABLE! 
7.500000e+001 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Info_Delay TABLE! 
60 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @@ }Bio_Alarm_Delay TABLE! 
41 ^Bio_Index_Alarms_System_Estop @! 
" System Estop" ^Bio_Index_Alarms_System_Estop @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_System_Estop @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Estop_Feedback 
^Bio_Index_Alarms_System_Estop @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_System_Estop @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_System_Estop @@ }Bio_Info_Delay TABLE! 
0 ^Bio_Index_Alarms_System_Estop @@ }Bio_Warning_Delay TABLE! 
0 ^Bio_Index_Alarms_System_Estop @@ }Bio_Alarm_Delay TABLE! 
42 ^Bio_Index_Alarms_Feed_System_Estop @! 
" Feed System Estop" ^Bio_Index_Alarms_Feed_System_Estop @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Feed_System_Estop @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Feed_System_Estop_Feedback 
^Bio_Index_Alarms_Feed_System_Estop @@ PTBL_Bio_Alarm_Points TABLE!
0 I>F ^Bio_Index_Alarms_Feed_System_Estop @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Feed_System_Estop @@ }Bio_Info_Delay TABLE! 
0 ^Bio_Index_Alarms_Feed_System_Estop @@ }Bio_Warning_Delay TABLE! 
0 ^Bio_Index_Alarms_Feed_System_Estop @@ }Bio_Alarm_Delay TABLE! 
43 ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @! 
" Safety Water Pressure Low" ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_Low @@ ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ }Bio_Alarm_Types TABLE! 
^Bio_Safety_Water_Pressure 
^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ PTBL_Bio_Alarm_Points TABLE!
10 I>F ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
5 ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ }Bio_Info_Delay TABLE! 
60 60 *  ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ }Bio_Warning_Delay TABLE! 
60 60 *  12 *  ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @@ }Bio_Alarm_Delay TABLE! 
45 ^Bio_Index_Alarms_Ash_Temp_Low @! 
" Ash Temperature Low" ^Bio_Index_Alarms_Ash_Temp_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_Low @@ ^Bio_Index_Alarms_Ash_Temp_Low @@ }Bio_Alarm_Types TABLE! 
^Bio_Ash_Temp 
^Bio_Index_Alarms_Ash_Temp_Low @@ PTBL_Bio_Alarm_Points TABLE!
60 I>F ^Bio_Index_Alarms_Ash_Temp_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
30 ^Bio_Index_Alarms_Ash_Temp_Low @@ }Bio_Info_Delay TABLE! 
-1 ^Bio_Index_Alarms_Ash_Temp_Low @@ }Bio_Warning_Delay TABLE! 
-1 ^Bio_Index_Alarms_Ash_Temp_Low @@ }Bio_Alarm_Delay TABLE! 
46 ^Bio_Index_Alarms_Condensate_pH_Low @! 
" Condensate pH Low" ^Bio_Index_Alarms_Condensate_pH_Low @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_Low @@ ^Bio_Index_Alarms_Condensate_pH_Low @@ }Bio_Alarm_Types TABLE! 
^Bio_Condensate_pH 
^Bio_Index_Alarms_Condensate_pH_Low @@ PTBL_Bio_Alarm_Points TABLE!
7 I>F ^Bio_Index_Alarms_Condensate_pH_Low @@ }Bio_Alarm_Trigger_Points TABLE! 
0 ^Bio_Index_Alarms_Condensate_pH_Low @@ }Bio_Info_Delay TABLE! 
30 60 *  ^Bio_Index_Alarms_Condensate_pH_Low @@ }Bio_Warning_Delay TABLE! 
-1 ^Bio_Index_Alarms_Condensate_pH_Low @@ }Bio_Alarm_Delay TABLE! 
47 ^Bio_Index_Alarms_Moisturizer_Water_DP_High @! 
" Moisturizer Water High (DP)" ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Analog_High @@ ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ }Bio_Alarm_Types TABLE! 
^Bio_Air_Moisturizer_Level 
^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ PTBL_Bio_Alarm_Points TABLE!
3 I>F ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ }Bio_Alarm_Trigger_Points TABLE! 
1 ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ }Bio_Info_Delay TABLE! 
5 ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ }Bio_Warning_Delay TABLE! 
60 ^Bio_Index_Alarms_Moisturizer_Water_DP_High @@ }Bio_Alarm_Delay TABLE! 
48 ^Bio_Index_Alarms_Conveyor_Jam1 @! 
" Conveyor Belt Jam1" ^Bio_Index_Alarms_Conveyor_Jam1 @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Conveyor_Jam1 @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Conveyor_Blockage1 
^Bio_Index_Alarms_Conveyor_Jam1 @@ PTBL_Bio_Alarm_Points TABLE!
1 I>F ^Bio_Index_Alarms_Conveyor_Jam1 @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Conveyor_Jam1 @@ }Bio_Info_Delay TABLE! 
5 ^Bio_Index_Alarms_Conveyor_Jam1 @@ }Bio_Warning_Delay TABLE! 
10 ^Bio_Index_Alarms_Conveyor_Jam1 @@ }Bio_Alarm_Delay TABLE! 
49 ^Bio_Index_Alarms_Conveyor_Jam2 @! 
" Conveyor Belt Jam2" ^Bio_Index_Alarms_Conveyor_Jam2 @@ {Bio_Alarm_Descriptions $TABLE@  $MOVE 
^Bio_Index_Alarm_Type_Digital @@ ^Bio_Index_Alarms_Conveyor_Jam2 @@ }Bio_Alarm_Types TABLE! 
~Bio_Input_Conveyor_Blockage2 
^Bio_Index_Alarms_Conveyor_Jam2 @@ PTBL_Bio_Alarm_Points TABLE!
1 I>F ^Bio_Index_Alarms_Conveyor_Jam2 @@ }Bio_Alarm_Trigger_Points TABLE! 
2 ^Bio_Index_Alarms_Conveyor_Jam2 @@ }Bio_Info_Delay TABLE! 
5 ^Bio_Index_Alarms_Conveyor_Jam2 @@ }Bio_Warning_Delay TABLE! 
10 ^Bio_Index_Alarms_Conveyor_Jam2 @@ }Bio_Alarm_Delay TABLE! 
0 JUMP ;
: 0_134
}Bio_VFD_Data_Silo_1_Augers 
1 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Belt_Conveyor 
2 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Lift_Auger 
3 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Fuel_Feeder_Screw_1 
4 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Fuel_Feeder_Screw_2 
5 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Fuel_Feeder_Screw_3 
6 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Fuel_Feeder_Screw_4 
7 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Fuel_Feeder_Screw_5 
8 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Ash_Removal_Screws 
9 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Quench_Outlet_Pump_1 
10 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Quench_Outlet_Pump_2 
11 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Scrubber_Outlet_Pump 
12 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Air_Moisturizer_Outlet_Pump 
13 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Quench_Inlet_Pump 
14 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Scrubber_Inlet_Pump 
15 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Air_Moisturizer_Inlet_Pump 
16 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Primary_Air_Fan 
17 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Secondary_Air_Fan 
18 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Tertiary_Air_Fan 
19 PTBL_Bio_VFD_Data TABLE!
}Bio_VFD_Data_Stack_Fan 
20 PTBL_Bio_VFD_Data TABLE!
" tcp:192.168.1.187:22500" *Modbus_Port  $MOVE 
}VFD_Data_Int 
0 PTBL_VFD_Data TABLE!
}VFD_Data_Float 
1 PTBL_VFD_Data TABLE!
2050 1 }VFD_Setup TABLE! 
50 2 }VFD_Setup TABLE! 
500 3 }VFD_Setup TABLE! 
0 4 }VFD_Setup TABLE! 
0 5 }VFD_Setup TABLE! 
0 6 }VFD_Setup TABLE! 
0 7 }VFD_Setup TABLE! 
0 ^Bio_Index_VFD_Frequency @! 
1 ^Bio_Index_VFD_Volts @! 
2 ^Bio_Index_VFD_Amps @! 
3 ^Bio_Index_VFD_Alarm @! 
4 ^Bio_Index_VFD_Capacitor_Life @! 
5 ^Bio_Index_VFD_PC_Board_Capacitor_Life @! 
6 ^Bio_Index_VFD_Heatsink_Life @! 
7 ^Bio_Index_VFD_Heatsink_Temp @! 
-4 JUMP ;
: 0_139
  &Mode_Management   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_141
  &Safety_Systems   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_147
  &Feed_Section   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_150
  &Gasifier   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_152
  &Boilers   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_156
  &Incineration   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_158
  &Ph_Dose   START.T
  ^status_trashcan @! 
0 JUMP ;
: 0_160
  &Day_Bin_Fill   START.T
  ^status_trashcan @! 
1 JUMP ;
: 0_165
  &Day_Bin_Timed_Fill   START.T
  ^status_trashcan @! 
0 JUMP ;
T: T0
DUMMY
0_0
0_99
0_109
0_116
0_128
0_134
0_139
0_141
0_147
0_150
0_152
0_156
0_158
0_160
0_165
T;
&Powerup ' T0 SETTASK
: 26_0
24 JUMP ;
: 26_140
  ~Bio_Input_Air_Moisturizer_Air_Outlet_Temp @@ 
  ^Bio_Air_Moisturizer_Air_Outlet_Temp @!   
26 JUMP ;
: 26_143
  ~Bio_Input_Boiler_3_Outlet_Temp @@ 
  ^Bio_Boiler_3_Outlet_Temp @!   
0 JUMP ;
: 26_144
  ~Bio_Input_Boiler_4_Exit_Temp @@ 
  ^Bio_Boiler_4_Outlet_Temp @!   
29 JUMP ;
: 26_148
  ~Bio_Input_Gasifier_Temp_4 @@ 
  ^Bio_Gasifier_Temp_4 @!   
13 JUMP ;
: 26_157
  ~Bio_Input_Air_Moisturizer_Water_Temp @@ 
  ^Bio_Air_Moisturizer_Water_Temp @!   
34 JUMP ;
: 26_162
  ~Bio_Input_Above_Door_Temp @@ 
  ^Bio_Above_Door_Temp @!   
27 JUMP ;
: 26_213
  ~Bio_Input_Air_Moisturizer_Inlet_Water_Flow @@ 
  ^Bio_Air_Moisturizer_Water_Inlet_Flow @!   
35 JUMP ;
: 26_245
~Bio_Input_Boiler_Steam_Pressure_1 @@ ^Bio_Boiler_Steam_Pressure @! 
|Primary_Air ^Bio_Boiler_Steam_Pressure @@  $0001.. 3 ROLL !PID 
3 JUMP ;
: 26_336
  |Gasifier_Pressure 
  ^Bio_Gasifier_Pressure @@   $0001.. 3 ROLL !PID
26 JUMP ;
: 26_342
  100    DELAY
-11 JUMP ;
: 26_392
  ~Bio_Input_Quench_Inlet_Water_Pressure @@ 
  ^Bio_Quench_Nozzle_Water_Pressure @!   
-4 JUMP ;
: 26_569
  ~Bio_Input_Air_Moisturizer_Level @@ 
  ^Bio_Air_Moisturizer_Level @!   
10 JUMP ;
: 26_573
  ~Bio_Input_Feed_Auger_1_Temp @@ 
  ^Bio_Feed_Auger_1_Temp @!   
0 JUMP ;
: 26_574
  ~Bio_Input_Feed_Auger_2_Temp @@ 
  ^Bio_Feed_Auger_2_Temp @!   
1 JUMP ;
: 26_575
  ~Bio_Input_Feed_Auger_4_Temp @@ 
  ^Bio_Feed_Auger_4_Temp @!   
1 JUMP ;
: 26_576
  ~Bio_Input_Feed_Auger_3_Temp @@ 
  ^Bio_Feed_Auger_3_Temp @!   
-2 JUMP ;
: 26_577
  ~Bio_Input_Feed_Auger_5_Temp @@ 
  ^Bio_Feed_Auger_5_Temp @!   
26 JUMP ;
: 26_594
  ~Bio_Input_Gasifier_Temp_5 @@ 
  ^Bio_Gasifier_Temp_5 @!   
0 JUMP ;
: 26_595
  ~Bio_Input_Gasifier_Temp_6 @@ 
  ^Bio_Gasifier_Temp_6 @!   
0 JUMP ;
: 26_596
  ~Bio_Input_Gasifier_Temp_7 @@ 
  ^Bio_Gasifier_Temp_7 @!   
6 JUMP ;
: 26_597
  ~Bio_Input_Gasifier_Temp_2 @@ 
  ^Bio_Gasifier_Temp_2 @!   
0 JUMP ;
: 26_599
  ~Bio_Input_Gasifier_Temp_3 @@ 
  ^Bio_Gasifier_Temp_3 @!   
-19 JUMP ;
: 26_632
  ~Bio_Input_Scrubber_Water_Level @@ 
  ^Bio_Scrubber_Water_Level @!   
17 JUMP ;
: 26_639
|Primary_Air  $0008.. ROT PID@ ^Bio_Trim_Primary_Air @@ F+ ~Bio_Primary_Air_Fan_Motor_Speed @! 
~Bio_Primary_Air_Fan_Motor_Speed @@ ^Bio_primary_max_speed @@F F> IF 
^Bio_primary_max_speed @@F ~Bio_Primary_Air_Fan_Motor_Speed @! 
THEN 
~Bio_Input_Primary_Air_Flow @@  FSQRT 2400 I>F F* 9 I>F 3.141590e+000 F* 144 I>F F/ F* ^Bio_Primary_Air_Flow @! 
|Feed_Rate  $0008.. ROT PID@ ^Bio_Feed_Rate_Trim @@ F+ ^Bio_Feeder_Screw_Speed @! 
-18 JUMP ;
: 26_642
~Bio_Input_Gasifier_Pressure @@ ^Bio_Gasifier_Pressure @! 
-17 JUMP ;
: 26_655
~Bio_Input_Stack_Exhaust_O2_Level_1 @@ ~Bio_Input_Stack_Exhaust_O2_Level_2 @@ F+ 200 I>F F/ ^Bio_Stack_Exhaust_O2_Level @! 
-16 JUMP ;
: 26_656
~Bio_Input_Gasifier_Top_Temp_1 @@ ^Bio_Gasifier_Top_Temp_1 @! 
|Tertiary_Air ^Bio_Gasifier_Top_Temp_1 @@  $0001.. 3 ROLL !PID 
-22 JUMP ;
: 26_657
~Bio_Input_Air_Super_Heater_Outlet_Temp_1 @@ ~Bio_Input_Air_Super_Heater_Outlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Air_Super_Heater_Outlet_Temp @! 
-24 JUMP ;
: 26_658
~Bio_Input_Quench_3_Outlet_Temp_1 @@ ~Bio_Input_Quench_3_Outlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Quench_3_Outlet_Temp @! 
0 JUMP ;
: 26_659
~Bio_Input_Quench_4_Outlet_Temp_1 @@ ~Bio_Input_Quench_4_Outlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Quench_4_Outlet_Temp @! 
^Bio_Quench_3_Outlet_Temp @@ ^Bio_Quench_4_Outlet_Temp @@ F> IF 
|Quench_Water_Inlet_Flow ^Bio_Quench_3_Outlet_Temp @@  $0001.. 3 ROLL !PID 
ELSE 
|Quench_Water_Inlet_Flow ^Bio_Quench_4_Outlet_Temp @@  $0001.. 3 ROLL !PID 
THEN 
1 JUMP ;
: 26_660
  ~Bio_Input_Ceiling_Temp @@ 
  ^Bio_Ceiling_Temp @!   
6 JUMP ;
: 26_661
~Bio_Input_Scrubber_Outlet_Temp_1 @@ ~Bio_Input_Scrubber_Outlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Scrubber_Outlet_Temp @! 
-2 JUMP ;
: 26_662
~Bio_Input_Ash_Temp_Left @@ ~Bio_Input_Ash_Temp_Right @@ F+ 2 I>F F/ ^Bio_Ash_Temp @! 
3 JUMP ;
: 26_663
~Bio_Input_Boiler_3_Inlet_Temp_1 @@ ~Bio_Input_Boiler_3_Inlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Boiler_3_Inlet_Temp @! 
0 JUMP ;
: 26_664
~Bio_Input_Boiler_4_Inlet_Temp_1 @@ ~Bio_Input_Boiler_4_Inlet_Temp_2 @@ F+ 2 I>F F/ ^Bio_Boiler_4_Inlet_Temp @! 
-34 JUMP ;
: 26_690
  |Gasifier_Pressure   $0008.. ROT PID@
  ~Bio_Stack_Fan_Motor_Speed @! 
-13 JUMP ;
: 26_706
^Bio_Timer_Since_Volume_Calculation  T0= IF 
^Bio_Fuel_Fed_Short @@ ^Bio_Feeder_Screw_Speed @@ 100 I>F F/ 6.000000e+001 F* ^Bio_Timer_Since_Volume_Calculation @@F F* 1 I>F F* 12 I>F 3 I>F  F^ F/ F+ ^Bio_Fuel_Fed_Short @! 
^Bio_Ash_Removed @@ ^Bio_Feeder_Screw_Speed @@ 100 I>F F/ 6.000000e+001 F* ^Bio_Timer_Since_Volume_Calculation @@F F* 1 I>F F* 12 I>F 3 I>F  F^ F/ F+ ^Bio_Ash_Removed @! 
^Bio_Timer_Since_Volume_Calculation  StartTimer 
THEN 
^Bio_Timer_Since_Volume_Storage  T0= IF 
^Bio_Fuel_Fed_Short @@ ^Bio_Fuel_Fed_12_Hour_Total @! 
1 46 1 + 0 DO? I ^Bio_Volume_Storage_Counter @! 
^Bio_Volume_Storage_Counter @@ 1 +  }Bio_Fuel_Fed TABLE@ ^Bio_Volume_Storage_Counter @@ }Bio_Fuel_Fed TABLE! 
^Bio_Fuel_Fed_12_Hour_Total @@ ^Bio_Volume_Storage_Counter @@ }Bio_Fuel_Fed TABLE@ F+ ^Bio_Fuel_Fed_12_Hour_Total @! 
1 +LOOP 
^Bio_Fuel_Fed_Short @@ 47 }Bio_Fuel_Fed TABLE! 
0 I>F ^Bio_Fuel_Fed_Short @! 
^Bio_Timer_Since_Volume_Storage  StartTimer 
THEN 
4 JUMP ;
: 26_709
~Bio_Input_Secondary_Air_Temp_1 @@ ~Bio_Input_Secondary_Air_Temp_2 @@ F+ 2 I>F F/ ^Bio_Secondary_Air_Temp @! 
|Secondary_Air_Low_Clamp ^Bio_Secondary_Air_Temp @@  $0001.. 3 ROLL !PID 
|Secondary_Air |Secondary_Air_Low_Clamp  $0008.. ROT PID@  $1000.. 3 ROLL !PID 
0 JUMP ;
: 26_710
~Bio_Input_Tertiary_Air_Temp_1 @@ ~Bio_Input_Tertiary_Air_Temp_2 @@ F+ 2 I>F F/ ^Bio_Tertiary_Air_Temp @! 
|Tertiary_Air_Low_Clamp ^Bio_Tertiary_Air_Temp @@  $0001.. 3 ROLL !PID 
|Tertiary_Air |Tertiary_Air_Low_Clamp  $0008.. ROT PID@  $1000.. 3 ROLL !PID 
|Tertiary_Air  $0008.. ROT PID@ ^Bio_Trim_Tertiary @@ F+ ~Bio_Tertiary_Air_Fan_Speed @! 
-27 JUMP ;
: 26_715
  ~Bio_Input_Air_Moisturizer_Water_Inlet_Temp @@ 
  ^Bio_Air_Moisturizer_Water_Inlet_Temp @!   
-12 JUMP ;
: 26_721
  ~Bio_Input_Safety_Water_Pressure @@ 
  ^Bio_Safety_Water_Pressure @!   
3 JUMP ;
: 26_727
3 JUMP ;
: 26_730
~Bio_Input_Bed_Height @@ ^Bio_Bed_Height @! 
-18 JUMP ;
: 26_733
~Bio_Input_Gasifier_Temp_1 @@ ^Bio_Gasifier_Temp_1 @! 
|Secondary_Air ^Bio_Gasifier_Temp_1 @@  $0001.. 3 ROLL !PID 
|Secondary_Air  $0008.. ROT PID@ ~Bio_Secondary_Air_Fan_Motor_Speed @! 
-24 JUMP ;
: 26_737
  ~Bio_Input_PH_Level @@ 
  ^Bio_Condensate_pH @!   
-45 JUMP ;
: 26_745
~Bio_Input_Ceiling_Temp @@ 110 I>F F< IF 
~Bio_Ventilation_Fan_On_Off  OFF 
ELSE 
~Bio_Input_Ceiling_Temp @@ 115 I>F F> IF 
~Bio_Ventilation_Fan_On_Off  ON 
THEN THEN 
0 JUMP ;
: 26_748
~Bio_Steam_Flow_1 @@ ~Bio_Steam_Flow_2 @@ F+ ~Bio_Steam_Flow_3 @@ F+ ^Bio_Steam_Flow_Total @! 
-38 JUMP ;
T: T26
DUMMY
26_0
26_140
26_143
26_144
26_148
26_157
26_162
26_213
26_245
26_336
26_342
26_392
26_569
26_573
26_574
26_575
26_576
26_577
26_594
26_595
26_596
26_597
26_599
26_632
26_639
26_642
26_655
26_656
26_657
26_658
26_659
26_660
26_661
26_662
26_663
26_664
26_690
26_706
26_709
26_710
26_715
26_721
26_727
26_730
26_733
26_737
26_745
26_748
T;
&Read_Sensors ' T26 SETTASK
: 54_0
13 JUMP ;
: 54_1
  ~Bio_Quench_3_Safety_Water_Valve   OFF
22 JUMP ;
: 54_2
  ~Bio_Quench_3_Safety_Water_Valve   ON
21 JUMP ;
: 54_9
  ~Bio_Quench_4_Safety_Water_Valve   ON
21 JUMP ;
: 54_10
  ~Bio_Quench_4_Safety_Water_Valve   OFF
20 JUMP ;
: 54_13
  ~Bio_Scrubber_Safety_Water_Valve   OFF
20 JUMP ;
: 54_14
  ~Bio_Scrubber_Safety_Water_Valve   ON
19 JUMP ;
: 54_56
  1.0    FDELAY
26 JUMP ;
: 54_100
  25.0  
  ~Bio_Secondary_Air_Spray_1   F!TPO%
  25.0  
  ~Bio_Secondary_Air_Spray_2   F!TPO%
  25.0  
  ~Bio_Secondary_Air_Spray_3   F!TPO%
  25.0  
  ~Bio_Tertiary_Air_Spray   F!TPO%
18 JUMP ;
: 54_107
  100.0  
  ~Bio_Secondary_Air_Spray_1   F!TPO%
  100.0  
  ~Bio_Secondary_Air_Spray_2   F!TPO%
  100.0  
  ~Bio_Secondary_Air_Spray_3   F!TPO%
  100.0  
  ~Bio_Tertiary_Air_Spray   F!TPO%
1 JUMP ;
: 54_108
  0.0  
  ~Bio_Secondary_Air_Spray_1   F!TPO%
  0.0  
  ~Bio_Secondary_Air_Spray_2   F!TPO%
  0.0  
  ~Bio_Secondary_Air_Spray_3   F!TPO%
  0.0  
  ~Bio_Tertiary_Air_Spray   F!TPO%
0 JUMP ;
: 54_155
16 JUMP ;
: 54_161
  ~Bio_Reset_Primary_VFD   ON
  1.0    FDELAY
  ~Bio_Reset_Primary_VFD   OFF
  ^Bio_Primary_Tripped_Counter   1+@!
16 JUMP ;
: 54_169
  ~Bio_Reset_Stack_VFD   ON
  1.0    FDELAY
  ~Bio_Reset_Stack_VFD   OFF
16 JUMP ;
: 54_172
8 JUMP ;
: 54_176
  ~Bio_Reset_Condensate1_VFD   ON
  1.0    FDELAY
  ~Bio_Reset_Condensate1_VFD   OFF
15 JUMP ;
: 54_183
  ~Bio_Silo_1_Augers_Forward   OFF
15 JUMP ;
: 54_190
  ~Bio_Topside_Blowdown_2   ON
15 JUMP ;
: 54_193
  ~Bio_Topside_Blowdown_2   OFF
-12 JUMP ;
: 54_204
-20 JUMP ;
: 54_210
  ^Bio_DayBin_TPO_Percent @@ 
  ~Bio_Day_Bin_Spray   F!TPO%
14 JUMP ;
: 54_212
  100 I>F 
  ~Bio_Day_Bin_Spray   F!TPO%
13 JUMP ;
: 54_223
  &Extinguish_Day_Bin   START.T
  ^status_trashcan @! 
-4 JUMP ;
: 54_3
FALSE
  0  
  ^Bio_Index_Alarms_Quench_3_Temp_High @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Quench_3_Temp_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Quench_3_Temp_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
IF -22 ELSE -23 THEN JUMP ;
: 54_8
FALSE
  0  
  ^Bio_Index_Alarms_Quench_4_Temp_High @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Quench_4_Temp_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Quench_4_Temp_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -22 ELSE -21 THEN JUMP ;
: 54_15
FALSE
  0  
  ^Bio_Index_Alarms_Scrubber_Temp_High @@ 
  }Bio_Info TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Temp_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
  0  
  ^Bio_Index_Alarms_Scrubber_Temp_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
IF -20 ELSE -21 THEN JUMP ;
: 54_101
FALSE
  ^Bio_Above_Door_Temp @@ 
  ^Bio_Gasifier_Warning_Temp @@   F>=
LOR
  ^Bio_Gasifier_Temp_1 @@ 
  ^Bio_Gasifier_Warning_Temp @@   F>=
LOR
  ^Bio_Gasifier_Temp_2 @@ 
  ^Bio_Gasifier_Warning_Temp @@   F>=
LOR
  ^Bio_Gasifier_Top_Temp_1 @@ 
  ^Bio_Gasifier_Warning_Temp @@   F>=
LOR
  0  
  ^Bio_Index_Alarms_Gasifier_Temp_High @@ 
  }Bio_Warnings TABLE@   <>
LOR
IF -19 ELSE -17 THEN JUMP ;
: 54_106
FALSE
  0  
  ^Bio_Index_Alarms_Gasifier_Temp_High @@ 
  }Bio_Alarms TABLE@   <>
LOR
  ^Bio_Above_Door_Temp @@ 
  ^Bio_Gasifer_Alarm_Temp @@   F>=
LOR
  ^Bio_Gasifier_Temp_1 @@ 
  ^Bio_Gasifer_Alarm_Temp @@   F>=
LOR
  ^Bio_Gasifier_Temp_2 @@ 
  ^Bio_Gasifer_Alarm_Temp @@   F>=
LOR
  ^Bio_Gasifier_Top_Temp_1 @@ 
  ^Bio_Gasifer_Alarm_Temp @@   F>=
LOR
IF -19 ELSE -17 THEN JUMP ;
: 54_159
TRUE
  ~Bio_Input_Primary_VFD_Error   ?ON
LAND
IF -17 ELSE 0 THEN JUMP ;
: 54_165
TRUE
  ~Bio_Input_Stack_VFD_Error   ?ON
LAND
IF -17 ELSE 0 THEN JUMP ;
: 54_177
TRUE
  ~Bio_Input_Condensate1_VFD_Error   ?ON
LAND
IF -16 ELSE 0 THEN JUMP ;
: 54_182
TRUE
  ~Bio_Belt_Conveyor_On_Off   ?OFF
LAND
IF -16 ELSE 0 THEN JUMP ;
: 54_189
TRUE
  ~Bio_Input_Boiler_Steam_Pressure_1 @@ 
  50.0    F>=
LAND
IF -16 ELSE 0 THEN JUMP ;
: 54_192
TRUE
  ~Bio_Input_Boiler_Steam_Pressure_1 @@ 
  45.0    F<=
LAND
IF -16 ELSE -27 THEN JUMP ;
: 54_211
TRUE
  ^Bio_DayBin_Spray_ON @@   0<>
LAND
IF -14 ELSE -15 THEN JUMP ;
: 54_222
TRUE
  ^Bio_Extinguish_Day_Bin_Fire @@   0<>
LAND
IF 0 ELSE -17 THEN JUMP ;
: 54_229
TRUE
  &Extinguish_Day_Bin   ?RUNNING
LAND
IF -18 ELSE -15 THEN JUMP ;
T: T54
DUMMY
54_0
54_1
54_2
54_9
54_10
54_13
54_14
54_56
54_100
54_107
54_108
54_155
54_161
54_169
54_172
54_176
54_183
54_190
54_193
54_204
54_210
54_212
54_223
54_3
54_8
54_15
54_101
54_106
54_159
54_165
54_177
54_182
54_189
54_192
54_211
54_222
54_229
T;
&Safety_Systems ' T54 SETTASK
: 53_0
4 JUMP ;
: 53_1
^Bio_VFD_Loop_Counter @@ 0 }VFD_Setup TABLE! 
PTBL_VFD_Data }VFD_Setup }VFD_Register_Data_Types *Modbus_Port {VFD_Serial_Status }VFD_Port_Status &PACModbusMaster03_Read_Holding_Registers CALL.SUB DROP  
1 {VFD_Serial_Status $TABLE@ " OK" $= IF 
^Bio_VFD_Loop_Counter @@ PTBL_Bio_VFD_Data 
TableMoveToPointer PTR_Bio_Current_VFD_Data 
2050 }VFD_Data_Int TABLE@F 3.333330e+002 F/ ^Bio_Index_VFD_Frequency @@ PTR_Bio_Current_VFD_Data TABLE! 
2060 }VFD_Data_Int TABLE@F 1.000000e+002 F/ ^Bio_Index_VFD_Amps @@ PTR_Bio_Current_VFD_Data TABLE! 
2061 }VFD_Data_Int TABLE@F 1.000000e+001 F/ ^Bio_Index_VFD_Volts @@ PTR_Bio_Current_VFD_Data TABLE! 
2065 }VFD_Data_Int TABLE@F ^Bio_Index_VFD_Alarm @@ PTR_Bio_Current_VFD_Data TABLE! 
2095 }VFD_Data_Int TABLE@F 1.000000e+001 F/ ^Bio_Index_VFD_Capacitor_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
2096 }VFD_Data_Int TABLE@F ^Bio_Index_VFD_PC_Board_Capacitor_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
2097 }VFD_Data_Int TABLE@F ^Bio_Index_VFD_Heatsink_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
2111 }VFD_Data_Int TABLE@F 1.800000e+000 F* 32 I>F F+ ^Bio_Index_VFD_Heatsink_Temp @@ PTR_Bio_Current_VFD_Data TABLE! 
ELSE 
^Bio_VFD_Loop_Counter @@ PTBL_Bio_VFD_Data 
TableMoveToPointer PTR_Bio_Current_VFD_Data 
-99 I>F ^Bio_Index_VFD_Frequency @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Amps @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Volts @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Alarm @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Capacitor_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_PC_Board_Capacitor_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Heatsink_Life @@ PTR_Bio_Current_VFD_Data TABLE! 
-99 I>F ^Bio_Index_VFD_Heatsink_Temp @@ PTR_Bio_Current_VFD_Data TABLE! 
THEN 
7 JUMP ;
: 53_15
  ^Bio_VFD_Loop_Counter   1+@!
-2 JUMP ;
: 53_16
  0  
  ^Bio_VFD_Loop_Counter @!   
-2 JUMP ;
: 53_25
  10    DELAY
-2 JUMP ;
: 53_29
  *Bio_Steam_Meter_Serial_1   OPEN
  ^Bio_Steam_Meter_Status1 @! 
2 JUMP ;
: 53_30
  *Bio_Steam_Meter_Readback1 
  16  
  *Bio_Steam_Meter_Serial_1   GETN$
  ^Bio_Steam_Meter_Status1 @! 
0 JUMP ;
: 53_33
  5.0    FDELAY
-8 JUMP ;
: 53_36
  " AC" 
 
  *Bio_Steam_Meter_Serial_1 
  *Bio_Steam_Meter_Readback1   S&R$
  ^Bio_Steam_Meter_Status1 @! 
  *Bio_Steam_Meter_Serial_1   STREAM.CR
  ^Bio_Steam_Meter_Status1 @! 
-3 JUMP ;
: 53_11
TRUE
  ^Bio_VFD_Loop_Counter @@ 
  20    <=
LAND
IF -8 ELSE -6 THEN JUMP ;
T: T53
DUMMY
53_0
53_1
53_15
53_16
53_25
53_29
53_30
53_33
53_36
53_11
T;
&VFD_Serial ' T53 SETTASK
CREATE T.ARRAY
&Alarms ,
&Ash_Out ,
&Boilers ,
&Changelog ,
&Clear_Ash_Auger ,
&Clear_Belt_Auger_Transistion ,
&Clear_Belt_Jam ,
&Clear_Lift_Auger ,
&Condensate ,
&Day_Bin_Fill ,
&Day_Bin_Timed_Fill ,
&Email ,
&Extinguish_Day_Bin ,
&Feed_Section ,
&Gasifier ,
&Incineration ,
&Mode_Fuel_Saver ,
&Mode_Management ,
&Mode_Run ,
&Mode_Shutdown ,
&Mode_Startup ,
&Moisturizer_Pumpout ,
&Ph_Dose ,
&Powerup ,
&Read_Sensors ,
&Safety_Systems ,
&VFD_Serial ,
 0 ,
CREATE V.ARRAY
^Bio_24hour_reset ,
^Bio_Above_Door_Temp ,
^Bio_Air_Moisturizer_Air_Outlet_Temp ,
^Bio_Air_Moisturizer_Level ,
^Bio_Air_Moisturizer_Water_Inlet_Flow ,
^Bio_Air_Moisturizer_Water_Inlet_Temp ,
^Bio_Air_Moisturizer_Water_Temp ,
^Bio_Air_Super_Heater_Outlet_Temp ,
^Bio_Alarm_Count ,
^Bio_Alarm_Triggered ,
^Bio_Ash_Removed ,
^Bio_Ash_Temp ,
^Bio_Bed_Height ,
^Bio_Boiler_3_Condensate_Pump_Cycle_Ratio ,
^Bio_Boiler_3_Inlet_Temp ,
^Bio_Boiler_3_Outlet_Temp ,
^Bio_Boiler_4_Condensate_Pump_Cycle_Ratio ,
^Bio_Boiler_4_Inlet_Temp ,
^Bio_Boiler_4_Outlet_Temp ,
^Bio_Boiler_Steam_Pressure ,
^Bio_Boiler_Steam_Pressure_Setpoint ,
^Bio_Ceiling_Temp ,
^Bio_Chip_Volume_Factor ,
^Bio_Clearing_Belt_Auger_Jam ,
^Bio_Clearing_Belt_Jam ,
^Bio_Condensate_pH ,
^Bio_Current_Chip_Volume ,
^Bio_Database_Connection ,
^Bio_Day_Bin_Fill_Delay ,
^Bio_Day_Bin_Fill_Timer ,
^Bio_Day_Bin_Filling_Cycle_Ratio ,
^Bio_Day_Bin_High_Disabled ,
^Bio_Day_Bin_Running_On_Timer ,
^Bio_Day_Bin_TPO_Period ,
^Bio_Day_Of_Week ,
^Bio_DayBin_Spray_ON ,
^Bio_DayBin_TPO_Percent ,
^Bio_Delay_Ash_Removal_Time ,
^Bio_Delay_Day_Bin_Clear_Conveyors_Msec ,
^Bio_Delay_Ph_Check ,
^Bio_Delay_Ph_Dose_Time ,
^Bio_Delay_Silo_Auger_Jam_Clear ,
^Bio_Error_No_Database_Connection ,
^Bio_Extinguish_Day_Bin_Fire ,
^Bio_Feed_Auger_1_Temp ,
^Bio_Feed_Auger_2_Temp ,
^Bio_Feed_Auger_3_Temp ,
^Bio_Feed_Auger_4_Temp ,
^Bio_Feed_Auger_5_Temp ,
^Bio_Feed_Auto ,
^Bio_Feed_Rate_Trim ,
^Bio_Feed_Screw_Period ,
^Bio_Feeder_Screw_Speed ,
^Bio_Fuel_Fed_12_Hour_Total ,
^Bio_Fuel_Fed_24_Hour_Total ,
^Bio_Fuel_Fed_Short ,
^Bio_Gasifer_Alarm_Temp ,
^Bio_Gasifier_Pressure ,
^Bio_Gasifier_Temp_1 ,
^Bio_Gasifier_Temp_2 ,
^Bio_Gasifier_Temp_3 ,
^Bio_Gasifier_Temp_4 ,
^Bio_Gasifier_Temp_5 ,
^Bio_Gasifier_Temp_6 ,
^Bio_Gasifier_Temp_7 ,
^Bio_Gasifier_Top_Temp_1 ,
^Bio_Gasifier_Warning_Temp ,
^Bio_Incineration_Temp_Minimum ,
^Bio_Index_Alarm_Type_Analog_High ,
^Bio_Index_Alarm_Type_Analog_Low ,
^Bio_Index_Alarm_Type_Custom ,
^Bio_Index_Alarm_Type_Digital ,
^Bio_Index_Alarm_Type_Torque ,
^Bio_Index_Alarms_Ash_Temp_High ,
^Bio_Index_Alarms_Ash_Temp_Low ,
^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High ,
^Bio_Index_Alarms_Boiler_3_Over_Pressure ,
^Bio_Index_Alarms_Boiler_3_Water_Critical ,
^Bio_Index_Alarms_Boiler_3_Water_High ,
^Bio_Index_Alarms_Boiler_3_Water_Low ,
^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High ,
^Bio_Index_Alarms_Boiler_4_Over_Pressure ,
^Bio_Index_Alarms_Boiler_4_Water_Critical ,
^Bio_Index_Alarms_Boiler_4_Water_High ,
^Bio_Index_Alarms_Boiler_4_Water_Low ,
^Bio_Index_Alarms_Condensate_pH_Low ,
^Bio_Index_Alarms_Conveyor_Jam1 ,
^Bio_Index_Alarms_Conveyor_Jam2 ,
^Bio_Index_Alarms_Day_Bin_High_PE ,
^Bio_Index_Alarms_Day_Bin_Low ,
^Bio_Index_Alarms_Feed_Auger_1_Temp_High ,
^Bio_Index_Alarms_Feed_Auger_2_Temp_High ,
^Bio_Index_Alarms_Feed_Auger_3_Temp_High ,
^Bio_Index_Alarms_Feed_Auger_4_Temp_High ,
^Bio_Index_Alarms_Feed_Auger_5_Temp_High ,
^Bio_Index_Alarms_Feed_Screw_1_Torque_High ,
^Bio_Index_Alarms_Feed_Screw_2_Torque_High ,
^Bio_Index_Alarms_Feed_Screw_3_Torque_High ,
^Bio_Index_Alarms_Feed_Screw_4_Torque_High ,
^Bio_Index_Alarms_Feed_Screw_5_Torque_High ,
^Bio_Index_Alarms_Feed_System_Estop ,
^Bio_Index_Alarms_Gasifier_DP_High ,
^Bio_Index_Alarms_Gasifier_Temp_High ,
^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low ,
^Bio_Index_Alarms_Lift_Auger_Torque_High ,
^Bio_Index_Alarms_Moisturizer_Water_DP_High ,
^Bio_Index_Alarms_Moisturizer_Water_DP_Low ,
^Bio_Index_Alarms_Moisturizer_Water_High ,
^Bio_Index_Alarms_Moisturizer_Water_Low ,
^Bio_Index_Alarms_Primary_Air_Temp_Low ,
^Bio_Index_Alarms_Quench_3_Temp_High ,
^Bio_Index_Alarms_Quench_3_Water_High ,
^Bio_Index_Alarms_Quench_3_Water_Low ,
^Bio_Index_Alarms_Quench_4_Temp_High ,
^Bio_Index_Alarms_Quench_4_Water_High ,
^Bio_Index_Alarms_Quench_4_Water_Low ,
^Bio_Index_Alarms_Scrubber_Temp_High ,
^Bio_Index_Alarms_Scrubber_Water_High ,
^Bio_Index_Alarms_Scrubber_Water_Low ,
^Bio_Index_Alarms_Silo_1_Auger_Torque_High ,
^Bio_Index_Alarms_Steam_Pressure_High ,
^Bio_Index_Alarms_Steam_Pressure_Low ,
^Bio_Index_Alarms_System_Estop ,
^Bio_Index_Alarms_Topside_Blowdown_Ratio_High ,
^Bio_Index_Alarms_Water_Storage_Tank_Low ,
^Bio_Index_Feed ,
^Bio_Index_Operation_Modes_Fuel_Saver ,
^Bio_Index_Operation_Modes_Off ,
^Bio_Index_Operation_Modes_Run ,
^Bio_Index_Operation_Modes_Shutdown ,
^Bio_Index_Operation_Modes_Startup ,
^Bio_Index_VFD_Alarm ,
^Bio_Index_VFD_Amps ,
^Bio_Index_VFD_Capacitor_Life ,
^Bio_Index_VFD_Frequency ,
^Bio_Index_VFD_Heatsink_Life ,
^Bio_Index_VFD_Heatsink_Temp ,
^Bio_Index_VFD_PC_Board_Capacitor_Life ,
^Bio_Index_VFD_Volts ,
^Bio_Jam_Clear_Attemps ,
^Bio_Momentary_Ash_Out ,
^Bio_Momentary_B3_Low ,
^Bio_Momentary_B4_Low ,
^Bio_Momentary_Day_Bin_Low ,
^Bio_Momentary_Moisturizer_High ,
^Bio_Momentary_Quench_High ,
^Bio_Momentary_Scrubber_High ,
^Bio_Most_Severe_Alarm ,
^Bio_Operation_Mode ,
^Bio_Operation_Mode_Last ,
^Bio_Operation_Mode_Modulate ,
^Bio_Operation_Mode_Next ,
^Bio_Operation_Mode_Override ,
^Bio_Primary_Air_Flow ,
^Bio_primary_max_speed ,
^Bio_Primary_Tripped_Counter ,
^Bio_Quench_3_Outlet_Temp ,
^Bio_Quench_4_Outlet_Temp ,
^Bio_Quench_Nozzle_Water_Pressure ,
^Bio_Quench_Outlet_Pump_Cycle_Ratio ,
^Bio_Quench_Output_1_active ,
^Bio_running_on_biomass ,
^Bio_Safety_Water_Pressure ,
^Bio_Scratchpad_Status ,
^Bio_Scrubber_DP_Level ,
^Bio_Scrubber_Outlet_Pump_Cycle_Ratio ,
^Bio_Scrubber_Outlet_Temp ,
^Bio_Scrubber_Water_Level ,
^Bio_Secondary_Air_Spray_1_Open_Ratio ,
^Bio_Secondary_Air_Spray_2_Open_Ratio ,
^Bio_Secondary_Air_Spray_3_Open_Ratio ,
^Bio_Secondary_Air_Temp ,
^Bio_Send_Email ,
^Bio_Send_Email_Result ,
^Bio_Setpoint_Above_Door_Spray ,
^Bio_Setpoint_Air_Moisturizer_Inlet_Pump_Speed ,
^Bio_Setpoint_Air_Moisturizer_Outlet_Pump_Speed ,
^Bio_Setpoint_Day_Bin_Not_High_Time ,
^Bio_Setpoint_Moisturizer_Out_Time ,
^Bio_Setpoint_Moisturizer_Pumpout_Time ,
^Bio_Setpoint_Ph_Level ,
^Bio_Setpoint_Steam_Pressure ,
^Bio_Setpoint_Water_Spray ,
^Bio_Silo_Days_Until_Empty ,
^Bio_Silo_Days_Until_Empty_Extra_Hours ,
^Bio_Silo_Hours_Until_Empty ,
^Bio_Single_Condensate ,
^Bio_Stack_Exhaust_O2_Level ,
^Bio_State_Spray_Lance_PID ,
^Bio_Steam_Flow_Total ,
^Bio_Steam_Meter_Status1 ,
^Bio_System_Ash_Out ,
^Bio_System_Both_Coaters_Running ,
^Bio_System_Clear_Ash_Augers ,
^Bio_System_Clear_Lift_Auger ,
^Bio_System_Enabled_Ash_Removal ,
^Bio_System_Enabled_Day_Bin ,
^Bio_System_Enabled_Feed_Screws ,
^Bio_System_Enabled_Moisturizer ,
^Bio_System_Enabled_Scrubber ,
^Bio_Tertiary_Air_Spray_Open_Ratio ,
^Bio_Tertiary_Air_Temp ,
^Bio_Time_Until_Day_Bin_Empty ,
^Bio_Topside_Blowdown_Cycle_Ratio ,
^Bio_Total_Chip_Add_Amount ,
^Bio_Total_Chip_Add_Btn ,
^Bio_Total_Cu_Ft_Per_12_Hours ,
^Bio_Total_Cu_Ft_Per_Hour ,
^Bio_Total_Cu_Yd_Per_12_Hours ,
^Bio_Total_Cu_Yd_Per_Hour ,
^Bio_Total_Past_12_Hours ,
^Bio_Total_Past_Hour ,
^Bio_Trim_Primary_Air ,
^Bio_Trim_Tertiary ,
^Bio_VFD_Loop_Counter ,
^Bio_Volume_Storage_Counter ,
^Current_Day_Of_Year ,
^Current_Hour_Of_Day ,
^email_alarm_1_sent_flag ,
^email_alarm_1_test_trigger ,
^index ,
^loop_counter ,
^nDelay ,
^nResult ,
^status_trashcan ,
^Tertiary_Low_Clamp_Running ,
^Tertiary_Low_Clamp_Running_2_Coaters ,
*Bio_current_pump ,
*Bio_Email_Server_Hostname ,
*Bio_Email_Server_Port ,
*Bio_Email_Server_Protocol ,
*Bio_Operation_Mode_String ,
*Bio_Steam_Meter_Readback1 ,
*Bio_System_Last_Db_Timestamp ,
*Bio_Version ,
*CRLF ,
*strErrorMessage ,
*strTempString ,
*_HSV_SEMA ,
*_HSV_TEMP ,
*Bio_Steam_Meter_Serial_1 ,
*Modbus_Port ,
 0 ,
CREATE TI.ARRAY
^Bio_Check_Database_Timer ,
^Bio_Pusher_Timer ,
^Bio_Timer_15_Minute ,
^Bio_Timer_20minute_day_bin ,
^Bio_Timer_Ash_Removal ,
^Bio_Timer_Day_Bin_Emptying ,
^Bio_Timer_Day_Bin_Filling ,
^Bio_Timer_Day_Bin_High ,
^Bio_Timer_Day_Bin_High_Disabled ,
^Bio_Timer_Day_Bin_Low ,
^Bio_Timer_DayBin_Not_High ,
^Bio_Timer_Feed_Per_15 ,
^Bio_Timer_Feed_Screw_Period ,
^Bio_Timer_Moisturizer_Adjust_Time ,
^Bio_Timer_Moisturizer_High ,
^Bio_Timer_Moisturizer_Out_OFF ,
^Bio_Timer_Moisturizer_Out_ON ,
^Bio_Timer_Quench_Outlet_OFF ,
^Bio_Timer_Quench_Outlet_ON ,
^Bio_Timer_Scrubber_Outlet_OFF ,
^Bio_Timer_Scrubber_Outlet_ON ,
^Bio_Timer_Since_Volume_Calculation ,
^Bio_Timer_Since_Volume_Storage ,
 0 ,
CREATE PTR.ARRAY
Ptr' PTR_Bio_Current_Alarm_Point_Analog ,
Ptr' PTR_Bio_Current_Alarm_Point_Digital ,
Ptr' PTR_Bio_Current_Alarm_Point_VFD ,
Ptr' PTR_Bio_Current_Quench_Outlet_Pump ,
Ptr' PTR_Bio_Current_VFD_Data ,
 0 ,
CREATE TA.ARRAY 
}Bio_Alarm_Delay ,
}Bio_Alarm_Throw_Time ,
}Bio_Alarm_Trigger_Points ,
}Bio_Alarm_Types ,
}Bio_Alarm_VFDs ,
}Bio_Alarms ,
}Bio_Boiler_3_Condensate_Pump_Off_Times ,
}Bio_Boiler_3_Condensate_Pump_On_Times ,
}Bio_Boiler_4_Condensate_Pump_Off_Times ,
}Bio_Boiler_4_Condensate_Pump_On_Times ,
}Bio_Day_Bin_Filling_Off_Times ,
}Bio_Day_Bin_Filling_On_Times ,
}Bio_Feed_15_Minute_Table ,
}Bio_Feed_Day_Of_Year_Total ,
}Bio_Fuel_Fed ,
}Bio_Info ,
}Bio_Info_Delay ,
}Bio_Quench_Outlet_Pump_Off_Times ,
}Bio_Quench_Outlet_Pump_On_Times ,
}Bio_Scrubber_Outlet_Pump_Off_Times ,
}Bio_Scrubber_Outlet_Pump_On_Times ,
}Bio_Setpoints_Feed_Rate ,
}Bio_Setpoints_Gasifier_Pressure ,
}Bio_Setpoints_Moisturizer_Inlet ,
}Bio_Setpoints_Primary_Air ,
}Bio_Setpoints_Quench_Inlet ,
}Bio_Setpoints_Scrubber_Inlet ,
}Bio_Setpoints_Secondary_Air ,
}Bio_Setpoints_Tertiary_Air ,
}Bio_Topside_Blowdown_Off_Times ,
}Bio_Topside_Blowdown_On_Times ,
}Bio_VFD_Data_Air_Moisturizer_Inlet_Pump ,
}Bio_VFD_Data_Air_Moisturizer_Outlet_Pump ,
}Bio_VFD_Data_Ash_Removal_Screws ,
}Bio_VFD_Data_Belt_Conveyor ,
}Bio_VFD_Data_Fuel_Feeder_Screw_1 ,
}Bio_VFD_Data_Fuel_Feeder_Screw_2 ,
}Bio_VFD_Data_Fuel_Feeder_Screw_3 ,
}Bio_VFD_Data_Fuel_Feeder_Screw_4 ,
}Bio_VFD_Data_Fuel_Feeder_Screw_5 ,
}Bio_VFD_Data_Lift_Auger ,
}Bio_VFD_Data_Primary_Air_Fan ,
}Bio_VFD_Data_Quench_Inlet_Pump ,
}Bio_VFD_Data_Quench_Outlet_Pump_1 ,
}Bio_VFD_Data_Quench_Outlet_Pump_2 ,
}Bio_VFD_Data_Scrubber_Inlet_Pump ,
}Bio_VFD_Data_Scrubber_Outlet_Pump ,
}Bio_VFD_Data_Secondary_Air_Fan ,
}Bio_VFD_Data_Silo_1_Augers ,
}Bio_VFD_Data_Stack_Fan ,
}Bio_VFD_Data_Tertiary_Air_Fan ,
}Bio_Warning_Delay ,
}Bio_Warnings ,
}VFD_Data_Float ,
}VFD_Data_Int ,
}VFD_Port_Status ,
}VFD_Register_Data_Types ,
}VFD_Setup ,
{arrstrAttach ,
{arrstrBody ,
{arrstrRecipients ,
{arrstrServer ,
{Bio_Alarm_Descriptions ,
{Bio_Email_Body ,
{Bio_Email_Recipients ,
{Bio_Email_Server_Info ,
{Bio_Operation_Mode_Strings ,
{database ,
{VFD_Serial_Status ,
 0 ,
CREATE PTRTABLE.ARRAY 
PTBL_Bio_Alarm_Points ,
PTBL_Bio_VFD_Data ,
PTBL_VFD_Data ,
 0 ,
CREATE B.ARRAY
%Bioler_Room ,
%Biomass_1 ,
%Biomass_2 ,
 0 ,
CREATE P.ARRAY
~B1_diverter ,
~B1_enabled ,
~B2_diverter ,
~B2_enabled ,
~BA_Coater_1_clearance ,
~BA_Coater_1_ESTOP ,
~BA_Coater_1_request_for_incineration ,
~BA_Coater_2_clearance ,
~BA_Coater_2_ESTOP ,
~BA_Coater_2_request_for_incineration ,
~Bio_Air_Blast_Sensor_Clean ,
~Bio_Air_Moisturizer_Inlet_Pump_Motor_On_Off ,
~Bio_Air_Moisturizer_Outlet_Pump ,
~Bio_Air_Moisturizer_Outlet_Pump_Motor_On_Off ,
~Bio_Air_Super_Heater_Boost_Steam_Valve ,
~Bio_Ash_Removal_Screws_Forward ,
~Bio_Ash_Removal_Screws_Reverse ,
~Bio_Audible_Alarm ,
~Bio_Belt_Conveyor_On_Off ,
~Bio_Boiler_3_Condensate_Pump_On_Off ,
~Bio_Boiler_4_Condensate_Pump_On_Off ,
~Bio_Day_Bin_Spray ,
~Bio_Fuel_Feeder_Screw_1_Forward ,
~Bio_Fuel_Feeder_Screw_1_Reverse ,
~Bio_Fuel_Feeder_Screw_2_Forward ,
~Bio_Fuel_Feeder_Screw_2_Reverse ,
~Bio_Fuel_Feeder_Screw_3_Forward ,
~Bio_Fuel_Feeder_Screw_3_Reverse ,
~Bio_Fuel_Feeder_Screw_4_Forward ,
~Bio_Fuel_Feeder_Screw_4_Reverse ,
~Bio_Fuel_Feeder_Screw_5_Forward ,
~Bio_Fuel_Feeder_Screw_5_Reverse ,
~Bio_Green_Light ,
~Bio_Incineration_Diverter ,
~Bio_Incineration_Enabled ,
~Bio_Input_Boiler_3_Over_Pressure ,
~Bio_Input_Boiler_3_Water_Critical ,
~Bio_Input_Boiler_3_Water_High ,
~Bio_Input_Boiler_3_Water_Low ,
~Bio_Input_Boiler_4_Over_Pressure ,
~Bio_Input_Boiler_4_Water_Critical ,
~Bio_Input_Boiler_4_Water_High ,
~Bio_Input_Boiler_4_Water_Low ,
~Bio_Input_Condensate1_VFD_Error ,
~Bio_Input_Condensate2_VFD_Error ,
~Bio_Input_Conveyor_Blockage1 ,
~Bio_Input_Conveyor_Blockage2 ,
~Bio_Input_Day_Bin_High ,
~Bio_Input_Day_Bin_Low ,
~Bio_Input_Daybin_High_PE ,
~Bio_Input_Estop_Feedback ,
~Bio_Input_Feed_Screw_1_VFD_Error ,
~Bio_Input_Feed_Screw_2_VFD_Error ,
~Bio_Input_Feed_Screw_3_VFD_Error ,
~Bio_Input_Feed_Screw_4_VFD_Error ,
~Bio_Input_Feed_Screw_5_VFD_Error ,
~Bio_Input_Feed_System_Estop_Feedback ,
~Bio_Input_Incineration_Diverter_Closed_Switch ,
~Bio_Input_Incineration_Diverter_Open_Switch ,
~Bio_Input_Primary_VFD_Error ,
~Bio_Input_Secondary_Water_Flow_1 ,
~Bio_Input_Secondary_Water_Flow_2 ,
~Bio_Input_Secondary_Water_Flow_3 ,
~Bio_Input_Stack_VFD_Error ,
~Bio_Input_Steam_Flow_1_Pulse ,
~Bio_Input_Steam_Flow_2_Pulse ,
~Bio_Input_Steam_Flow_3_Pulse ,
~Bio_Input_Tertiary_Water_Flow ,
~Bio_Lift_Auger_Forward ,
~Bio_Lift_Auger_Reverse ,
~Bio_Ph_Pump ,
~Bio_Primary_Air_Fan_Motor_On_Off ,
~Bio_Pusher_Extend ,
~Bio_Quench_3_Nozzle_1 ,
~Bio_Quench_3_Nozzle_2 ,
~Bio_Quench_3_Nozzle_3 ,
~Bio_Quench_3_Nozzle_4 ,
~Bio_Quench_3_Safety_Water_Valve ,
~Bio_Quench_4_Nozzle_1 ,
~Bio_Quench_4_Nozzle_2 ,
~Bio_Quench_4_Nozzle_3 ,
~Bio_Quench_4_Nozzle_4 ,
~Bio_Quench_4_Safety_Water_Valve ,
~Bio_Quench_Inlet_Pump_On_Off ,
~Bio_Quench_Outlet_Pump_2_On_Off ,
~Bio_Red_Light ,
~Bio_Relay1 ,
~Bio_Relay1_0501 ,
~Bio_Relay1_0502 ,
~Bio_Relay1_0503 ,
~Bio_Relay1_0504 ,
~Bio_Relay1_0505 ,
~Bio_Relay1_0506 ,
~Bio_Relay1_0507 ,
~Bio_Relay1_0510 ,
~Bio_Relay1_0511 ,
~Bio_Relay1_0512 ,
~Bio_Relay1_0513 ,
~Bio_Relay1_0514 ,
~Bio_Relay1_0515 ,
~Bio_Relay1_0520 ,
~Bio_Relay1_0521 ,
~Bio_Relay1_0522 ,
~Bio_Relay1_0523 ,
~Bio_Relay1_0524 ,
~Bio_Relay1_0525 ,
~Bio_Relay1_0526 ,
~Bio_Relay1_0527 ,
~Bio_Relay1_0528 ,
~Bio_Relay1_0529 ,
~Bio_Relay1_0530 ,
~Bio_Relay1_0531 ,
~Bio_Reset_Condensate1_VFD ,
~Bio_Reset_Condensate2_VFD ,
~Bio_Reset_Primary_VFD ,
~Bio_Reset_Stack_VFD ,
~Bio_Scrubber_Inlet_Pump_On_Off ,
~Bio_Scrubber_Outlet_Pump_On_Off ,
~Bio_Scrubber_Safety_Water_Valve ,
~Bio_Secondary_Air_Fan_Motor_On_Off ,
~Bio_Secondary_Air_Spray_1 ,
~Bio_Secondary_Air_Spray_2 ,
~Bio_Secondary_Air_Spray_3 ,
~Bio_Silo_1_Augers_Forward ,
~Bio_Silo_1_Augers_Reverse ,
~Bio_Stack_Fan_Motor_On_Off ,
~Bio_Tertiary_Air_Fan_On_Off ,
~Bio_Tertiary_Air_Spray ,
~Bio_Topside_Blowdown_1 ,
~Bio_Topside_Blowdown_2 ,
~Bio_Ventilation_Fan_On_Off ,
~Bio_Water_Storage_Tank_Fill_Valve ,
~Bio_Yellow_Light ,
~ai_Biomass_2_0401 ,
~ai_Biomass_2_0402 ,
~ai_Biomass_2_0403 ,
~Bio_Air_Moisturizer_Inlet_Pump_Speed ,
~Bio_Air_Moisturizer_Outlet_Pump_Speed ,
~Bio_Input_Above_Door_Temp ,
~Bio_Input_Air_Moisturizer_Air_Outlet_Temp ,
~Bio_Input_Air_Moisturizer_Inlet_Water_Flow ,
~Bio_Input_Air_Moisturizer_Level ,
~Bio_Input_Air_Moisturizer_Water_Inlet_Temp ,
~Bio_Input_Air_Moisturizer_Water_Temp ,
~Bio_Input_Air_Super_Heater_Outlet_Temp_1 ,
~Bio_Input_Air_Super_Heater_Outlet_Temp_2 ,
~Bio_Input_Ash_Temp_Left ,
~Bio_Input_Ash_Temp_Right ,
~Bio_Input_Bed_Height ,
~Bio_Input_Boiler_3_Inlet_Temp_1 ,
~Bio_Input_Boiler_3_Inlet_Temp_2 ,
~Bio_Input_Boiler_3_Outlet_Temp ,
~Bio_Input_Boiler_4_Exit_Temp ,
~Bio_Input_Boiler_4_Inlet_Temp_1 ,
~Bio_Input_Boiler_4_Inlet_Temp_2 ,
~Bio_Input_Boiler_Steam_Pressure_1 ,
~Bio_Input_Boiler_Steam_Pressure_2 ,
~Bio_Input_Ceiling_Temp ,
~Bio_Input_Feed_Auger_1_Temp ,
~Bio_Input_Feed_Auger_2_Temp ,
~Bio_Input_Feed_Auger_3_Temp ,
~Bio_Input_Feed_Auger_4_Temp ,
~Bio_Input_Feed_Auger_5_Temp ,
~Bio_Input_Gasifier_Pressure ,
~Bio_Input_Gasifier_Temp_1 ,
~Bio_Input_Gasifier_Temp_2 ,
~Bio_Input_Gasifier_Temp_3 ,
~Bio_Input_Gasifier_Temp_4 ,
~Bio_Input_Gasifier_Temp_5 ,
~Bio_Input_Gasifier_Temp_6 ,
~Bio_Input_Gasifier_Temp_7 ,
~Bio_Input_Gasifier_Top_Temp_1 ,
~Bio_Input_Gasifier_Top_Temp_2 ,
~Bio_Input_Hot_Water_Storage_Tank_Level ,
~Bio_Input_PH_Level ,
~Bio_Input_Primary_Air_Flow ,
~Bio_Input_Quench_3_Outlet_Temp_1 ,
~Bio_Input_Quench_3_Outlet_Temp_2 ,
~Bio_Input_Quench_4_Outlet_Temp_1 ,
~Bio_Input_Quench_4_Outlet_Temp_2 ,
~Bio_Input_Quench_Inlet_Water_Pressure ,
~Bio_Input_Quench_Water_Level ,
~Bio_Input_Safety_Water_Pressure ,
~Bio_Input_Scrubber_Inlet_Water_Pressure ,
~Bio_Input_Scrubber_Outlet_Temp_1 ,
~Bio_Input_Scrubber_Outlet_Temp_2 ,
~Bio_Input_Scrubber_Water_Level ,
~Bio_Input_Secondary_Air_Temp_1 ,
~Bio_Input_Secondary_Air_Temp_2 ,
~Bio_Input_Stack_Exhaust_O2_Level_1 ,
~Bio_Input_Stack_Exhaust_O2_Level_2 ,
~Bio_Input_Steam_Temp ,
~Bio_Input_Tertiary_Air_Temp_1 ,
~Bio_Input_Tertiary_Air_Temp_2 ,
~Bio_Primary_Air_Fan_Motor_Speed ,
~Bio_Quench_Inlet_Pump_Speed ,
~Bio_Scrubber_Inlet_Pump_Speed ,
~Bio_Secondary_Air_Fan_Motor_Speed ,
~Bio_Stack_Fan_Motor_Speed ,
~Bio_Steam_Flow_1 ,
~Bio_Steam_Flow_2 ,
~Bio_Steam_Flow_3 ,
~Bio_Temp_PPM ,
~Bio_Tertiary_Air_Fan_Speed ,
~Condensate_Tank_temp ,
 0 ,
CREATE PID.ARRAY
 |Feed_Rate ,
 |Gasifier_Pressure ,
 |PID_Secondary_Center_Lance ,
 |PID_Secondary_Outer_Lances ,
 |PID_Tertiary_Lance ,
 |Primary_Air ,
 |Primary_Air_Low_Clamp ,
 |Quench_Water_Inlet_Flow ,
 |Scrubber_Inlet_PID ,
 |Secondary_Air ,
 |Secondary_Air_Low_Clamp ,
 |Tertiary_Air ,
 |Tertiary_Air_Low_Clamp ,
 0 ,
CREATE E/R.ARRAY
 0 ,
CREATE E/RGROUP.ARRAY
 0 ,
: CONFIG_PORTS
;
: W_INIT_IO
CONFIG_PORTS
|Feed_Rate ENABLE
|Gasifier_Pressure ENABLE
|PID_Secondary_Center_Lance ENABLE
|PID_Secondary_Outer_Lances ENABLE
|PID_Tertiary_Lance ENABLE
|Primary_Air ENABLE
|Primary_Air_Low_Clamp ENABLE
|Quench_Water_Inlet_Flow ENABLE
|Scrubber_Inlet_PID ENABLE
|Secondary_Air ENABLE
|Secondary_Air_Low_Clamp ENABLE
|Tertiary_Air ENABLE
|Tertiary_Air_Low_Clamp ENABLE
" %Bioler_Room  (1/3)" *_HSV_INIT_IO $MOVE 2 ^_HNV_INIT_IO @!
%Bioler_Room ENABLE
" %Biomass_1  (2/3)" *_HSV_INIT_IO $MOVE 1 ^_HNV_INIT_IO @!
%Biomass_1 ENABLE
" %Biomass_2  (3/3)" *_HSV_INIT_IO $MOVE 0 ^_HNV_INIT_IO @!
%Biomass_2 ENABLE
 " Initializing variables" *_HSV_INIT_IO $MOVE
0 ^Bio_24hour_reset @!
0.00000000 ^Bio_Above_Door_Temp @!
0.00000000 ^Bio_Air_Moisturizer_Air_Outlet_Temp @!
0.00000000 ^Bio_Air_Moisturizer_Level @!
0.00000000 ^Bio_Air_Moisturizer_Water_Inlet_Flow @!
0.00000000 ^Bio_Air_Moisturizer_Water_Inlet_Temp @!
0.00000000 ^Bio_Air_Moisturizer_Water_Temp @!
0.00000000 ^Bio_Air_Super_Heater_Outlet_Temp @!
0 ^Bio_Alarm_Count @!
0 ^Bio_Alarm_Triggered @!
0.00000000 ^Bio_Ash_Temp @!
0.00000000 ^Bio_Bed_Height @!
0.00000000 ^Bio_Boiler_3_Condensate_Pump_Cycle_Ratio @!
0.00000000 ^Bio_Boiler_3_Inlet_Temp @!
0.00000000 ^Bio_Boiler_3_Outlet_Temp @!
0.00000000 ^Bio_Boiler_4_Condensate_Pump_Cycle_Ratio @!
0.00000000 ^Bio_Boiler_4_Inlet_Temp @!
0.00000000 ^Bio_Boiler_4_Outlet_Temp @!
0.00000000 ^Bio_Boiler_Steam_Pressure @!
0.00000000 ^Bio_Ceiling_Temp @!
0.0 ^Bio_Check_Database_Timer @!
0.00000000 ^Bio_Chip_Volume_Factor @!
0 ^Bio_Clearing_Belt_Auger_Jam @!
0 ^Bio_Clearing_Belt_Jam @!
0.00000000 ^Bio_Condensate_pH @!
1 ^Bio_Database_Connection @!
190.00000 ^Bio_Day_Bin_Fill_Delay @!
210.00000 ^Bio_Day_Bin_Fill_Timer @!
0.00000000 ^Bio_Day_Bin_Filling_Cycle_Ratio @!
0 ^Bio_Day_Bin_High_Disabled @!
0 ^Bio_Day_Bin_Running_On_Timer @!
120 ^Bio_Day_Bin_TPO_Period @!
0 ^Bio_Day_Of_Week @!
0 ^Bio_DayBin_Spray_ON @!
0.00000000 ^Bio_Delay_Ash_Removal_Time @!
0 ^Bio_Delay_Day_Bin_Clear_Conveyors_Msec @!
0.00000000 ^Bio_Delay_Ph_Check @!
0.00000000 ^Bio_Delay_Ph_Dose_Time @!
0 ^Bio_Delay_Silo_Auger_Jam_Clear @!
0 ^Bio_Error_No_Database_Connection @!
0 ^Bio_Extinguish_Day_Bin_Fire @!
0.00000000 ^Bio_Feed_Auger_1_Temp @!
0.00000000 ^Bio_Feed_Auger_2_Temp @!
0.00000000 ^Bio_Feed_Auger_3_Temp @!
0.00000000 ^Bio_Feed_Auger_4_Temp @!
0.00000000 ^Bio_Feed_Auger_5_Temp @!
0 ^Bio_Feed_Auto @!
0.00000000 ^Bio_Feed_Rate_Trim @!
0.00000000 ^Bio_Feed_Screw_Period @!
0.00000000 ^Bio_Fuel_Fed_12_Hour_Total @!
0.00000000 ^Bio_Fuel_Fed_24_Hour_Total @!
0.00000000 ^Bio_Fuel_Fed_Short @!
2200.0000 ^Bio_Gasifer_Alarm_Temp @!
0.00000000 ^Bio_Gasifier_Pressure @!
0.00000000 ^Bio_Gasifier_Temp_1 @!
0.00000000 ^Bio_Gasifier_Temp_2 @!
0.00000000 ^Bio_Gasifier_Temp_3 @!
0.00000000 ^Bio_Gasifier_Temp_4 @!
0.00000000 ^Bio_Gasifier_Temp_5 @!
0.00000000 ^Bio_Gasifier_Temp_6 @!
0.00000000 ^Bio_Gasifier_Temp_7 @!
0.00000000 ^Bio_Gasifier_Top_Temp_1 @!
2100.0000 ^Bio_Gasifier_Warning_Temp @!
0.00000000 ^Bio_Incineration_Temp_Minimum @!
0 ^Bio_Index_Alarm_Type_Analog_High @!
0 ^Bio_Index_Alarm_Type_Analog_Low @!
0 ^Bio_Index_Alarm_Type_Custom @!
0 ^Bio_Index_Alarm_Type_Digital @!
0 ^Bio_Index_Alarm_Type_Torque @!
0 ^Bio_Index_Alarms_Ash_Temp_High @!
0 ^Bio_Index_Alarms_Ash_Temp_Low @!
0 ^Bio_Index_Alarms_Boiler_3_Inlet_Temp_High @!
0 ^Bio_Index_Alarms_Boiler_3_Over_Pressure @!
0 ^Bio_Index_Alarms_Boiler_3_Water_Critical @!
0 ^Bio_Index_Alarms_Boiler_3_Water_High @!
0 ^Bio_Index_Alarms_Boiler_3_Water_Low @!
0 ^Bio_Index_Alarms_Boiler_4_Inlet_Temp_High @!
0 ^Bio_Index_Alarms_Boiler_4_Over_Pressure @!
0 ^Bio_Index_Alarms_Boiler_4_Water_Critical @!
0 ^Bio_Index_Alarms_Boiler_4_Water_High @!
0 ^Bio_Index_Alarms_Boiler_4_Water_Low @!
0 ^Bio_Index_Alarms_Condensate_pH_Low @!
0 ^Bio_Index_Alarms_Conveyor_Jam1 @!
0 ^Bio_Index_Alarms_Conveyor_Jam2 @!
0 ^Bio_Index_Alarms_Day_Bin_High_PE @!
0 ^Bio_Index_Alarms_Day_Bin_Low @!
0 ^Bio_Index_Alarms_Feed_Auger_1_Temp_High @!
0 ^Bio_Index_Alarms_Feed_Auger_2_Temp_High @!
0 ^Bio_Index_Alarms_Feed_Auger_3_Temp_High @!
0 ^Bio_Index_Alarms_Feed_Auger_4_Temp_High @!
0 ^Bio_Index_Alarms_Feed_Auger_5_Temp_High @!
0 ^Bio_Index_Alarms_Feed_Screw_1_Torque_High @!
0 ^Bio_Index_Alarms_Feed_Screw_2_Torque_High @!
0 ^Bio_Index_Alarms_Feed_Screw_3_Torque_High @!
0 ^Bio_Index_Alarms_Feed_Screw_4_Torque_High @!
0 ^Bio_Index_Alarms_Feed_Screw_5_Torque_High @!
0 ^Bio_Index_Alarms_Feed_System_Estop @!
0 ^Bio_Index_Alarms_Gasifier_DP_High @!
0 ^Bio_Index_Alarms_Gasifier_Temp_High @!
0 ^Bio_Index_Alarms_Index_Safety_Water_Pressure_Low @!
0 ^Bio_Index_Alarms_Lift_Auger_Torque_High @!
0 ^Bio_Index_Alarms_Moisturizer_Water_DP_High @!
0 ^Bio_Index_Alarms_Moisturizer_Water_DP_Low @!
0 ^Bio_Index_Alarms_Moisturizer_Water_High @!
0 ^Bio_Index_Alarms_Moisturizer_Water_Low @!
0 ^Bio_Index_Alarms_Primary_Air_Temp_Low @!
0 ^Bio_Index_Alarms_Quench_3_Temp_High @!
0 ^Bio_Index_Alarms_Quench_3_Water_High @!
0 ^Bio_Index_Alarms_Quench_3_Water_Low @!
0 ^Bio_Index_Alarms_Quench_4_Temp_High @!
0 ^Bio_Index_Alarms_Quench_4_Water_High @!
0 ^Bio_Index_Alarms_Quench_4_Water_Low @!
0 ^Bio_Index_Alarms_Scrubber_Temp_High @!
0 ^Bio_Index_Alarms_Scrubber_Water_High @!
0 ^Bio_Index_Alarms_Scrubber_Water_Low @!
0 ^Bio_Index_Alarms_Silo_1_Auger_Torque_High @!
0 ^Bio_Index_Alarms_Steam_Pressure_High @!
0 ^Bio_Index_Alarms_Steam_Pressure_Low @!
0 ^Bio_Index_Alarms_System_Estop @!
0 ^Bio_Index_Alarms_Topside_Blowdown_Ratio_High @!
0 ^Bio_Index_Alarms_Water_Storage_Tank_Low @!
0 ^Bio_Index_Feed @!
0 ^Bio_Index_Operation_Modes_Fuel_Saver @!
0 ^Bio_Index_Operation_Modes_Off @!
0 ^Bio_Index_Operation_Modes_Run @!
0 ^Bio_Index_Operation_Modes_Shutdown @!
0 ^Bio_Index_Operation_Modes_Startup @!
0 ^Bio_Index_VFD_Alarm @!
0 ^Bio_Index_VFD_Amps @!
0 ^Bio_Index_VFD_Capacitor_Life @!
0 ^Bio_Index_VFD_Frequency @!
0 ^Bio_Index_VFD_Heatsink_Life @!
0 ^Bio_Index_VFD_Heatsink_Temp @!
0 ^Bio_Index_VFD_PC_Board_Capacitor_Life @!
0 ^Bio_Index_VFD_Volts @!
0 ^Bio_Jam_Clear_Attemps @!
0 ^Bio_Momentary_Ash_Out @!
0 ^Bio_Momentary_B3_Low @!
0 ^Bio_Momentary_B4_Low @!
0 ^Bio_Momentary_Day_Bin_Low @!
0 ^Bio_Momentary_Moisturizer_High @!
0 ^Bio_Momentary_Quench_High @!
0 ^Bio_Momentary_Scrubber_High @!
0 ^Bio_Most_Severe_Alarm @!
0 ^Bio_Operation_Mode_Last @!
0 ^Bio_Operation_Mode_Modulate @!
0 ^Bio_Operation_Mode_Next @!
0 ^Bio_Operation_Mode_Override @!
0.00000000 ^Bio_Primary_Air_Flow @!
100 ^Bio_primary_max_speed @!
0.0 ^Bio_Pusher_Timer @!
0.00000000 ^Bio_Quench_3_Outlet_Temp @!
0.00000000 ^Bio_Quench_4_Outlet_Temp @!
0.00000000 ^Bio_Quench_Nozzle_Water_Pressure @!
0.00000000 ^Bio_Quench_Outlet_Pump_Cycle_Ratio @!
1 ^Bio_Quench_Output_1_active @!
0.00000000 ^Bio_Safety_Water_Pressure @!
0 ^Bio_Scratchpad_Status @!
0.00000000 ^Bio_Scrubber_DP_Level @!
0.00000000 ^Bio_Scrubber_Outlet_Pump_Cycle_Ratio @!
0.00000000 ^Bio_Scrubber_Outlet_Temp @!
0.00000000 ^Bio_Scrubber_Water_Level @!
0.00000000 ^Bio_Secondary_Air_Spray_1_Open_Ratio @!
0.00000000 ^Bio_Secondary_Air_Spray_2_Open_Ratio @!
0.00000000 ^Bio_Secondary_Air_Spray_3_Open_Ratio @!
0.00000000 ^Bio_Secondary_Air_Temp @!
0 ^Bio_Send_Email @!
0 ^Bio_Send_Email_Result @!
0 ^Bio_Setpoint_Above_Door_Spray @!
0.00000000 ^Bio_Setpoint_Air_Moisturizer_Inlet_Pump_Speed @!
0.00000000 ^Bio_Setpoint_Air_Moisturizer_Outlet_Pump_Speed @!
2400.0000 ^Bio_Setpoint_Day_Bin_Not_High_Time @!
3600 ^Bio_Setpoint_Moisturizer_Out_Time @!
60 ^Bio_Setpoint_Moisturizer_Pumpout_Time @!
0.00000000 ^Bio_Setpoint_Ph_Level @!
0.00000000 ^Bio_Setpoint_Steam_Pressure @!
0.00000000 ^Bio_Setpoint_Water_Spray @!
0 ^Bio_Single_Condensate @!
0.00000000 ^Bio_Stack_Exhaust_O2_Level @!
0 ^Bio_State_Spray_Lance_PID @!
0.00000000 ^Bio_Steam_Flow_Total @!
0 ^Bio_Steam_Meter_Status1 @!
0 ^Bio_System_Ash_Out @!
0 ^Bio_System_Both_Coaters_Running @!
0 ^Bio_System_Clear_Ash_Augers @!
0 ^Bio_System_Clear_Lift_Auger @!
1 ^Bio_System_Enabled_Feed_Screws @!
0.00000000 ^Bio_Tertiary_Air_Spray_Open_Ratio @!
0.00000000 ^Bio_Tertiary_Air_Temp @!
300.00000 ^Bio_Time_Until_Day_Bin_Empty @!
0.0 ^Bio_Timer_15_Minute @!
0.0 ^Bio_Timer_20minute_day_bin @!
0.0 ^Bio_Timer_Ash_Removal @!
0.0 ^Bio_Timer_Day_Bin_Emptying @!
0.0 ^Bio_Timer_Day_Bin_Filling SetTimer
0.0 ^Bio_Timer_Day_Bin_High SetTimer
0.0 ^Bio_Timer_Day_Bin_High_Disabled SetTimer
0.0 ^Bio_Timer_Day_Bin_Low SetTimer
0.0 ^Bio_Timer_DayBin_Not_High SetTimer
0.0 ^Bio_Timer_Feed_Per_15 SetTimer
0.0 ^Bio_Timer_Feed_Screw_Period SetTimer
0.0 ^Bio_Timer_Moisturizer_Adjust_Time @!
0.0 ^Bio_Timer_Moisturizer_High SetTimer
0.0 ^Bio_Timer_Moisturizer_Out_OFF SetTimer
0.0 ^Bio_Timer_Moisturizer_Out_ON SetTimer
0.0 ^Bio_Timer_Quench_Outlet_OFF SetTimer
0.0 ^Bio_Timer_Quench_Outlet_ON SetTimer
0.0 ^Bio_Timer_Scrubber_Outlet_OFF SetTimer
0.0 ^Bio_Timer_Scrubber_Outlet_ON SetTimer
0.0 ^Bio_Timer_Since_Volume_Calculation SetTimer
0.0 ^Bio_Timer_Since_Volume_Storage SetTimer
0.00000000 ^Bio_Topside_Blowdown_Cycle_Ratio @!
80 ^Bio_Total_Chip_Add_Amount @!
0 ^Bio_Total_Chip_Add_Btn @!
0.00000000 ^Bio_Total_Cu_Ft_Per_12_Hours @!
0.00000000 ^Bio_Total_Cu_Ft_Per_Hour @!
0.00000000 ^Bio_Total_Past_12_Hours @!
0.00000000 ^Bio_Total_Past_Hour @!
0.00000000 ^Bio_Trim_Primary_Air @!
0.00000000 ^Bio_Trim_Tertiary @!
0 ^Bio_VFD_Loop_Counter @!
0 ^Bio_Volume_Storage_Counter @!
0 ^Current_Day_Of_Year @!
0 ^Current_Hour_Of_Day @!
0 ^email_alarm_1_sent_flag @!
0 ^email_alarm_1_test_trigger @!
0 ^index @!
0 ^loop_counter @!
5 ^nDelay @!
0 ^nResult @!
0 ^status_trashcan @!
11 ^Tertiary_Low_Clamp_Running @!
16 ^Tertiary_Low_Clamp_Running_2_Coaters @!
0 0 -1 }Bio_Alarm_Delay InitTable
0 0 -1 }Bio_Alarm_Throw_Time InitTable
0.00000000 0 -1 }Bio_Alarm_Trigger_Points InitTable
0 0 -1 }Bio_Alarm_Types InitTable
0 0 -1 }Bio_Alarm_VFDs InitTable
0 0 -1 }Bio_Alarms InitTable
0.00000000 0 -1 }Bio_Fuel_Fed InitTable
0 0 -1 }Bio_Info InitTable
0 0 -1 }Bio_Info_Delay InitTable
0.00000000 0 -1 }Bio_Setpoints_Feed_Rate InitTable
0.00000000 0 -1 }Bio_Setpoints_Gasifier_Pressure InitTable
0.00000000 0 -1 }Bio_Setpoints_Moisturizer_Inlet InitTable
0.00000000 0 -1 }Bio_Setpoints_Primary_Air InitTable
0.00000000 0 -1 }Bio_Setpoints_Quench_Inlet InitTable
0.00000000 0 -1 }Bio_Setpoints_Scrubber_Inlet InitTable
0.00000000 0 -1 }Bio_Setpoints_Secondary_Air InitTable
0.00000000 0 -1 }Bio_Setpoints_Tertiary_Air InitTable
0.00000000 0 -1 }Bio_VFD_Data_Air_Moisturizer_Inlet_Pump InitTable
0.00000000 0 -1 }Bio_VFD_Data_Air_Moisturizer_Outlet_Pump InitTable
0.00000000 0 -1 }Bio_VFD_Data_Ash_Removal_Screws InitTable
0.00000000 0 -1 }Bio_VFD_Data_Belt_Conveyor InitTable
0.00000000 0 -1 }Bio_VFD_Data_Fuel_Feeder_Screw_1 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Fuel_Feeder_Screw_2 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Fuel_Feeder_Screw_3 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Fuel_Feeder_Screw_4 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Fuel_Feeder_Screw_5 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Lift_Auger InitTable
0.00000000 0 -1 }Bio_VFD_Data_Primary_Air_Fan InitTable
0.00000000 0 -1 }Bio_VFD_Data_Quench_Inlet_Pump InitTable
0.00000000 0 -1 }Bio_VFD_Data_Quench_Outlet_Pump_1 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Quench_Outlet_Pump_2 InitTable
0.00000000 0 -1 }Bio_VFD_Data_Scrubber_Inlet_Pump InitTable
0.00000000 0 -1 }Bio_VFD_Data_Scrubber_Outlet_Pump InitTable
0.00000000 0 -1 }Bio_VFD_Data_Secondary_Air_Fan InitTable
0.00000000 0 -1 }Bio_VFD_Data_Silo_1_Augers InitTable
0.00000000 0 -1 }Bio_VFD_Data_Stack_Fan InitTable
0.00000000 0 -1 }Bio_VFD_Data_Tertiary_Air_Fan InitTable
0 0 -1 }Bio_Warning_Delay InitTable
0 0 -1 }Bio_Warnings InitTable
0.00000000 0 -1 }VFD_Data_Float InitTable
0 0 -1 }VFD_Data_Int InitTable
0 0 -1 }VFD_Port_Status InitTable
0 0 -1 }VFD_Register_Data_Types InitTable
0 0 -1 }VFD_Setup InitTable
" "
 *Bio_current_pump $MOVE
" "
 *Bio_Email_Server_Hostname $MOVE
" "
 *Bio_Email_Server_Port $MOVE
" "
 *Bio_Email_Server_Protocol $MOVE
" "
 *Bio_Operation_Mode_String $MOVE
" "
 *Bio_Steam_Meter_Readback1 $MOVE
" "
 *Bio_System_Last_Db_Timestamp $MOVE
" "
 *Bio_Version $MOVE
" "
 *CRLF $MOVE
" "
 *strErrorMessage $MOVE
" "
 *strTempString $MOVE
" tcp: 192.168.1.103: 22514"
 *Bio_Steam_Meter_Serial_1 $MOVE
" "
 *Modbus_Port $MOVE
 " "
0 -1 {arrstrAttach Init$Table
 " "
0 -1 {arrstrBody Init$Table
 " "
0 -1 {arrstrRecipients Init$Table
 " "
0 -1 {arrstrServer Init$Table
 " "
0 -1 {Bio_Alarm_Descriptions Init$Table
 " "
0 -1 {Bio_Email_Body Init$Table
 " "
0 -1 {Bio_Email_Recipients Init$Table
 " "
0 -1 {Bio_Email_Server_Info Init$Table
 " "
0 -1 {Bio_Operation_Mode_Strings Init$Table
 " "
0 -1 {database Init$Table
 " "
0 -1 {VFD_Serial_Status Init$Table
0 MoveToPointer PTR_Bio_Current_Alarm_Point_Analog
0 MoveToPointer PTR_Bio_Current_Alarm_Point_Digital
0 MoveToPointer PTR_Bio_Current_Alarm_Point_VFD
0 MoveToPointer PTR_Bio_Current_Quench_Outlet_Pump
0 MoveToPointer PTR_Bio_Current_VFD_Data
63 FOR 0 I PTBL_Bio_Alarm_Points TABLE! NEXT
29 FOR 0 I PTBL_Bio_VFD_Data TABLE! NEXT
4 FOR 0 I PTBL_VFD_Data TABLE! NEXT
 " " *_HSV_INIT_IO $MOVE
;
T: T_INIT_IO
W_INIT_IO
&Powerup START.T DROP
T;
&_INIT_IO ' T_INIT_IO  SETTASK
   : _RUN
   CLEARERRORS
   &_INIT_IO START.T DROP
   ;
: DATESTAMP ." 07/10/15 " ;
: TIMESTAMP ." 14:59:12 " ;
: CRCSTAMP  ." 4374ED36426D88ECD6E09F1E5F370EEA " ;
MAKECHECK
CLEAR.BREAKS

\ ********************** INC File **********************

0.00000000 ^Bio_DayBin_TPO_Percent @!
0 ^Bio_Primary_Tripped_Counter @!
\ ""DOWNLOAD_COMPRESSION_OFF
MAKECHECK 
0 0 0 BP! 
0 0 1 BP! 
0 0 2 BP! 
0 0 3 BP! 
0 0 4 BP! 
0 0 5 BP! 
0 0 6 BP! 
0 0 7 BP! 
0 0 8 BP! 
0 0 9 BP! 
0 0 10 BP! 
0 0 11 BP! 
0 0 12 BP! 
0 0 13 BP! 
0 0 14 BP! 
0 0 15 BP! 
BurnIt . 
1 I!AUTORUN 
?EXISTS DL.STAT SWAP DROP .IF 0 DL.STAT .THEN
